pc-automata -f ../../md/tesis_with-bib.md \
            -c ../in/img/portada.png \
            -i ../in/img/ \
            -x ../in/xhtml.tmp/ \
            --no-pre --overwrite

mv epub-*_*.epub tesis.epub
mv mobi-* tesis.mobi
rm -rf epub*
