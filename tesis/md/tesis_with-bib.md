# Agradecimientos

Para mi familia por el diverso apoyo incondicional.

Para Deirdre por persuadirme asertivamente a perseguir esta maestría
y por ser la primera en estar ahí.

Para Lau, Mel, Antílope, Hacklib, Naya y Enero por la apertura
de horizontes a través de sus discursos o acciones.

Para Priani y Gabi por su auxilio académico y extracadémico.

Para Colima Hacklab, el Rancho Electrónico y el grupo Miau por
el soporte técnico y la muestra de que otro mundo es posible.

Para Julián, Leti y el personal administrativo de la Coordinación
de Posgrado, con especial énfasis a Itzel, por llevarme de la
mano entre las estructuras del mundo en el que vivimos.

Para LibGen, Aaaaarg, Sci-Hub, EpubLibre y Memory of the World
por ejercer la función de bibliotecas públicas en un mundo cada
vez más privatizado.

# Lista de siglas y acrónimos

+++BC+++ / bienes comunes {.frances}

+++BOAI+++ / Budapest Open Access Initiative {.frances}

+++CC+++ / Creative Commons {.frances}

+++FSF+++ / Free Software Foundation {.frances}

+++GNU+++ / _+++GNU+++ is Not Unix_ {.frances}

+++GPL+++ / General Public License {.frances}

+++OMPI+++ / Organización Mundial de la Propiedad Intelectual
{.frances}

+++OSI+++ / Open Source Initiative {.frances}

+++P2P+++ / _peer-to-peer_ {.frances}

+++PI+++ / propiedad intelectual {.frances}

+++PRDC+++ / producción, reproducción, distribución y conservación
{.frances}

# Introducción. Un tema filosófico desatendido

El aumento de los años y del tipo de objetos que abarcan los
derechos de autor o cualquier otro tipo de propiedad intelectual
ha estado bajo disputa entre abogados, sociólogos, economistas,
políticos, programadores, _hackers_, escritores, artistas y,
en general, entre personas interesadas en el quehacer cultural
y el desarrollo tecnológico. Sin embargo, rara vez se ha reflexionado
sobre las posibilidades o limitaciones que este régimen de propiedad
puede tener sobre la producción, reproducción, distribución,
conservación e intelección de y desde la filosofía.

El impacto de la propiedad intelectual en la práctica filosófica
se presenta en un primer momento como barreras económicas, geopolíticas,
infraestructurales, legislativas o técnicas que dificultan su
ejercicio. Un ejemplo nítido de este (des)encuentro es la disposición
de la bibliografía para esta investigación. El presente trabajo
no hubiera sido posible sin el acceso digital a través de bibliotecas
que obvian las restricciones de los derechos de autor. Por un
lado, la mayoría de las obras consultadas no se encuentran en
los repositorios académicos disponibles en la +++UNAM+++. Por
el otro, sus copias impresas ---si las hay--- se encuentran en
bibliotecas o archivos alejados de la Ciudad de México. Por último,
la adquisición digital y legal de esta bibliografía implica un
desembolso imposible de llevar a cabo pese al apoyo del Conacyt.

La incapacidad o la necesidad de sortear estas barreras no solo
están presentes en la +++UNAM+++, en la Ciudad de México o entre
los becarios del Conacyt. En casi cualquier universidad o instituto
de educación superior hay una carencia de acceso a la información,
sin importar el tamaño de su infraestructura cultural. La cercanía
a los centros de producción cultural disminuye el tamaño de esta
barrera, pero con el costo de una intensa y precaria movilidad
de la periferia. La restricción del acceso a bienes culturales
no es la misma en Colima, Chihuahua, Xalapa o Puebla que en la
Ciudad de México, San Pablo, Buenos Aires o Barcelona. Sin embargo,
estas ciudades están distantes a la infraestructura para la producción
cultural, científica y tecnológica de Los Ángeles, Nueva York,
Londres, París, Fráncfort, Tokio, Shenzhen o Seúl.

Muchas instituciones gubernamentales han percibido este fenómeno
como una limitación en la accesibilidad de bienes culturales
o una restricción en la movilidad. Para contrarrestarlo se ejecutan
programas para la construcción de más infraestructura o se subsidian
viajes de estudios y de investigación al extranjero. Aunque esto
permite disminuir las barreras, no evidencia que la infraestructura
para la cultura y el conocimiento depende ya de un ecosistema
de producción global, cuyos mercados en la actualidad extraen
más capital del que ingresa a las economías de América Latina
(+++CERLALC-UNESCO+++, 2015).

Esta globalización se da a partir de dos ejes. Las legislaciones
actuales de la propiedad intelectual funcionan como mecanismos
que permiten la transferencia de bienes y conocimiento alrededor
del mundo, aunque a partir de un intercambio desigual donde las
economías de Estados Unidos, la Unión Europea o China son las
principales beneficiarias. Por otro lado, gran parte de este
traslado se da en su conversión a información que viaja por el
globo a través de las nuevas tecnologías de la información y
la comunicación. La producción de cultura en su contexto actual
es una cadena que trasciende países y sus legislaciones. Por
ejemplo, la edición de libros en España, su impresión en China
y su comercialización en países de América Latina es posible
gracias al envío de información en una red de computadoras que
viaja de Barcelona a una imprenta en Qingdao, hasta el envío
de libros en barco a América Latina, cuyas transacciones financieras
de nueva cuenta se dan en esta red informática conocida como
internet.

El impacto rara vez perceptible de las legislaciones de propiedad
intelectual y el desarrollo de tecnologías computacionales yace
en las modificaciones a los procesos de producción, reproducción,
distribución y conservación de bienes culturales. Un primer aspecto
es la modificación que está sufriendo la industria editorial,
por ejemplo. Los reproductores de conocimiento cuyo trabajo es
la elaboración de soportes, como libros, revistas o periódicos,
están en un proceso de adaptación a los horizontes abiertos por
las nuevas técnicas y a los desafíos jurídicos que suponen. Sin
embargo, en su transformación también quedan afectados los procesos
que se consideran propios del productor de conocimiento. Para
seguir con el ejemplo, el alcance de la filosofía queda delimitada
a la compatibilidad de sus técnicas de producción con las condiciones
tecnológicas actuales para su preproducción. En esta acotación
es perceptible la modificación en sus posibilidades de difusión
y en la manera en como se escribe filosofía. Una escritura a
mano, con máquina de escribir, con procesadores de texto o con
otras técnicas, como los lenguajes de marcado y de programación
---la modalidad de redacción para esta investigación---, determina
las posibilidades técnicas de su reproducción.

Estas modificaciones técnicas o el advenimiento de otras nuevas
generan la pregunta sobre qué tanto afectan la manera en como
diversas disciplinas, entre ellas la filosofía y las humanidades,
construyen su conocimiento. La siguiente investigación pretende
dar un panorama y una serie de problemáticas que permiten evidenciar
el desfase entre estos cambios y las categorías empleadas a menudo
entre los discursos sobre la producción cultural, cuyo punto
central de análisis es la concepción de un supuesto vínculo intrínseco
entre el creador y la creación de productos culturales.

Esta suposición está presente en la concepción de un ecosistema
regido por diversos tipos de propiedad intelectual, como las
patentes, los derechos de autor, las marcas, el diseño industrial,
las denominaciones de origen o los secretos comerciales. ¿Cómo
se fundamenta esta concepción? ¿Cuáles actores están presentes?
¿Por qué se percibe así y no de otra manera? ¿Cuántos problemas
y temas pueden desprenderse? Esta investigación tiene el objetivo
de dar pauta para la admisión de estas cuestiones como materia
de reflexión filosófica.

Una filosofía que no se preocupa por sus condiciones de producción,
reproducción, distribución o conservación es un ejercicio que
termina por aislarse en la soledad de lo que la tradición ha
estipulado como _sus propios_ temas y cuyo cuidado y control
se ejerce a través de una vigilancia disciplinaria que de manera
constante dice qué es o no es filosofía. Por la manera en como
se ha pensado la propiedad intelectual y los bienes comunes ---por
lo general temas de reflexión jurídica o de ciencias sociales,
políticas o computacionales---, en un primer momento podría preguntarse
cuál es el ámbito _propio_ del quehacer filosófico para esta
cuestión.

La filosofía ha encontrado su nicho en el intento por dotar de
una justificación a la propiedad intelectual. Sin embargo, en
esta investigación se propone otra manera en como la reflexión
filosófica puede abordar la disputa entre esta y los bienes comunes.
Además de la búsqueda por el control legal y técnico de obras
e invenciones, la propiedad intelectual también pone en juego
las posibilidades de recepción, intelección e incluso de producción
del discurso. Con especial énfasis en los derechos de autor,
la amalgama de legislaciones que representan estos tipos de propiedad
comprometen la materialidad del discurso en pos de maneras más
intensas y centralizadas de acumulación de capital.

La producción y reproducción de textos está constreñida a las
técnicas conformes a la legalidad estipulada por los derechos
de propiedad intelectual. ¿Qué tanto afecta las posibilidades
de elaboración del discurso? ¿Qué impacto tiene al momento de
hacer filosofía? Son preguntas aún difíciles de abordar. Más
que una relación causa y efecto o la ausencia de una correlación,
es motivo de sospecha y desasosiego que el quehacer filosófico
no ha prestado relevancia a la relación de su producción «abstracta»,
«teórica» o «inmaterial» de discursos con su materialidad.

La enseñanza y la elaboración de filosofía están permeados por
las condiciones materiales que los derechos de autor y las patentes
hacen posibles. Estos derechos constriñen o favorecen la recepción,
reproducción y producción de discursos acorde a determinados
contextos. No son barreras físicas o geográficas que en el pasado
impedían el flujo constante de conocimiento y de diálogo entre
continentes: se trata de un impedimento legal y su implementación
técnica y política que delimitan cómo y desde dónde se puede
hacer filosofía, por ejemplo. Por otro lado, las patentes determinan
los lugares donde es posible aplicar nuevas tecnologías, como
las técnicas para la reproducción de textos. La pujante producción
filosófica en otras lenguas está relacionada con la capacidad
productiva de los profesionales diestros en la redacción y edición
de filosofía, pero también en la infraestructura que permite
su reproducción y distribución de maneras más eficientes y vastas,
al mismo tiempo que concentra y determina el curso de los debates
filosóficos actuales.

Entonces, esta investigación plantea que la filosofía tiene su
nicho en la disputa entre la propiedad intelectual y los bienes
comunes como disciplina que cuestiona la conformación contemporánea
de esta discusión. Pero en su camino también termina por interpelar
el sospechoso e inexplicado entendido por el cual la elaboración
filosófica de discursos ha descuidado su materialidad, cuando
es a través de artículos y libros como se abren las posibilidades
de su enseñanza y producción. Este cuestionamiento es complejo
y trasciende esta investigación, por lo que a continuación no
se ofrece una respuesta definitiva, sino una propuesta y varias
sugerencias en como la reflexión filosófica puede pensar sus
condiciones materiales y las consecuencias intelectuales de concebir
a la producción filosófica como un ---¿disconforme?--- quehacer
cultural inserto en una economía global.

## Estructura de la investigación

Este texto se compone de dieciocho secciones distribuidas en
tres apartados. El primer bloque comprende el examen a las teorías
de la propiedad intelectual que buscan un equilibro entre la
producción como propiedad privada, su reproducibilidad y sus
modalidades de acceso. Sus seis secciones van de la búsqueda
de definiciones y teorías a las distintas propuestas teóricas
y sus dificultades para la justificación de este régimen de propiedad.

El siguiente apartado repasa las críticas de los partidarios
de los bienes comunes a estas teorías. El análisis intenta demostrar
la importancia de este tipo de bienes para la fundación de una
teoría de la propiedad privada de la producción cultural. Sus
ejes de análisis son los movimientos del _copyleft_, _copyjustright_,
_anticopyright_ y _copyfarleft_. Este bloque concluye con las
dificultades en la definición de los bienes comunes y en la descripción
breve de un ecosistema en el que estos conviven con la propiedad
intelectual.

El último apartado describe y critica los puntos de encuentro
entre ambas tendencias dentro de la producción cultural. La reflexión
se realiza a partir de la crítica a un marco teórico en común
que supone al quehacer cultural como un ecosistema distribuido
en las esferas del creador, el distribuidor y el público, así
como reduce esta actividad a su aspecto productivo y las técnicas
y formas jurídicas que amplifican y regulan sus capacidades de
reproducción. Los primeros dos apartados son centrales para la
comprensión de este último bloque porque ahí se propone que la
disputa entre la propiedad intelectual y los bienes comunes son
un síntoma de un problema más extenso en el quehacer cultural:
el desfase entre las nuevas tecnologías de la comunicación que
lo modifican, las legislaciones que pretenden regularlas y la
teoría que bebe de esta discusión para criticar las categorías
operativas que delimitan este ecosistema y que, tal vez, permita
la expansión de sus horizontes.

# Teorías de la propiedad intelectual

## 1. En la búsqueda de una definición

La propiedad intelectual (+++PI+++) se entiende de muchas maneras.
Hughes (1988), Hettinger (1989) y Stengel (2004) dicen que es
uno de los pilares para el progreso de las ciencias y las artes.
Para Hughes (1988), la +++PI+++ se entiende como propiedad intangible
cuyo valor se basa en ideas con cierto grado de novedad. Esta
también se refiere a un modo popular de apropiación en las sociedades
posindustriales donde la manufactura y manipulación de bienes
físicos abrió el camino para la producción y uso de la información
(Hettinger, 1989). Por otro lado, la +++PI+++ se define como
escasez artificial para la generación de ingresos (Palmer, 1990):
una simulación de los procesos que gobiernan al libre mercado
de los bienes tangibles (Palmer, 1990). Para Stengel (2004) la
+++PI+++ es un objeto abstracto que no tiene límites claros pero
que sirve para el control de los bienes por un tiempo definido.
O como toda propiedad, esta es un principio abstracto de individuación
que permite el establecimiento de relaciones intersubjetivas
mediadas por objetos (Schroeder, 2004). Asimismo, la +++PI+++
se comprende como un «tipo» con muchos «_tokens_» en los cuales
hay trabajo involucrado durante su producción (Shiffrin, 2007).
Para Moore (2008), la +++PI+++ es un producto de un proceso cognitivo
cuyo valor reside en ideas, un derecho para controlar su expresión
y un sistema que protege sus medios de producción.

Las definiciones son diversas y en ocasiones incompatibles; sin
embargo, caben organizarse en tres dimensiones. La +++PI+++ puede
ser _una cosa_, sea una obra como _Cien años de soledad_, la
invención de los iPhone, el logotipo de Nike, el patrón textil
de Louis Vuitton, el ingrediente secreto de la Coca-Cola o una
bebida producida con ingredientes, procesos y en lugares específicos
como el tequila José Cuervo. La +++PI+++ puede referirse a _los
derechos en torno a esas cosas_ como los derechos de autor, patentes,
marcas, diseños industriales, secretos comerciales o denominaciones
de origen, respectivamente. La +++PI+++ también puede entenderse
como _un sistema que aglomera estos derechos_.

Por los distintos grados de abstracción la +++PI+++ significa
una cosa, un derecho o un sistema. Pero entre estas acepciones
hay una primera: la +++PI+++ es _un objeto_. No existe consenso
en cuanto a su delimitación, a pesar de ello, la Organización
Mundial de la Propiedad Intelectual (+++OMPI+++) ha optado por
definirla como «creaciones de la mente» sujetas a derechos embebidos
dentro de un sistema (+++OMPI+++, 2019). Esta definición se llamará
«estándar» porque la +++OMPI+++ es el organismo especializado
de la +++ONU+++ para estandarizar la +++PI+++ entre los países
miembro. Esto implica legislaciones nacionales y acuerdos o tratados
internacionales como el «Convenio de Berna para la Protección
de las Obras Literarias y Artísticas» (+++OMPI+++, 1971). Sin
excepción, los miembros de la +++ONU+++ han de seguir las directrices
delineadas por la +++OMPI+++.

Todos estos intentos por definir la +++PI+++ han fracasado. Sin
un dejo de pesimismo, Hettinger (1989) concluye que no es fácil
de justificar. Stengel (2004) señala que se trata de un concepto
enraizado en una comprensión contemporánea del mundo. Schroeder
(1998) ve en la +++PI+++ un «montón de palos» cuyo símbolo fálico
son los fasces; es decir, son derechos poco asimilables pero
concebidos para la ocupación de objetos: la apropiación por excelencia
en las sociedades capitalistas (Schroeder, 2004). A Palmer (1990)
le parece un concepto vasto cuya suposición es que a mayor cantidad
de definiciones, una mejor justificación. Shiffrin (2007) es
escéptica y declara que la +++PI+++ es un concepto ambiguo. Barron
(2012) menciona que su énfasis en lo económico privilegia posturas
liberales o utilitaristas, ya que supone que la privatización
es el mejor modelo productivo (Barron, 2012). Epstein (2009)
indica que existe una inclinación a desintegrar la +++PI+++ por
incoherente o por ya no ser una guía fiable para el capitalismo
contemporáneo.

A pesar de la falta de consenso hay una opinión compartida. Estas
disparidades y ambigüedades tienen un mismo origen: durante décadas,
legisladores, jueces y empresarios han pretendido definir la
+++PI+++ a partir del material jurídico disponible (Hughes, 1988).
La +++PI+++ ha sido fijada a través de juicios, veredictos y
legislaciones; sin embargo, los resultados son teóricamente insuficientes.
En este panorama varias personas han aceptado el desafío de elaborar
una teoría de la +++PI+++ como Breakey (2010), Epstein (2009),
Hettinger (1989), Hughes (1988), Moore (2008), Palmer (1990),
Schroeder (2004), Shiffrin (2007) y Stengel (2004), cuyo objetivo
es la definición de la +++PI+++ allende al ámbito legislativo.

## 2. En la búsqueda de una teoría

En la sección pasada se vio que la +++PI+++ es un objeto aún
bajo definición, sea como cosa, derecho o sistema. Este término
se remonta al siglo +++XVIII+++ (Wikipedia, 2019) aunque su uso
sistemático actual data del siglo +++XX+++ (Stengel, 2004). Antes
de ello se hacían referencias a sus «manifestaciones», de las
cuales no hay consenso sobre sus primeros usos. Stengel (2004)
las rastrea desde los siglos +++XVI+++ ---en torno a privilegios
ingleses para la impresión de libros--- y +++XVII+++ ---sobre
patentes venecianas---. Estos usos no fueron en beneficio de
creadores, sino medios para el control de nuevas industrias como
la imprenta. Moore _et al._ (2014) indica que la protección más
temprana para un creador es un documento emitido en 1421 en la
República de Florencia a favor del arquitecto Filippo Brunelleschi.
Incluso el mismo autor señala antecedentes en la Antigua Grecia
o en el Imperio romano. Sin embargo, existe un consenso sobre
la atipicidad de estos casos debido a la carencia de instituciones
específicas para su protección (Moore _et al._, 2014).

De manera paulatina la discusión pasó de tipos en particular
a la búsqueda por sintetizarlos en un concepto. En su sentido
actual la +++PI+++ es _un objeto que engloba una cantidad diversa
de objetos_ ---cosas, derechos y sistemas---, los cuales son
ahora sus manifestaciones. Además, estas dimensiones se entrecruzan
hasta ser incomprensibles de manera aislada. El trato de una
cosa ---por ejemplo, _El perfil del hombre y la cultura en México_---
_como propiedad_ ya implica al propietario de sus derechos y
a sus funciones dentro de un sistema cuya concreción son las
legislaciones nacionales o internacionales. La «Ley Federal del
Derecho de Autor» (1996) señala un plazo de cien años a partir
de la muerte del autor para que su obra sea de dominio público;
hasta el año 2060 para el caso de Ramos---.

Semejante empleo de la +++PI+++ no ha pasado desapercibido. Por
un lado, este trato pretende aglutinar una diversidad de objetos
bajo un mismo concepto. Por el otro, esta sistematización pone
sobre la mesa el lugar de la +++PI+++ dentro de la propiedad
en general. Ante estas dificultades se identifican cuatro posturas.
La más común entre los teóricos analizados acepta la +++PI+++
como un subconjunto dentro de la propiedad en general (Breakey,
2010; Epstein, 2009; Hettinger, 1989; Hughes, 1988; Moore, 2008;
Palmer, 1990; Schroeder, 2004; Shiffrin, 2007; Stengel, 2004;
Lessig, 2005). La segunda acepta la +++PI+++ aunque ajena al
entendimiento común sobre la propiedad (Barron, 2012). Una tercera
postura no acepta la existencia de la +++PI+++ aunque sí de la
propiedad en general. Los argumentos para esta postura suponen
que la propiedad es tangible o que la escasez de la +++PI+++
es artificial y moralmente ilegítima (Stallman, 2004). Por último,
entre anarquistas y varios espectros políticos de izquierda se
niega por completo cualquier régimen de propiedad, aunque en
el camino la reduzcan a «propiedad privada» (Proudhon, 2010).

Sin importar su filiación, la mayoría de los autores recurren
a tres teorías para reforzar, criticar o negar la +++PI+++ o
el régimen de propiedad. Estas son la progresista, de raigambre
utilitarista, la personalista, de corte «continental», y la laborista,
de raíces anglosajonas.

Aunque estas teorías surgen en distintos contextos, comparten
ciertas particularidades. Muchas son un ejercicio intelectual
a partir de diversos fragmentos de filósofos modernos, por lo
general de Locke y Hegel. Además, en diversas ocasiones estas
teorías buscan la generación de nexos entre ellos y la noción
contemporánea sobre la creación intelectual. Esto ocasiona más
de una adaptación accidentada o un pleno desvío de las concepciones
de la propiedad de estos filósofos. Los traslados semánticos,
interpolaciones y extrapolaciones llevados a cabo se han catalogado
como «fundamentos filosóficos» de la +++PI+++.

Desde un horizonte plural de posturas, en las que es posible
proponer una diversidad de definiciones o teorías, de manera
deliberada muchos investigadores han optado por reducir su bagaje
cultural a un pastiche del canon filosófico de la modernidad
occidental. No hay menciones expresas para su explicación, como
tampoco existen argumentos que demuestren el paso de la _creación_
a la _propiedad_ intelectual. Sin embargo, a través de estas
tres vertientes teóricas se busca su fundamentación o destrucción.

## 3. La teoría progresista: el utilitarismo de la legislación estadunidense

La búsqueda por sintetizar la creación intelectual como un tipo
de propiedad se ha dado a partir de tres distintas teorías, como
se mencionó en la sección pasada. La primera teoría a revisar
es la teoría progresista ---también conocida como teoría utilitarista,
incentivista o consecuencialista (Hettinger, 1989; Palmer, 1990;
Stengel, 2004; Shiffrin, 2007; Moore, 2008; Barron, 2012)---
la cual percibe a la +++PI+++ como sinónimo de progreso en las
ciencias y las artes (Hettinger, 1989; Stengel, 2004). No es
la teoría más elaborada pero sí la más popular (Hettinger, 1989).
Stengel (2004) traza su origen en Joseph Alois Schumpeter, economista
que asoció la innovación con el progreso social.

La idea general es que la protección de la +++PI+++ fomenta la
creación de más +++PI+++ (Shiffrin, 2007), que a su vez aumenta
la utilidad social (Moore, 2008) al crear objetos valiosos para
las comunidades. La +++PI+++ se constituiría por un compromiso
del Estado con la actividad innovadora privada (Moore, 2008;
Barron, 2012).

Esta suposición es difícil de comprobar e incluso problemática
(Moore, 2008; Hettinger, 1989). Sin embargo, Shiffrin (2007)
explica que da mayor incertidumbre a la inversión porque la creación
de +++PI+++ requiere de una fuerte financiación y su reproducibilidad
es a muy bajo costo. El monopolio _artificial_ de los derechos
de +++PI+++ permite compensar esta caída en los precios al conceder
exclusividad en su comercialización por un tiempo limitado, por
lo que se gesta como un contrapeso entre la monopolización y
la libre difusión.

Los derechos de +++PI+++ también suponen que una difusión sin
protección de los intereses económicos equivale a un menor fomento
para la creación de más +++PI+++ y, en consecuencia, la disminución
de la utilidad social. Según Stengel (2004), el fomento puede
darse desde tres vertientes. Antes del acto creativo los _incentivos_
motivan al creador a realizar su trabajo. Las _recompensas_ se
otorgan por el cumplimiento del proceso creativo. Durante la
difusión del objeto producido, la _compensación_ es el reconocimiento
público del trabajo hecho por el creador.

No obstante, un problema recurrente en las teorías utilitaristas
es la ambigüedad en el empleo de los términos «utilidad», «progreso»
o «beneficio» social. Para sobrepasar esta dificultad, Palmer
(1990) propone dos argumentos: _justice-as-order_ y _+++X+++-maximization_.
El _justice-as-order_ pretende fomentar un ecosistema que permita
a las personas realizar sus fines sin la incertidumbre de la
escasez de recursos, el conflicto social o lo depredación violenta.
Por esta incertidumbre se fijan políticas para un orden social
que la evite. Como en la +++PI+++ la escasez no es «estática»,
este utilitarismo no procede porque la reconfiguración social
es innecesaria cuando la escasez puede manipularse. Entonces,
el argumento de _+++X+++-maximization_ es el que aplica para
la +++PI+++. Su objetivo es la maximización de _x_ al menor costo
posible. La _x_ es igual a la utilidad, la riqueza o cualquier
otro elemento relativo al «progreso» de las ciencias y las artes,
así como de la sociedad. Aquí la escasez tiene la función estratégica
de manipular el régimen de la +++PI+++ para la obtención de un
máximo beneficio al menor costo. Es decir, la _+++X+++-maximization_
busca un punto medio donde creadores, distribuidores y el público
se beneficien mutuamente.

Esta teoría también ha sido criticada por su carácter paradójico.
En muchos casos la +++PI+++ frena su generación (Hettinger, 1989),
porque el supuesto de que las personas producen más si tienen
mayores incentivos, recompensas o compensaciones fortalece los
derechos de +++PI+++ hasta el punto de afectar a futuros creadores
(Palmer, 1990). La teoría progresista se enfoca en las consecuencias
de la +++PI+++ _sin justificarla_ ni demostrar el progreso que
pretende. Barron (2012) señala que esta teoría supone que el
progreso de las ciencias y las artes es más eficiente si la actividad
creativa se privatiza. Pero su énfasis en lo económico explicita
un desconocimiento profundo sobre el quehacer cultural, la dinámica
social y el impacto del «expansionismo» de la +++PI+++ sobre
estos ecosistemas.

Sin importar estas dificultades, la teoría progresista está en
el fondo de la jurisdicción estadunidense. Stengel (2004) llama
a ir más allá de las palabras para ver los efectos de los monopolios
en nuestra cultura. Desde Hollywood o Silicon Valley, pasando
por las industrias petrolera o agrícola, hasta los tratados y
guerras comerciales, +++EE. UU.+++ es el Estado con mayor influencia
en materia de +++PI+++. A través de derechos de autor, patentes,
marcas, diseños industriales o secretos comerciales las industrias
estadunidenses regulan las distintas manifestaciones de la +++PI+++
y la producción de bienes culturales a nivel global. Este extenso
control de los mercados no es fortuito. La teoría progresista
tiene dos características que permiten la hegemonía de la +++PI+++
estadunidense. Por un lado, ofrece un discurso que da certidumbre
a emprendedores e inversores mediante la idea de que el acto
creativo es una cuestión privada: el autor y su obra, el inventor
y su invención. Por otro, ofrece un mecanismo para la privatización
del quehacer cultural. Si el progreso social en parte se realiza
con la +++PI+++; si la +++PI+++ es una actividad lleva a cabo
en privado; entonces el progreso social de manera parcial se
da en la privacidad del trabajo creativo.

Sin embargo, no existe un nexo que explique la relación entre
una actividad realizada en privado por un individuo y el beneficio
público o social que conlleva. Pero aún suponiendo la posibilidad
de este traslado y acorde a los teóricos expuestos, no hay datos
que comprueben que la privatización es una condición necesaria
para el progreso social. En este sentido, cabe la posibilidad
de una teoría progresista sin la iniciativa privada.

Si el objetivo es el progreso, puede establecerse un sistema
de gestión pública de la +++PI+++. El fomento sería a través
de recursos públicos donde el creador mantendría la atribución
pero los sistemas de +++PI+++ velarían por su adecuada gestión
pública, en lugar de salvaguardar derechos privados de explotación.
Debido a la deficiencia de la administración pública, esta gestión
podría ser realizada por organismos autónomos o descentralizados,
donde la intervención directa del Estado es innecesaria.

## 4. La teoría personalista: Hegel, Kant y ¿más Hegel?

La segunda teoría que pretende dar fundamentos filosóficos a
la +++PI+++ es una que retoma ideas y conceptos de Hegel y Kant.
A diferencia de la teoría progresista revisada la sección pasada,
Hughes (1988), Schroeder (2004) y Stengel (2004) asienten en
que la teoría personalista es la aproximación más completa ---también
conocida como «hegeliana» o «continental» (Moore, 2008; Shiffrin,
2007; Schroeder, 2004; Stengel, 2004; Palmer, 1990; Hughes, 1988)---.
Esta teoría bebe de los _Principios de la filosofía del derecho_
de Hegel, con especial énfasis en la primera parte ---«El derecho
abstracto»---, primera sección ---«La propiedad»--- (Hegel, 2005).
En esta obra Hegel delinea su teoría de la propiedad aunque,
como resalta Schroeder (1998), los teóricos de la +++PI+++ la
han abordado de manera heterodoxa.

Según Palmer (1990), la personalidad tiene que pasar de la potencia
---«Concepto» en términos hegelianos--- a la actualidad ---«Idea»---,
para lo cual se requiere de la propiedad. Lo que en un primer
momento parece una teoría de adquisición de objetos pronto se
convierte en una sobre la externalización de la voluntad a través
de la objetificación.

Schroeder (1998) es la autora más rigurosa en el desarrollo de
esta teoría, el cual comienza con una advertencia: varios de
sus compañeros le adjudican supuestos liberales que no se sustentan.
Hegel parte y comparte muchos de estos presupuestos pero los
lleva a sus últimas consecuencias lógicas. Un punto de partida
para entender su teoría de la propiedad es la consideración de
que no son derechos naturales porque la «naturaleza» no es libre
y el derecho permite un medio para actualizar la libertad. Su
teoría surgió cuando la explicación de la propiedad como derecho
natural dejó de ser satisfactoria. Desde un comienzo para Hegel
la libertad y la voluntad se dan en un contexto social.

En una concepción hegeliana del mundo, el individuo _está obligado_
a ser libre. Esta libertad se alcanza por la actualización de
su voluntad (Moore, 2008): no está dada, tiene que hacerse. ¿Cómo
es posible elaborarla? A través de la propiedad. La teoría hegeliana
no se limita a cosas físicas, ya que su concepción de «objeto»
implica todo lo que no es sujeto; a saber, lo carente de autoconciencia
(Schroeder, 2004). Para los teóricos de la +++PI+++ ahí hay un
vínculo orgánico para hablar de la +++PI+++ como parte de la
propiedad en general.

En seguimiento a Schroeder (2004), la sociedad es previa a la
propiedad; es decir, es el primer paso para la actualización
de la libertad a partir de relaciones intersubjetivas. La propiedad
requiere de otros, principalmente de su reconocimiento, porque
es un principio abstracto de individuación. Schroeder (2004)
indica el error común entre sus compañeros en pensar que Hegel
concibe la propiedad como la percibe la tradición liberal anglosajona
de la que forman parte. Hegel ve a la propiedad como _un momento_
inicial que ella denomina «sujeto legal». De ahí a la constitución
de la personalidad existen otros mecanismos que absorben la propiedad
privada, como la familia, la sociedad civil, el Estado o el Espíritu.
Con esto Schroeder (2004) puntualiza que para Hegel la propiedad
es solo una etapa dentro de una dialéctica que va más allá del
individuo ---un elemento que Hughes (1988) tiene presente al
tratar a la +++PI+++ como negatividad, como se verá más adelante---.

El sujeto legal implica la capacidad de obedecer las leyes. Su
constitución es formal y abstracta: es la base para la gestación
de la personalidad. Su dialéctica y concreción avanzan a través
de las relaciones de propiedad, debido a que ayudan en el establecimiento
de relaciones intersubjetivas que permiten la constitución de
los sujetos a partir del reconocimiento de otros. El asentimiento
donde un _a_ es propietario de _x_ explicita una relación de
propiedad pero también un reconocimiento de que _a_ es un sujeto.
Esta identificación no es unilateral, sino llevada a cabo por
un sujeto _b_. Entonces, el establecimiento de relaciones de
propiedad, como la venta, el regalo o incluso el robo, es un
reconocimiento recíproco de subjetividad. La propiedad es el
intermediario para la constitución de los sujetos, por lo que
sirve como un vehículo para sus fines e impide que los sujetos
_a_ y _b_ se usen como medios (Schroeder, 2004).

Además, la propiedad tiene tres características (Schroeder, 2004).
En su _posesión_ se identifica a un objeto con un sujeto; en
su _goce_ se diferencia al objeto como medio y al sujeto como
fin, y en su _alienación_ se evita la dependencia, así como otro
sujeto entra en relación. Estas características se constatan
una vez que la alienación se ha llevado a cabo ya que es una
lógica retroactiva, no prospectiva. Una vez acontecida la relación
de propiedad se hace posible evidenciar su función para la constitución
de la subjetividad: es imposible mostrarla _avant la lettre_.

Esta lógica no es aislada y, acorde a Schroeder (2004), existen
consecuencias indeseadas para los teóricos de la +++PI+++. En
el sistema hegeliano cada ciudadano precisa de un mínimo de propiedad
para actualizar su libertad porque el desarrollo de la personalidad
implica una esfera de propiedad. Por la definición hegeliana
del «objeto», es posible la fundamentación de la +++PI+++ dentro
de la propiedad en general. Sin embargo, la dialéctica hegeliana
no es prospectiva, por lo que no existe la posibilidad de cumplir
las pretensiones de varios de estos teóricos. En Hegel no hay
necesidad de un «engrosamiento» de la +++PI+++, incluso cabe
la posibilidad de argumentar lo contrario. Si la +++PI+++ es
una expresión de la voluntad, tras su muerte ya no se requiere
de protección, lo que conlleva una apertura inmediata de su obra
al dominio público. Por otro lado, con Hegel no es posible extraer
la conclusión de que las sociedades requieren sistemas de +++PI+++.
No existen lineamientos al respecto porque su interés no fue
la defensa de la propiedad, sino su justificación como derecho
positivo en un momento específico de su sistema.

Estas consecuencias limitan las pretensiones de concebir una
teoría de la +++PI+++ _ad hoc_ al quehacer cultural contemporáneo.
Para ajustarla se han recurrido a otros autores también interesados
en la persona. Un ejemplo es Humboldt y su énfasis en el desarrollo
del potencial humano (Palmer, 1990). Otro es Fichte y su distinción
entre idea y expresión de la obra literaria (Fichte, 1793). Sin
embargo, entre los teóricos de la +++PI+++ revisados, el uso
de estos autores no va más allá de una mención.

Dentro de esta problemática el caso de Kant es muy particular.
Entre los filósofos modernos canónicos a los que muchos teóricos
de la +++PI+++ reducen su campo de estudio, Kant de manera explícita
habla de una «manifestación» de la +++PI+++: los derechos de
autor. Pero su defensa es de poca ayuda. En términos contemporáneos,
el discurso kantiano va en contra de la piratería, pero sin ver
en los derechos de autor un régimen de propiedad.

Barron (2012) identifica las fuentes kantianas en un ensayo titulado
«Von der Unrechtmäßigkeit des Büchernachdrucks» (Kant, 1785)
y en la sección «¿Qué es un libro?» de _La metafísica de las
costumbres_ (Kant, 2005). Ahí Kant expone los motivos para proteger
los derechos del autor _sobre su discurso_. Según Barron (2012)
este filósofo entiende al libro en dos dimensiones: como objeto
material alienable y como un acto del habla inalienable. Para
Kant la discusión pública es necesaria para una cultura ilustrada.
Por este motivo es menester incluir la libertad de expresión
en su teoría del derecho. Es decir, los derechos de autor son
derechos a la libertad de expresión. Sin ellos sería posible
que alguien tome el discurso de otro o, más grave aún, que lo
use sin la autorización de su autor o para perjudicarlo.

La manera en como Kant-Barron entienden estos derechos implica
una mayor apertura a los actuales derechos de +++PI+++. Como
el discurso público es un elemento primordial para la cultura
ilustrada, cualquier elemento que lo obstaculice tiene que ser
eliminado. Los derechos de autor actuales son más un impedimento
que un mecanismo para desplegar esta clase de cultura. Por lo
tanto, tienen que ser descartados en pos de unos derechos de
autor más adecuados a la consecución de una cultura ilustrada
y cosmopolita.

La defensa kantiana no permite una teoría personalista basada
en un sistema de +++PI+++. Sin embargo, el énfasis en el peligro
del uso ilícito del discurso no pasó desapercibido. Josef Kohler,
jurista y poeta decimonónico alemán, vio un vínculo entre los
derechos de autor kantianos y la teoría de la propiedad hegeliana
(Cotter, 1997; Baldwin, 2014). Kohler extrajo de Hegel la defensa
de los derechos del productor para la alienación de su trabajo,
y de Kant tomó el argumento donde la obra es el discurso de su
autor. Como consecuencia, existe el derecho a impedir la apropiación
no autorizada del trabajo del autor (Cotter, 1997).

Esto desemboca en dos clases de derechos de autor. Los derechos
patrimoniales permiten la explotación de las obras por el autor
o un tercero («Ley Federal del Derecho de Autor», 1996). Estos
son los derechos de reproducción, adaptación, distribución, comunicación,
representación o ejecución pública, los cuales tienen una duración
limitada. Para dar flexibilidad, durante ese tiempo son lícitos
el uso justo, que permite el empleo de la obra con fines privados
o de investigación, y la regla de la primera venta, que admite
la reventa sin interferencia del autor o editor (Epstein, 2009).
Por otro lado, los derechos morales garantizan una unión inalienable
del autor con su obra (Cotter, 1997; Baldwin, 2014). Sin importar
su expropiación, el autor puede determinar cómo divulgarla, reclamar
autoría, prevenir desinformación o mutilación, e incluso prohibir
la crítica severa o perjudicial (Cotter, 1997).

Esta doctrina se considera parte de la tradición jurídica «continental»
(Cotter, 1997), la cual está anclada en Alemania y Francia, pero
también en México. En otro extremo, el _copyright_ es la doctrina
anglosajona que contempla los derechos de copia, muy similares
a los derechos patrimoniales y demasiado ajenos a los derechos
morales. Por este motivo la teoría personalista a veces se le
llama «continental», haciendo patente una diferencia entre estas
doctrinas que cabe tomarse en cuenta.

A primera vista parece que la doctrina de los derechos morales
ayuda a los objetivos de una teoría personalista de la +++PI+++,
ya que la inalienabilidad permite su prolongación más allá de
la vida del autor. Así es posible una «extensión» de la +++PI+++
y la necesidad de un sistema que la sustente. Sin embargo, abre
más de una dificultad. Hughes (1988) menciona que esta derivación
condiciona la protección de la +++PI+++ según su grado de expresión
personal. Existen manifestaciones muy personales como las obras
literarias. Sin embargo, en la mayoría de los tipos de +++PI+++
el grado de expresibilidad personal no es perceptible; por ejemplo,
la patente de los envases Tetra Pack o la denominación de origen
del queso Manchego. Esta doctrina se amolda muy bien a los derechos
de autor, pero su generalización es problemática. Hughes (1988)
además manifiesta que estos derechos generan problemas en la
alienación de la +++PI+++ porque el sujeto que la adquiere no
es libre de hacer valer su voluntad. En conclusión, para Palmer
(1990) y Schroeder (2004) los derechos morales son una deformación
que de manera equívoca fundamenta una teoría personalista de
la +++PI+++.

## 5. La teoría laborista: la defensa de la propiedad de Locke

Si bien la teoría personalista sentó las bases para los derechos
morales de la tradición jurídica continental, como se vio en
la sección pasada, la tercera y última teoría general para fundar
la +++PI+++ forma parte de la tradición jurídica anglosajona:
el _copyright_. Esta data sus orígenes en el _Segundo Tratado
sobre el Gobierno Civil_, capítulo 5 ---«De la propiedad»---,
donde John Locke desarrolla su teoría de la propiedad (Locke,
2006). Ahí propone una solución al problema de derivar el derecho
positivo a partir del derecho natural al combinar la creatividad
divina con la humana (Stengel, 2004). Según Locke (2006), Dios
dio la tierra a los hombres para su comodidad y existencia. Aunque
esta es de todos, existe una propiedad que solo le pertenece
a cada uno: su cuerpo. Mediante este y sus manos el hombre produce
los frutos que «podemos decir que son suyos». Con su labor el
hombre saca los elementos de su estado de naturaleza que, por
consiguiente, «hace que no tengan ya derecho a ella los demás
hombres». La propiedad queda así justificada mediante la labor
(Shiffrin, 2007).

Aunque Locke habló de propiedad física, principalmente en un
contexto agrícola, su argumento permite una extrapolación. Así
como el hombre es dueño de las manzanas que cultiva, también
puede ser propietario de las ideas que genera. Lo que tenemos
en la argumentación lockeana es una expansión de los derechos
que van de la propiedad del cuerpo a sus frutos producidos (Moore,
2012). El enlace entre uno y otro extremo es la actividad física
y poco placentera que se lleva a cabo en la labor (Hughes, 1988).
El cuerpo sería el punto de partida mínimo para la esfera que
comprende lo que es suyo, donde la propiedad corresponde a los
objetos que la expanden y la labor es aquello que convierte el
bien común en un objeto entremezclado con la corporeidad del
ahora propietario.

Se trata de una visión individualista de la creación de +++PI+++
(Palmer, 1990; Moore, 2012). Con Locke está patente el supuesto
liberal anglosajón donde el individuo precede a la sociedad (Schroeder,
2004) o al menos no la requiere para el surgimiento de su derecho
positivo sobre la propiedad, pese a que este tenga su sustento
en un Estado de derecho.

Este constante aumento de la esfera «privada» es conflictivo
en un contexto comunitario, por lo que Locke establece dos condiciones
para legitimar y delimitar estas apropiaciones (Hettinger, 1989;
Stengel, 2004). Para evitar los monopolios, (1) siempre se tiene
que dejar lo suficiente para los demás (Hettinger, 1989; Stengel,
2004). Con el fin de que nadie produzca más de lo que necesita,
(2) el desperdicio queda prohibido (Hettinger, 1989; Stengel,
2004).

La teoría laborista es fácil de comprender; no obstante, por
su simpleza existen dificultades para la fundación de legislaciones
pertinentes. Locke nunca quiso establecer una base jurídica,
sino justificar la propiedad en su traslado del derecho natural
al positivo (Stengel, 2004).

Un problema es la «primera ocupación» (Schroeder, 2004). En la
teoría de Locke hay un punto de partida por el cual es posible
una ocupación primigenia y unilateral de los bienes comunes.
Como en principio hay suficientes recursos para todos, no es
necesario el consentimiento para estas ocupaciones, lo que implica
una oligarquía de los que llegan primero (Breakey, 2010). Con
el paso de los años estos bienes se agotan y el acceso deja de
ser por medio de la «primera ocupación» para darle paso a un
mercado que permite la transferencia de la propiedad. Esto genera
un desequilibrio entre los primeros y consecutivos propietarios.
En consecuencia se aumenta el tamaño del aparato burocrático
y se «robustece» la legislación hasta llegar a ser «ridículos»
(Breakey, 2010). Las soluciones son la modificación de las condiciones
lockeanas o el permiso de la «primera ocupación» siempre y cuando
exista un «desierto moral»: el derecho a poseer sin consentimiento
si no existe un contexto previo de reclamos. Cuando esto deja
de ser posible, la legislación ha de modificarse para responder
_ad hoc_ a la situación (Moore, 2012). Sin embargo, Hughes (1988)
y Moore (2012) argumentan que este problema no afecta a la +++PI+++
ya que la base común de la que parten ---a saber, las ideas---
nunca se acaba ni puede ser propiedad exclusiva de una persona.
La primera ocupación siempre sería posible porque no hay nadie
que agote el campo infinito de ideas.

En este sentido, la condición (1) es relevante para la producción
y distribución de la +++PI+++. Hughes (1988) y Moore (2012) suponen
que la +++PI+++ no tiene límite en cuanto ideas, pero sí sobre
el reparto de sus «expresiones concretas». Sin embargo, la definición
estándar de la +++PI+++ obvia que la entrada al «mundo de las
ideas» se da a través de expresiones concretas cuyos frutos no
siempre vienen del productor, su cuerpo o talento. Para tener
conocimiento del _cogito ergo sum_ es necesaria la disponibilidad
de una edición del _Discurso del método_ u otra fuente que hable
al respecto. Sin este acceso, es muy improbable que un sujeto
deduzca que el «pienso y luego existo» fue un punto sin retorno
para la filosofía moderna. Las ideas requieren contexto y un
soporte físico para su acceso, son raros los casos como el de
Leibniz y Newton que de manera autónoma elaboraron lo que posteriormente
se conocería como cálculo ---nótese que a pesar de ello, compartieron
un contexto histórico del desarrollo de las matemáticas---. Estos
mismos teóricos indican que la condición (2) no afecta a la +++PI+++
debido a que las ideas nunca se desperdician. No obstante, si
el acceso de una idea depende de su soporte y contexto, su desperdicio
es relevante cuando los derechos de +++PI+++ controlan sus «expresiones
concretas». La limitación artificial desaprovecha un acceso más
amplio a las ideas y, por ende, limita las posibilidades de sus
transformaciones o la generación de más ideas.

Otra dificultad es el condicionamiento de la propiedad a la labor
desagradable. Locke supone que no hay labor placentera y por
ello ha de recompensarse. En consecuencia, a mayor goce, menor
custodia (Stengel, 2004). El grado de protección de la propiedad
se hace relativo al desagrado que implica su generación. Si bien
cabe la posibilidad de mecanismos que lo compensen (Moore, 2012),
Hettinger (1989) explica que estos suponen un valor intrínseco
de la labor. Esta valoración ignora los factores externos que
afectan al valor de los frutos, como las disposiciones circunstanciales
del mercado.

Una objeción yace en la presentación de la teoría. El argumento
va de manera progresiva de un comienzo mítico, religioso y natural
a uno real, histórico y positivo. Sin embargo, las condiciones
lógicas para la satisfacción de este recorrido suponen su punto
de partida desde un inicio. Schroeder (2004) ha señalado ---aunque
no para criticar esta teoría--- que el «estado de naturaleza»
es una hipótesis lógicamente necesaria para su explicación en
retrospectiva. Es decir, semejante estado es una producción del
hombre (Schroeder, 2004) para dar significado y sentido a su
situación actual a partir de una génesis que podría remontarse
hasta Adán y Eva, como lleva a cabo Locke.

Un último problema es que la teoría laborista permite fundamentar
una «antiteoría» de la +++PI+++. Moore (2008) menciona que desde
Pierre-Joseph Proudhon se ha criticado la teoría lockeana por
suponer una expansión del régimen de la propiedad a partir del
cuerpo. Aunque en _¿Qué es la propiedad?_ Proudhon (2010) no
hace referencia a Locke, este dedica cuantiosas páginas para
criticar el fundamento de la propiedad a partir del «trabajo»
como una concepción que atenta a la libertad de otros o de la
sociedad, hasta su necesaria abolición.

Pero supóngase que el trabajo es el fundamento para la propiedad.
De ser así, su valor reside en el trabajo empleado para su producción.
Es decir, la utilidad yacería en la labor del sujeto y no en
el objeto producido. Entonces, una primera consecuencia es que
los objetos no serían propiedad del dueño de los medios de producción,
sino de quien los dispone de manera efectiva a través de su trabajo.
El peón y no el capataz sería el propietario de la milpa, porque
con su trabajo la siembra y cultiva. Pero ¿para qué detenerse
ahí? Si el trabajo es la fuente de apropiación de los objetos.
Si este se ejerce por medio de las herramientas que permiten
su manufactura y si al mismo tiempo requiere de un lugar para
su ejecución, que para Locke también está sujeto a dominio. Por
lo tanto, el peón también es propietario de la infraestructura
necesaria para la producción, al menos durante el tiempo necesario
para la actividad productiva. En consecuencia, la teoría laborista
volatiza, absorbe o vuelve irrelevante a la propiedad privada,
incluyendo la +++PI+++ que se pretende fundamentar: solo se requiere
una reforma a los derechos laborales.

Por su simplicidad y potencia, esta teoría ofrece una mayor flexibilidad,
hasta el punto de ser inestable. En cuanto a su nomenclatura,
puede encontrarse con distintos nombres según el aspecto preferido
a resaltar. Aquí se prefiere el mote de «teoría laborista» por
un doble cometido. En su tratado, Locke habla de _labor_ y no
de _work_. Además, se atiende a la distinción que Arendt (2003)
hace entre labor, trabajo y acción, donde la primera se dedica
a la satisfacción de las necesidades vitales. La teoría de Locke
defiende los derechos de propiedad a partir del sustento básico
de la vida humana, por lo que la «labor» arendtiana es aplicable.

## 6. La posibilidad de una teoría

Pese al ánimo generalizado para elaborar una teoría de la +++PI+++,
como se ha expuesto en las tres secciones pasadas donde se revisaron
las teorías progresista, personalista y laborista, que respectivamente
están basadas en teorías utilitaristas, en Hegel y Kant, así
como en Locke, los resultados aún no son satisfactorios. Hughes
(1988) menciona que todas tienen sus detalles aunque podrían
complementarse para sobrepasarlos. Hettinger (1989) acepta que
no hay una justificación adecuada a la +++PI+++. Palmer (1990)
resalta que la mayoría de los argumentos ofrecidos en estas teorías
vienen de defensores de la propiedad privada y del libre mercado.
Para Stengel (2004) existen nexos entre las teorías, lo que refleja
la necesidad de poner parches. Schroeder (2004) hace énfasis
en que, con tal de defender la +++PI+++ mediante Hegel, se termina
por citarlo de manera incorrecta hasta generar una visión romántica
que no le corresponde y que en su lugar crea un fetiche en torno
a la creación intelectual. Shiffrin (2007) denota que sin importar
la postura ante la +++PI+++, la mayoría busca una justa compensación
al creador, aunque el detalle estriba en sus formas. Epstein
(2009) destaca la teoría de la +++PI+++ como un sistema sujeto
a los derechos liberales de libre empresa y de propiedad privada.
Moore (2012) lanza una advertencia: privilegiar el aspecto económico
de la +++PI+++ genera el peligro de minar al concepto y las instituciones
que la resguardan. Por último, al menos para los derechos de
autor Barron (2012) propone un giro: pasar de un régimen de propiedad
a un sistema para libertad de expresión.

Detrás de esta insatisfacción yace un conflicto de intereses.
La teoría no se considera apropiada si no sirve al menos de guía
para el quehacer político, económico y jurídico involucrado en
la creación y gestión de la +++PI+++. Si la teoría no da respuesta
a las necesidades del quehacer cultural contemporáneo, se ha
de poner entre paréntesis o desecharse.

Se habla de «contemporaneidad» cuando en realidad se alude al
quehacer cultural del capitalismo global. Se indica «insatisfacción»
en la teoría aunque más bien son inconsistencias entre las antiguas
formas de gestión de la +++PI+++ y los nuevos regímenes para
su gestación y administración catapultados por las tecnologías
de la información y la comunicación. Se acusa de «incompletud»
cuando por lo general estos teóricos ignoran la relevancia fundamental
que tienen los «bienes comunes» para la +++PI+++.

# Críticas de los bienes comunes

## 7. El familiar incómodo

La carencia de fundamentación del concepto «propiedad intelectual»,
la ausencia de teorías consistentes que lo justifiquen, todo
ello visto en las seis secciones anteriores, así como el engrosamiento
cuantitativo ---años aumentados para su protección--- y cualitativo
---tipo de obras bajo resguardo--- de este tipo de propiedad
no ha pasado inadvertido. Baldwin (2014) indica que sus retractores
han estado presentes desde hace más de tres siglos. Por ejemplo,
Condorcet (1776) abogó por una autoría distinta al derecho de
propiedad exclusivo. Años después el librero alemán Kehr (1799)
publicó diversas obras sin autorización para facilitar el acceso
a quienes no podían adquirir ediciones legítimas. Poco más de
sesenta años después, Proudhon (1862) escribió _Les Majorats
littéraires_ donde manifiesta su oposición a los escritores,
artistas y juristas franceses, encabezados por Alphonse de Lamartine,
que justificaron derechos de +++PI+++. Proudhon (1862) acepta
que los autores e inventores merecen una compensación justa por
su trabajo; sin embargo, la producción no es propiedad.

En la actualidad, los defensores de los «bienes comunes» (+++BC+++)
esgrimen argumentos similares a los elaborados desde el siglo
+++XVIII+++. No obstante, como Baldwin (2014) resalta, la ola
actual en contra de la +++PI+++ se caracteriza por su tenue conciencia
histórica. Quizá se deba al punto de partida de la mayoría de
los críticos: los campos del desarrollo de _software_ y, en general,
de las nuevas tecnologías de la información y la comunicación.

La +++PI+++ de manera constante se contrasta con los +++BC+++.
Por ello Lessig (2005) y Baldwin (2014) hablan de una «guerra»
con dos bandos: quienes defienden el engrosamiento o la permanencia
de las actuales legislaciones de la +++PI+++ y quienes buscan
su disminución o readaptación a las nuevas tecnologías de la
información. Hoy en día existe un desfase entre las nuevas técnicas
de producción y reproducción y sus regulaciones jurídicas. Las
consecuencias de esta «guerra» son sociales y culturales, aunque
gran parte del debate acontece en los terrenos económico, político
y jurídico. En cada bando se discute la manera adecuada para
resolver esta tensión. Los defensores de la +++PI+++ asienten
en que las tecnologías de la comunicación deben obedecer el dictamen
de las leyes. Por otro lado, los defensores de los +++BC+++ argumentan
que la adaptación ha de ser en las legislaciones y los modelos
económicos.

Los +++BC+++ han ocupado un lugar secundario dentro de las teorías
de la +++PI+++, por ejemplo, para Hughes (1988) son una consecuencia
de su expiración. Pocos teóricos, como Epstein (2009) y Barron
(2012), han manifestado una preocupación sobre un ecosistema
robusto de la +++PI+++ que podría convertirse en «anticomunes».
Este fenómeno acontece cuando una protección tan extensa y compleja
torna los costos de transacción en un acceso controlado y prohibitivo.
Barron (2012) llama a la preservación de los +++BC+++ por ser
_uno de los dos momentos_ de una estructura que garantiza la
libertad de autoría para todos. Por ello, pese a la poca o nula
importancia dada a los +++BC+++, vale la pena retomarlos para
evaluar la posibilidad de una teoría de la +++PI+++.

## 8. El resurgimiento: el _software_ libre

Uno de los principales orígenes de la ola actual en contra de
la +++PI+++, el familiar incómodo que se mencionó en la sección
pasada, se remonta a principios de los ochenta en el laboratorio
de inteligencia artificial del +++MIT+++ donde el joven programador
Richard Stallman y otros colaboradores se percataron de un fenómeno
que estaba modificando la manera de hacer _software_. Acorde
a Stallman (2004) durante los setenta el código se compartía
sin ninguna restricción. Sin embargo, en la siguiente década
se convirtió en propiedad privada. Esto ocasionó divisiones en
las comunidades tecnológicas que crecían alrededor de las primeras
universidades con computadoras conectadas a internet.

Stallman y otras personas se organizaron para dar lugar al movimiento
del _software_ libre. Su declaración de principios y plan de
trabajo se publicó en 1984 con el título _El manifiesto +++GNU+++_
(Stallman, 2016). El manifiesto declara cuatro libertades del
_software_: las de uso, estudio, distribución y modificación
de los programas computacionales. Para llevarlas a cabo este
movimiento comenzó el proyecto +++GNU+++ ---un acrónimo recursivo
y «humorístico» que significa _+++GNU+++ is Not Unix_---, el
cual busca el desarrollo de un sistema operativo completamente
«libre», y fundó la Free Software Foundation (+++FSF+++) para
financiar, preservar y difundir el _software_ libre.

En el terreno teórico el movimiento del _software_ libre propuso
el concepto de «_copyleft_». Este término, acuñado por Don Hopkins,
busca explicitar una oposición a las actuales legislaciones del
_copyright_ (Stallman, 2004). Las obras bajo _copyleft_ permiten
cualquier uso, excepto la adición de restricciones ---y aunque
este programador no lo indique, tampoco se admite la ausencia
de atribución---. En el ámbito legislativo el _copyleft_ se vale
del _copyright_ para un fin contrario a su cometido: el acceso
casi irrestricto de las obras. Su primera aplicación fue la +++GPL+++
(Licencia Pública General, por sus siglas en inglés).

La +++GPL+++ es una licencia de uso no exclusiva que se añade
al _software_ con _copyright_. Con la finalidad de facilitar
el acceso a las obras protegidas, esta licencia permite usarlas
sin necesidad de una cesión de derechos tradicional («Ley Federal
del Derecho de Autor», 1996). Lessig (2005) han señalado que
estas licencias no van en contra del _copyright_, sino que son
un traslado de «todos los derechos reservados» a «algunos derechos
reservados». Cualquier programa bajo +++GPL+++ puede usarse según
sus términos y condiciones sin la necesidad de pedir permiso
explícito al propietario de sus derechos.

Para proteger las cuatro libertades y la atribución, el _copyleft_
y la +++GPL+++ imponen el uso de la misma licencia a cualquiera
de sus obras derivadas. Esto se conoce como «cláusula hereditaria».
Si un usuario quiere modificar y distribuir un programa de cómputo
bajo +++GPL+++, este debe ser publicado con dicha licencia. La
originalidad de las licencias _copyleft_ dentro de los derechos
de autor es su ausencia de uso reservado. Según Stallman (2004),
así es posible la preservación del _software_ libre pese a las
actuales leyes del _copyright_: no resuelve el problema, pero
en la práctica solventa dificultades en torno a la restricción
de acceso al código.

## 9. La bifurcación: el código abierto

En la sección pasada se explicó que el _copyleft_ es uno de los
conceptos centrales del _software_ libre, ya que hace uso del
_copyright_ embebido en el código para su uso _casi_ irrestricto,
incluyendo la «libre» compartición y modificación sin necesidad
de autorización explícita por parte de su creador. Un supuesto
básico de este concepto y, en general, del movimiento del _software_
libre es que el código es un +++BC+++ en lugar de alguna clase
de propiedad privada. Stallman (2016) en continuas ocasiones
ha criticado la pretensión de hacer del código una propiedad,
así como una y otra vez ha indicado la ambigüedad omniabarcante
de la +++PI+++, hasta el punto de prohibir su uso en el discurso
y a cambio exigir que se hable de sus manifestaciones.

Esta inflexibilidad fue percibida por muchos colaboradores que
en un principio les atrajo las intenciones del _software_ libre.
Ya en _El manifiesto +++GNU+++_ existe la intención de Stallman
(2016) por fundamentar al movimiento a través de la constitución
de una ética deóntica. Cada vez con menos frecuencia este programador
habla de una «Regla de Oro» (Stallman, 2004; Stallman, 2016).
Esta estipula que si la acaparación de la información es indeseable,
se debe obrar de tal manera que su liberación sea posible. Stallman
(2004) la cataloga como una «ética kantiana», una especie de
imperativo categórico para el desarrollo de _software_. Como
es evidente, aún son necesarios estudios filosóficos al respecto.

La actitud y eticismo de Stallman generaron varias tensiones
que en los noventa llegaron a un punto de inflexión. Stallman
(2004) relata que personas dentro del movimiento argumentaron
que el uso del término «libertad» era contraproducente y, por
lo tanto, tenía que eliminarse. En inglés la denominación «_free
software_» puede entenderse como _software_ gratuito en lugar
de libre. La oposición señalaba que la ambigüedad del vocablo
se prestaba a interpretaciones poco atractivas para la iniciativa
privada capaz de brindar apoyo financiero al proyecto +++GNU+++.
En este sentido, otra tarea pendiente es el análisis de la noción
de «libertad»: ¿qué tanto la ambigüedad en su definición y uso
es un problema filosófico heredado al campo del desarrollo de
_software_?

En 1997 el debate llegó a un camino sin salida y, con ello, la
bifurcación del movimiento del _software_ libre. En ese año Eric
Raymond publicó el ensayo _La catedral y el bazar_, donde hace
hincapié a los nuevos modos de producción de _software_. Para
Raymond (2016), una organización más «horizontal» da mayor flexibilidad
y calidad a las técnicas de programación. Antes del sistema operativo
+++GNU+++/Linux, el desarrollo de los programas de cómputo y
su evolución se caracterizaban por jerarquías que de manera paulatina
volvían muy compleja y costosa su producción. Este modelo de
desarrollo le llama «catedral», ya que consiste en la división
del trabajo entre arquitectos y albañiles, además de conllevar
una planificación monumental _a priori_. En contraste, el modelo
bazar de +++GNU+++/Linux ofrece mayor versatilidad al fomentar
un modo de producción según los méritos de cada programador,
cuya resultado es una organización dinámica que mejora el programa
según sus necesidades y usos reales.

En 1998 se fundó la Open Source Initiative (+++OSI+++), lo que
formalizó la bifurcación del movimiento. Una consecuencia fue
la generación de licencias de uso no exclusivas y abiertas. Varios
adherentes a la iniciativa del código abierto, como Maynard (2010),
han denunciado que la cláusula hereditaria es paradójica ya que
niega su propia pretensión: «restringir la libertad para poder
maximizarla». Una de las principales diferencias entre la +++FSF+++
y la +++OSI+++ es que la última no supone que el código sea un
+++BC+++, sino una especie de propiedad trabajada en común pero
explotable por cualquier individuo. Por este motivo las licencias
de uso no cuentan con la cláusula hereditaria del _copyleft_,
lo que permite el «cierre» del código o el «retorno» a las formas
tradicionales de gestión de derechos. Según Maynard (2010), si
se busca maximizar la libertad, se tiene que garantizar la decisión
de no publicar las mejoras para salvaguardar la libertad de empresa.
Dada la posibilidad para la privatización del _software_, en
la actualidad el código abierto goza de mayor popularidad que
el _software_ libre, como es evidente en la lista de empresas
que apoyan a la +++OSI+++ o The Linux Foundation (2019): +++AT&amp;T+++,
Google, +++IBM+++, Intel, Microsoft, Samsung, entre otros.

## 10. La amplificación: la cultura libre y el acceso abierto

Aunque la +++FSF+++ y la +++OSI+++ difieren en criterios al momento
de definir qué es una licencia «libre» o «abierta», como se ha
evidenciado en las dos secciones anteriores, en la práctica los
desarrolladores de _software_ encuentran puntos intermedios para
poder ejecutar proyectos. Un ejemplo de esta flexibilidad son
comunidades que cuentan con sus propios criterios, como el _Contrato
social de Debian_ en el cual se define el _software_ libre para
la comunidad de Debian, una distribución de un sistema operativo
+++GNU+++/Linux (Debian, 2004).

A finales de los noventa y principios del nuevo milenio esta
diversidad de ideas empezaron a influenciar otras esferas de
la creación intelectual. Dos casos relevantes son la cultura
libre y el acceso abierto, cuya gestación se dio de manera paralela.

En sus orígenes las licencias _copyleft_ se limitaban al desarrollo
de _software_. Lawrence Lessig, abogado y académico, por lo general
se le acredita como uno de los personajes que trasladaron este
discurso al terreno general del quehacer cultural. En _Por una
cultura libre_ Lessig (2005) deriva los ideales del _software_
libre a un equilibrio entre la «anarquía» y el «control» de la
+++PI+++. El objetivo es un cambio legislativo que apoye y proteja
a los creadores sin perjudicar el derecho al acceso de la información
de los usuarios. No consiste en una cultura sin propiedad, sino
en una del permiso. Para Lessig (2005) el _copyright_ contemporáneo
es un problema debido a su engrosamiento cuantitativo y cualitativo
hasta la subversión de sus cometidos: un mecanismo que detiene
la labor creativa cuando el internet permite la difusión eficiente
de contenidos.

Lessig (2005) asiente con la relevancia social de la propiedad
y la +++PI+++; sin embargo, este régimen no es omniabarcante.
Este abogado argumenta que la legislación y los mercados deberían
ajustarse a las nuevas tecnologías de la comunicación. La regulación
ahora no solo depende de las leyes, sino también del código:
«el código es la ley» (Lessig, 2009). Para este autor, en esta
«guerra» se necesita un punto medio que evite el control sobre
los usuarios. Por ello, la cuestión no es la elección entre la
+++PI+++ o los +++BC+++, sino en la construcción de sistemas
flexibles de propiedad.

En general, Lessig (2005) indica que uno de sus objetivos es
la redefinición del debate en términos más amplios y no binarios,
para ello propone las licencias de uso Creative Commons (+++CC+++).
Estas retoman las licencias del _software_ libre o del código
abierto para su amplificación a cualquier creación intelectual.
Para Lessig (2005) las +++CC+++ son un _copyright_ razonable
que complementan las legislaciones actuales, cuya finalidad es
la consolidación de un movimiento de consumidores y creadores
en el cual todos puedan adquirir o recibir compensaciones de
manera justa.

Diversos académicos también percibieron la posibilidad de un
equilibrio entre las legislaciones actuales del _copyright_ y
el uso de las nuevas tecnologías. Según Moore (2019) los orígenes
de esta observación son las comunidades de edición independiente
de los ochenta y noventa. Para varios editores académicos la
alternativa del régimen de propiedad llevada a cabo por el movimiento
del _software_ dio un ejemplo de oposición a las prácticas de
editoriales comerciales. La influencia del movimiento del _software_
libre o la iniciativa del código abierto es todavía una cuestión
por examinar. También está pendiente el análisis del impacto
del acceso abierto en la producción filosófica de América Latina,
ya que lo más evidente ha sido la conveniencia del internet para
la publicación académica (Moore, 2019).

Moore (2019) describe que en los noventa varios académicos empezaron
a experimentar con tecnologías de edición digital, en ese entonces
un espacio libre de editoriales comerciales. Aunque la iniciativa
del acceso abierto no es unitario, sus adeptos asienten en que
las editoriales comerciales no siempre respetan los intereses
del quehacer académico. Por ello, no solo es una cuestión de
acceso a la información, sino la necesidad de mecanismos para
que los académicos tengan un mayor control sobre su trabajo.
Aunque aún es un espacio en disputa, en esos años surgieron varios
proyectos en pos del acceso abierto de los artículos científicos,
como la Budapest Open Access Initiative (+++BOAI+++) del 2002.

La «Budapest Open Access Initiative» (2002) busca que la literatura
científica esté disponible en internet para que los usuarios
la puedan leer, descargar, copiar, distribuir, imprimir o referenciar
sin ningún costo. La única restricción sería el derecho del autor
sobre la integridad de su trabajo. Este requisito marca una distancia
con la tradición del _copyright_ para acercarse a la doctrina
de los derechos de autor. No es un control sobre la copia, sino
el dominio del autor para ser propiamente citado y conocido.
Para garantizar este acceso, la +++BOAI+++ propone dos estrategias:
la constitución de repositorios públicos disponibles en internet
para la conservación y consulta de los artículos científicos,
y la publicación de revistas académicas sin costo, de preferencia
financiadas con recursos públicos.

Sci+++ELO+++ es un ejemplo previo a la +++BOAI+++ y en América
Latina de estas estrategias. En 1998 en Sao Pablo, Brasil, nació
el proyecto de una biblioteca electrónica de acceso abierto Scientific
Electronic Library Online (Wikipedia, 2019). En la actualidad
Sci+++ELO+++ tiene presencia en casi todos los países de América
Latina, incluyendo México ---gestionada por la +++DGB+++ de la
+++UNAM+++ (Sci+++ELO+++ México, 2019)---. Otros proyectos a
resaltar en esta parte del mundo son Dialnet, para la producción
académica en habla hispana, y la Red de Revistas Científicas
de América Latina y el Caribe, España y Portugal (Redalyc).

La amplificación de la ola contemporánea en pos de los +++BC+++
va de las comunidades de editores independientes y de desarrolladores
de _software_, pasando por comunidades científicas y creadores
en general, hasta derivaciones muy específicas como los movimientos
de fuentes o edición libres. Las fuentes libres hacen hincapié
en la especificidad de sus creaciones y en la necesidad de su
acceso público (Crossland, 2008). La edición libre pretende amplificar
las posibilidades de reproducción a partir de tecnologías, formatos
y licencias libres, con un fuerte énfasis en el aspecto político
y la responsabilidad social del quehacer editorial (Tuerto, 2019).
El resultado es un ecosistema cada vez más robusto de creadores
y usuarios en pos de los +++BC+++.

## 11. La crítica interna: el _copyfarleft_

El aumento de creadores y usuarios a favor de los +++BC+++ no
ha sido un fenómeno consistente ni uniforme, como ha quedado
en evidencia en las tres secciones pasadas. El único consenso
es que que las actuales legislaciones de +++PI+++ son demasiado
restringidas. Tampoco existe claridad sobre las categorías empleadas
por estos proponentes como «libertad», «apertura», «acceso»,
«información», «autoría», «propiedad», «propiedad intelectual»
e incluso el mismo término de «bienes comunes».

Ante la falta de congruencia entre la teoría y práctica, y de
coherencia en el discurso, en 2010 Dmytri Kleiner publicó el
_Manifiesto telecomunista_, cuya intención es la conducción de
los logros conseguidos por el _software_ libre hacia un socialismo
de izquierda. Para Kleiner (2019) la sociedad y la cultura como
un mercado es un imaginario capitalista que debe eliminarse.
Para ello se vale del «comunismo de riesgo»: un modelo de organización
inspirado en la tipología de la red, donde la transformación
social es a partir de un modelo de intercambio descentralizado
y compuesto por voluntarios, lo que también llama «economía de
red». Esta obra es un híbrido de diferentes ideas trabajadas
por colectivos europeos de izquierda y los discursos de la ola
contemporánea en pos de los +++BC+++. Por este motivo y con explícita
alusión al _Manifiesto comunista_, Kleiner redactó un manifiesto
«telecomunista».

Un supuesto central del manifiesto es que el internet no podrá
ser «libre» si continúa bajo el financiamiento capitalista. Kleiner
(2019) explica que el «trabajador en internet» no difiere del
obrero porque no es propietario de los productos de su trabajo.
Para resolver está tensión existe una postura concreta: la propiedad
privada es antagónica de la libertad porque a distancia controla
la producción de una persona, subyugándola al dueño de los medios
de producción. Kleiner (2019) propone dos estrategias para la
lucha de clases: el comunismo de riesgo y el _copyfarleft_. Este
comunismo busca la generación de una capacidad económica que
provoque un conflicto de clase y, con ello, la destrucción del
sistema capitalista. Las licencias _copyfarleft_ son el dispositivo
concreto para la gestación de esta capacidad económica.

Esta propuesta problematiza la posición del mercado en los +++BC+++.
Para Raymond (2016), Maynard (2010) y Lessig (2005) el mercado,
como elemento central para el desarrollo de la creación intelectual,
no es puesto en duda. A través de sus escritos es perceptible
la preocupación por los monopolios, pero de manera explícita
argumentan la necesidad del mercado para el quehacer cultural.
El apego a este no es explícito en la +++FSF+++. En varios escritos
Stallman (2004) habla sobre el código como una cuestión social;
sin embargo las posturas políticas de este autor y la +++FSF+++
hacen patente el estímulo a la comercialización del _software_.
El código como producto de una comunidad «libre» no es necesariamente
gratuito, debido a las posibilidades de venta final o de cobro
por soporte técnico.

Acorde a la Free Software Foundation (2019), una licencia libre
no discrimina su uso según el tipo de usuario, incluyendo a gobiernos
y corporaciones. Esto permite la apropiación del trabajo de las
comunidades por parte de terceros sin la obligación de contribución,
siempre y cuando se respeten los términos de licencia. Aunque
la pretensión de las licencias _copyleft_ sea la «libertad» de
sus usuarios, concede más libertades al uso del objeto producido
que al sujeto que lo produce. El movimiento chino anti-996 es
un buen ejemplo de cómo las compañías que se valen de licencias
de uso al mismo tiempo explotan a sus desarrolladores de _software_
con horarios de nueve de la mañana a nueve de la noche, seis
días a la semana («La ingeniosa protesta en China contra el sistema
de trabajo 996», 2019). Esta realidad es similar a las consecuencias
de la relocalización del trabajo de compañías estadunidenses
o europeas hacia India o América Latina.

Para Kleiner (2019) el _copyleft_ no permite observar el conflicto
de clases. Su principal innovación fue volver al _copyright_
contra sí mismo. Pero no existe una oposición al «capitalismo
de riesgo»: un modelo de apropiación privada de plataformas digitales
cuyo valor fue producido gratuitamente por sus usuarios. Según
Kleiner (2019) la pobreza tiene sus orígenes en la explotación
de la clase productora, no en una falta de cultura o de acceso
a la información. Por ello, el comunismo de riesgo debe operar
en un ecosistema integral de bienes y servicios. Con esto, Kleiner
(2019) comienza la crítica a la ola contemporánea en pos de los
+++BC+++ por su complicidad en la acumulación capitalista.

El juicio más duro de Kleiner (2019) es hacia Lessig y las licencias
+++CC+++. Acorde al manifiesto, antes de este abogado las comunidades
a favor de los +++BC+++ luchaban en contra de todo régimen de
+++PI+++, catalogándose como _anticopyright_. Sin embargo, después
de él estas comunidades se han inclinado a reformarla, convirtiéndose
en _copyjustright_. Kleiner (2019) declara que las licencias
+++CC+++ son una versión más elaborada del _copyright_. Según
este autor, las +++CC+++ conciben lo «libre» como libertad de
consumo de las obras bajo control del creador. Estas licencias
son una restricción arbitraria según las preferencias del autor,
las cuales niegan los derechos del consumidor e invisibilizan
la relación con los productores. En consecuencia, las +++CC+++
generan un anticomún que encubre la lógica capitalista. Por un
lado regresa al «mito» del pasado incorrupto del _copyright_
y a las ideas románticas sobre la originalidad y creatividad.
Por otro, da pequeñas concesiones en un «campo de juego» determinado
por la falta de cuestionamiento a la propiedad privada y su personalización.
En conclusión, para Kleiner (2019) el trabajo de Lessig es distante
a los objetivos del _anticopyright_.

Sobre el _copyleft_ Kleiner (2019) declara que es una regresión
a los objetivos del _anticopyright_. Este supone la existencia
de una Edad de Oro del _copyright_ que en la actualidad ha decaído,
por lo que viene a enmendar dicha corrupción. Sin embargo, según
él, esto jamás sucedió: el _copyright_ nunca ha beneficiado a
los creadores. Segundo, su origen es el contexto _hacker_ alrededor
de las primeras universidades con acceso a internet en Estados
Unidos y Europa cuyo interés es el conocimiento por sí mismo,
lo que deja de lado la lucha de clases al limitarse a la producción
«inmaterial». Tercero, no es incompatible con la economía capitalista
e incluso su uso disminuye los costos de producción ---esto explica
la adopción de licencias por parte de Microsoft, Apple e +++IBM+++
cuando en el pasado fueron grandes opositores--- e induce al
trabajo de subsistencia. En conclusión, el _copyleft_ no impacta
la distribución de la riqueza y el poder, así como su efectividad
en el desarrollo de _software_ hace patente su constitución como
medio para la acumulación de capital. Se trata de un retroceso
del _anticopyright_ porque neutraliza la lucha entre el capitalismo
y el comunismo de riesgo a una oposición entre +++PI+++ y +++BC+++.
El _copyleft_ se orienta más a la libertad de información que
a la de explotación: no ayuda a los productores culturales porque
no fomenta una economía controlada por ellos.

Kleiner (2019) propone un retorno crítico al _anticopyright_
basándose en la enseñanza dejada por el _copyleft_. El _copyfarleft_
es un regreso a la «libertad» absoluta y la abolición de toda
+++PI+++. Las licencias _copyfarleft_ pretenden que los trabajadores
retengan el valor de su actividad en un contexto de uso y sustracción
en común. El comercio es posible pero entre agentes en contra
de la explotación y en pos de una economía de trabajadores como
punto intermedio para la consecución de una sociedad sin clases.
La propiedad se entiende en términos comunales, la cual funciona
para la llegada de la fracturación y transformación de la economía
capitalista. Algunos ejemplos de licencias _copyfarleft_ son
la Licencia de Producción de Pares (Kleiner, 2012) o la Licencia
Editorial Abierta y Libre (Zhenya, 2019).

Debido a la amplia utilización de categorías trabajadas por organizaciones
europeas de izquierda o marxistas, un análisis sobre su coherencia
teórica aún está pendiente. También es menester contrastar este
discurso con su recepción y recodificación por parte de comunidades
_hacker_ de América Latina. No obstante, caben resaltar dos cuestiones
sobre la crítica hecha al _copyleft_ y _copyright_.

Kleiner hace patente que el punto de disputa es en torno al control
de la producción a partir de las tecnologías «digitales». Si
bien las creaciones intelectuales se han comprendido como +++PI+++
o +++BC+++, es posible un punto de encuentro. De uno u otro extremo
existe un consenso general de que la creación es propiedad, sea
privada, pública o «en común», lo que disminuye su tensión. Ambas
posturas están en la búsqueda de una teoría y práctica del control
de la producción «intelectual»: ¿quién o qué tiene la autoridad
de determinar cómo se desarrolla _software_, cómo se compone
música, cómo se filman películas, cómo se publican textos, cómo
se genera conocimiento y, en general, cómo se hace cultura? Las
teorías de la +++PI+++ y la ola contemporánea en pos de los +++BC+++
permiten interpretarse como dos tendencias dentro de la producción
cultural, como ya señalaba Barron (2012). Pero ¿qué tal si son
«síntomas» de un fenómeno más complejo al desfase entre la legislación
y las nuevas técnicas de producción?

Por otro lado, la producción es el aspecto que más relevancia
ha tenido en esta «guerra». La infraestructura cultural actual
no solo recae en la creación constante de nuevos productos. Aún
faltan más estudios sobre la dependencia de la +++PI+++ y los
+++BC+++ a la reproducción constante de mercancías para la proliferación
de sus respectivos valores. Los costos humanos y económicos para
la manutención de esta infraestructura es equiparable al precio
que se paga para la generación de nuevos objetos, incluyendo
textos. Un ejemplo es el salario del cuerpo académico. Por un
lado el docente es responsable de la reproducción del conocimiento
y la universidad a través de la enseñanza, el apoyo a la conclusión
de estudios del alumnado o la realización de diversas actividades
académicas, como congresos o coloquios. Por el otro, está obligado
a producir conocimiento a través de la redacción de artículos,
capítulos de libros, ponencias o libros a tal ritmo que incluso
se conoce con la frase «publica o muere». Causa sospecha la relevancia
que ambos «bandos» han prestado a la productividad intelectual,
cuando su ritmo ha sobrepasado su capacidad y sus intereses han
sido subordinados a fines políticos o económicos ajenos al quehacer
cultural.

## 12. En la búsqueda de una definición

Una crítica común a la +++PI+++ por parte de quienes apoyan a
los +++BC+++ es su acaparación de legislaciones no equiparables
entre sí, lo que provoca ambigüedad y, por ende, una dificultad
en la gestión de la creación intelectual. Sin embargo, las últimas
cinco secciones exhibieron que los «bienes comunes» y la «libertad»
promulgadas por el _anticopyright_, _copyjustright_, _copyleft_
y _copyfarleft_ están supuestos en el discurso. Otro impedimento
es la carencia de una delimitación clara sobre la crítica a la
propiedad. En varios de los casos existe una sinonimia entre
«propiedad», «propiedad privada» y «propiedad intelectual». Si
bien las teorías de la +++PI+++ actuales la suponen como «propiedad
privada», esto no cierra la posibilidad de una teoría que se
refiera a «propiedad pública» o a «propiedad en común». Además,
el término «bienes comunes» en muchos casos se usa como sinónimo
de alguna clase de propiedad.

Según Hughes (1988) la +++PI+++ puede entenderse como «bien común
potencial». Para Epstein (2009) la +++PI+++ robusta es un «anticomún».
Los +++BC+++ se entienden como consecuencia de la expiración
de la +++PI+++ (Hughes, 1988) o como el punto de partida «en
común» que permite el surgimiento positivo de la propiedad, según
la teoría laborista (Moore, 2012), lo que sugiere que estos se
comprenden a partir de su nexo con la +++PI+++ y viceversa. A
continuación se expone un marco que permite observar sus relaciones
a partir de los «anticomunes», «bienes no-comunes», y «bienes
comunes».

Los «anticomunes» son creaciones intelectuales cuyo costo o configuración
jurídica restringe su acceso. Aquí se encuentra la «+++PI+++
robustecida». El crecimiento cuantitativo y cualitativo de la
protección a la +++PI+++ genera un aumento en los costos de transacción
para su acceso hasta ser inaccesibles para la mayoría de la población.
Los teóricos de la +++PI+++ y los autores a favor de los +++BC+++
revisados aquí asienten en que este engrosamiento agrava el problema
sobre el control que deberían tener los creadores y distribuidores
sobre los bienes culturales. Un ejemplo son las patentes o los
derechos de autor cuyo precio impiden su reproducción, como la
implementación de una tecnología o la cesión de derechos para
la traducción o reedición de obras. Otro ejemplo son los eventos
culturales que por sus costos es imposible la asistencia de la
mayoría de la población. También yacen aquí las «+++PI+++ secreta»
y «+++PI+++ indefinida». Un ejemplo del primero son los secretos
comerciales que por contrato se impide su difusión pública. El
último caso son las legislaciones que no establecen un plazo
de vencimiento o que permiten su renovación ilimitada como son
los derechos morales, las denominaciones de origen y las marcas.
El «anticomún» no es una categoría estática, sino relativa al
poder adquisitivo o las formas jurídicas. En América Latina el
pago mensual por _software_ propietario no es tan viable a diferencia
de Estados Unidos o Europa, lo que incentiva la piratería. En
México los derechos morales son inalienables mientras que en
Canadá son renunciables.

Los «bienes no-comunes» suponen la posibilidad de acceso con
una transacción asequible o sin un intercambio económico o petición
de uso, pero su control está en dominio del creador, el distribuidor
o un propietario. Aquí se coloca la «+++PI+++ no prohibitiva»,
cuyo costo es asequible para una buena porción de la población.
Ejemplos son los eventos culturales de bajo costo como la muestra
de la Cineteca Nacional, los espectáculos callejeros de cooperación
voluntaria, las campañas de fomento a la lectura que regalan
libros y cualquier evento o producto intelectual subsidiado.
La «propiedad pública» es otra forma de bien no-común. Su gestión
corre a cargo del Estado en supuesto beneficio para sus comunidades.
No es un bien común porque requiere la autorización o el pago
al Estado, a modo de cuotas o impuestos, para su acceso. La disponibilidad
del acervo de museos, archivos o universidades, así como el acceso
a eventos culturales gubernamentales, son unos ejemplos. Por
último, la «+++PI+++ descatalogada» es un bien cultural que no
se encuentra disponible en ningún comercio ni archivo o biblioteca
públicas, lo que imposibilita su acceso, al menos que un propietario
lo permita. La digitalización de obras localizadas en bibliotecas
privadas es un caso.

Del otro extremo están los «bienes comunes», la «propiedad (en)
común» es un sinónimo si se comparte el supuesto de que la creación
es propiedad. El acceso a estos bienes no está determinado por
costos de transacción o por el control exclusivo de sus creadores.
Esto no excluye que su acceso esté limitado a la infraestructura
cultural o a cuestiones geopolíticas. Este espectro abarca varios
tipos de propiedad que no son _tratados_ como propiedad privada
ni pública.

La forma más común es el «dominio público», el cual no es propiedad
pública porque no son bienes en manos del Estado, sino que este
vela para su efectivo traslado de la +++PI+++ a los +++BC+++.
Ejemplos en México tenemos toda obra literaria o artística de
creadores que fallecieron _al menos_ hace cien años, como los
poemas de Sor Juana Inés de la Cruz o, en el caso _más contemporáneo_,
los textos de Amado Nervo. Esto evidencia que este dominio, al
igual que la +++PI+++, debe su existencia al Estado. Por las
instituciones gubernamentales los creadores tienen la autorización
de ejercer un monopolio temporal sobre su creación. A su término,
estas tienen la obligación de migrarlas al dominio público para
su expansión, al mismo tiempo que consolida la presencia del
Estado. En este sentido se entiende porque Hughes (1988) ve los
+++BC+++ como consecuencia de la expiración de la +++PI+++, ya
que los reduce al dominio público hasta el punto de catalogar
la +++PI+++ como «bien común potencial».

La lucha del _anticopyright_ llama a la violación de las restricciones
impuestas a las creaciones intelectuales. Así que otras propiedades
insertas en los +++BC+++ es el conjunto de la «+++PI+++ en disputa».
Su ubicación en este espectro está definida por el uso y no por
la autorización de la legislación vigente, donde al menos existen
dos tipos: la huérfana y la donada.

En la «+++PI+++ huérfana» la creación intelectual aún no está
en dominio público pero no es posible la localización del titular
de sus derechos. Este fenómeno es recurrente en los derechos
de autor, ya que la extensión de su protección abarca por lo
menos cincuenta años para los países firmantes del Convenio de
Berna (+++OMPI+++, 1971), aunque la mayoría de los Estados signatarios
han establecido una duración mínima de setenta años o en los
casos más extremos, como el mexicano, de cien. Una diferencia
de veinte o veinticinco años implica la herencia de la titularidad
a la generación siguiente. La protección por cincuenta años hace
que los derechos sean transmitidos del autor a sus nietos; una
de setenta años, a los bisnietos y una de cien, a sus tataranietos.
Con el fin de dejar un medio de sustento se justificó la extensión
_post mortem_ de los derechos de autor (Baldwin, 2014). Al buscar
el permiso para la reproducción de la obra cada vez es más común
el desconocimiento del titular de los derechos. Ante esta limitante,
ciertas personas o instituciones la utilizan como si estuviese
en dominio público, aunque legalmente sigue bajo protección.
Diversos gobiernos han empezado a solventar este problema al
emitir legislaciones que facilitan su uso, como la Unión Europea,
Inglaterra y Australia (Viollier, 2017). La European Union Intellectual
Property Office (2019) cuenta con una base de datos al respecto
disponible para su consulta pública.

La «+++PI+++ donada» es una creación intelectual disponible que
no está en dominio público pero cuyo titular ha consentido en
su uso gratuito. Las obras con licencias libres o abiertas son
un ejemplo, estas tienen derechos de +++PI+++, aunque permiten
su utilización no exclusiva. Un área gris de estas licencias
es la ausencia de legislación que determine su perpetuidad o
caducidad. Esto permite que con el tiempo el titular de los derechos
opte por una gestión tradicional, como suele suceder entre los
herederos que no siguen la voluntad del autor de poner su obra
en disposición pública. Otro ejemplo serían las obras sin mención
explícita de derechos reservados o licencias. Debido a esta falta
de legislación o la ambigüedad en cuanto a las posibilidades
de uso, la +++PI+++ donada carece de certeza jurídica en vida
y _post mortem_ (Zhenya, 2017).

Estos tipos de propiedad no son homogéneos y su categorización
es conflictiva. Los +++BC+++ como punto de partida o consecuencia
legal de la dilución de la +++PI+++ simplifica la flexibilidad
y la complejidad del «campo de guerra» en el que «combaten» los
simpatizantes de la +++PI+++ y los +++BC+++. Además, la insistencia
en que los +++BC+++ son la solución a las dificultades generadas
por las legislaciones de la +++PI+++, o el ecosistema ha defender
de su constreñimiento, invisibiliza los múltiples encuentros
entre estos «bandos». La constitución de la creación intelectual
como propiedad no es un traslado sencillo ni directo. Este esquema
intenta mostrar que la falta de delimitación de conceptos es
común en las teorías de la +++PI+++ y las críticas de los +++BC+++,
lo que demuestra la necesidad de un ejercicio teórico que busque
una comprensión más amplia de este problema.

Los consensos entre ambos «bandos» son en (1) la creación intelectual
a modo de propiedad, (2) el uso, disponibilidad y reproducibilidad
como las características más relevantes de la creación y (3)
un terreno en disputa que comprende dos polos: la +++PI+++ y
los +++BC+++. Además, sin mención expresa y en detrimento de
las intenciones del _copyfarleft_, esta configuración permite
la acumulación de capital y sostiene la relevancia de la autoría,
sea para su custodia, reniego o donación al «bien común».

¿Por qué la «lógica capitalista» siempre parece beneficiarse
del robustecimiento o mantenimiento de las legislaciones, la
defensa al libre mercado del código abierto o la cultura libre,
el apoyo al acceso abierto del conocimiento, la precariedad de
las comunidades _hackers_ y de activistas en resistencia o la
prolongación de la explotación laboral denunciada por el movimiento
anti-996? ¿Por qué la autoría, la atribución y otras categorías
no están bajo disputa entre ambos «bandos»? ¿Cabe la posibilidad
de plantear alternativas por medio de una crítica a sus puntos
de encuentro?

# Puntos de encuentro

## 13. Del problema de la interpretación al de la teorización

La justificación de la +++PI+++ a través de Locke, Kant y Hegel
o a partir de teorías utilitaristas demuestran un interés de
dar fundamentos filosóficos. Sin embargo, como ha podido observarse
en las secciones 1--6, existen una serie de inconvenientes en
el empleo de cada uno de estos filósofos.

La primera dificultad es la fragmentación teórica con el fin
de justificar la +++PI+++. La teoría de la propiedad de Locke
no fue elaborada para defender la propiedad intangible: su objetivo
fue el paso del derecho natural al derecho positivo de la tangible
propiedad privada. Elaboraciones posteriores han _interpretado_
la teoría lockeana para justificar la +++PI+++, como se mencionó
en la sección 5, sin importar que Locke no creyera que el régimen
de la propiedad fuera aplicable al trabajo intelectual (Baldwin,
2014). Este traslado genera una visión individualista basada
en la «cultura del esfuerzo». Sin embargo, su efectivo cumplimiento
se da a través de instituciones estatales, cuyos principales
beneficiarios ---en cuanto acumulación de capital y ampliación
de la capacidad jurídica--- no son los creadores, sino las instituciones
o las empresas que por medio del contrato de obras por encargo
o del uso de los bienes de las «economías del regalo» aseguran
la titularidad de los derechos o disminuyen sus costos de producción
a la par que mantienen o aumentan sus precios de distribución.

La fragmentación de las teorías kantianas o hegelianas se da
por un uso que no toma en cuenta sus objetivos o consecuencias.
Ambos autores se preocuparon por la propiedad literaria y su
lugar dentro de sus filosofías. En Kant hay una defensa al escritor
donde su creación no es asimilada como propiedad. Esta es la
voz del autor por lo que deviene en el derecho a la libertad
de expresión. Para Hegel la propiedad forma parte de una dialéctica
allende al individuo. El objetivo no fue su defensa a perpetuidad,
sino su protección para determinado momento histórico. En la
sección 4 se vio que ulteriores _interpretaciones_ de Kant o
Hegel defienden el trabajo del autor como propiedad perenne.
Una consecuencia fue la constitución de los derechos morales,
los cuales defienden la creación literaria como propiedad inalienable
y cuya génesis se sitúa en las discusiones decimonónicas de juristas
franceses y alemanes (Baldwin, 2014). Con el Convenio de Berna
estas legislaciones se implementaron en el resto del mundo y
hoy en día están presentes, por ejemplo, en la «Ley Federal del
Derecho de Autor» (1996) del Estado mexicano.

La teoría progresista sigue fines socioeconómicos: la generación
de la mayor utilidad posible. En la sección 3 se indicó la manera
en como las _interpretaciones_ de teorías generales utilitaristas
fundamentan el caso particular de los derechos de +++PI+++. A
pesar de que estas teorías protegen la creación intelectual,
se tiene el efecto de trasladar gran parte de la plusvalía del
creador al distribuidor de bienes culturales.

El uso de Locke, Kant y Hegel puede considerarse fragmentario
y equívoco por sus condiciones ---la decisión sobre las partes
a considerar--- y sus resultados ---el carácter ecléctico o el
uso forzado de estos filósofos---, pero también muestra un problema
teórico de fondo. Las teorías de la +++PI+++ de manera intencional
redujeron sus fundamentos filosóficos a fragmentos ---por lo
general descontextualizados--- de un puñado de filósofos europeos
modernos, al mismo tiempo que sus suposiciones distan de los
supuestos teóricos de las filosofías empleadas. Aún están pendientes
más análisis al respecto.

Un problema similar se percibe entre los críticos de la +++PI+++
que apuestan por los +++BC+++, los cuales se analizaron en las
secciones 7--12. La dificultad teórica es más notoria, no por
su carencia de rigurosidad, sino por el poco de interés en legitimar
el discurso a partir de autoridades filosóficas que forman parte
del canon occidental. Algunos inconvenientes son la ambigüedad
en la extensión y definición de los +++BC+++; la falta de consenso
en la oposición a la +++PI+++, como es patente en las posturas
del _anticopyright_, _copyjustright_, _copyleft_ y _copyfarleft_,
y la crítica insuficiente en torno a ciertas categorías fundamentales
para sus argumentos como los conceptos de «autoría», «libertad»,
«apertura», «acceso», «atribución», «apropiación» u «originalidad».

Una explicación para estas dificultades va en torno a la búsqueda
de autonomía de cada organización o persona. En este ecosistema
se defiende la diferencia como estrategia para dar diversidad
y fuerza al movimiento en pos de los +++BC+++. Pese a que en
el discurso varios actores se opongan y distancien, su activismo
destaca por la cohesión, aunque por lo general se establezcan
alianzas para organizar campañas o eventos en lugar de planear
estrategias a largo plazo. Esto puede considerarse un fenómeno
sociopolítico. Sin embargo, la ausencia de teorías consistentes
o de reflexiones que cuestionen los cimientos de sus discursos
retraen las posibilidades para los modelos económicos y políticos
alternativos que anhela este espectro del quehacer cultural.

Según Hall (2016), estos problemas teóricos tienen sus raíces
en el desafío que se hace a la «dictadura» de lo humano que pretende
enfatizar lo no-humano, poshumano o posantropocéntrico. No obstante,
sus discursos y las políticas de su producción cultural tienden,
por un lado, a apoyarse en dispositivos jurídicos que alimentan
el modelo neoliberal de «emprendurismo académico». Por el otro,
se valen de la tradición del «humanismo liberal», por ejemplo,
la importancia a la autoría, la excelencia del libro como soporte,
el anhelo de originalidad fomentado a cada paso o la fijeza del
producto elaborado por el quehacer humanístico. A este respecto,
también se pueden añadir otros dos casos: el hábito del lector
de humanidades de ir del autor a la obra y el empleo de etimologías
para resignificar o acotar conceptos centrales.

La poca rigurosidad en el empleo de fragmentos escritos por Locke,
Kant y Hegel es un problema. Sin embargo, su señalamiento corre
el peligro de desviar la atención hacia los usos legítimos según
determinadas reglas hermenéuticas o del discurso, en lugar de
cuestionar los argumentos articulados por los teóricos de la
+++PI+++. Estos han centrado su fundamentación bajo los ideales
de una modernidad occidental, pese a que el mayor beneficio no
han sido para los creadores o su «liberación». Entonces, ¿será
que no es posible fundar la +++PI+++ afuera de esta modernidad?

Al mismo tiempo, existen categorías que no están en el centro
de la disputa y en cuya omisión son perceptibles ciertas semejanzas.
Esta tensión sugiere que el problema teórico de fondo es la tentación
de teorías culturales distantes a la modernidad o lo humano.
¿Cómo serían posibles? Se trata de una dificultad que se despliega
al tener, por un lado, las autodenominadas teorías de la +++PI+++
y su incapacidad de ir allende a la modernidad occidental y,
por el otro, los discursos en pos de los +++BC+++ y su recurrencia
paradójica a las formas humanísticas que pretenden minorizar
o negar.

## 14. El pacto para una alianza ¿tripartita?

El problema en la fundamentación de la +++PI+++ parece tener
sus raíces en problemas de mayor envergadura, motivo por el cual
existe cierta falta de claridad sobre la pretensión de distintas
personas en justificarla o rechazarla, como ha sido perceptible
en esta investigación, principalmente en las secciones 3--5 y
6--11. Las diferencias entre los teóricos de la +++PI+++ y los
defensores de los +++BC+++ analizados son perceptibles. Sin embargo,
también son visibles ciertos paralelismos. Por ejemplo, en ambos
«bandos» muchas veces hay una renuencia a cuestionar si la autoría
es relevante según las posibilidades de gestión de derechos y
las capacidades técnicas de producción, reproducción, distribución
y conservación (+++PRDC+++) de bienes culturales. El cuestionamiento
fundamental no es si la autoría debe o puede tener un papel menos
relevante en el quehacer cultural, sino por qué esta se concibe
como indispensable para esta actividad.

Otra similitud es el reduccionismo jurídico. El debate sobre
el quehacer cultural ha delimitado la +++PRDC+++ a una cuestión
de derecho según las legislaciones vigentes o las licencias de
uso avaladas por una entidad estatal o por organizaciones con
capacidad operativa a nivel global, como la +++FSF+++, la +++OSI+++
o +++CC+++. En otros términos, el producto cultural, como un
libro, se constituye en contrato: un objeto de naturaleza jurídica
dentro de un campo de acción cuyos actores lo suponen desde un
raigambre cultural o político y, después, con un carácter jurídico-legislativo.

Una semejanza adicional es el fetichismo de la mercancía. En
las teorías de la +++PI+++ examinadas hay un interés por el producto
y la economía que fomenta, además de la conservación de la productividad.
La disputa es sobre quién custodia los derechos y la plusvalía
de la +++PRDC+++ y hasta qué punto deben ser regulados por el
Estado a través de las garantías ofrecidas en el contrato o por
medio de su consecuente traslado al dominio público. Esto supone
una dependencia de la economía del quehacer cultural hacia la
constante producción de bienes, siendo necesario afinar los mecanismos
para incentivar, recompensar o compensar el trabajo hecho por
productores y reproductores.

Entre los actores a favor de los +++BC+++ el producto por lo
general hace patente posturas políticas e ideales respecto a
la sustentabilidad del quehacer cultural. Se trata de una «economía
del regalo» que tiende a invisibilizar la precariedad económica,
el exceso de trabajo de mantenimiento ---muchas veces llevado
a cabo por mujeres--- y la necesidad de dobles jornadas laborales,
además del estrés, las decaídas anímicas y el desgaste físico
que conllevan. La organización y atención se dedican al cuidado
del producto, no tanto en su materialidad, sino en su significado
político y pedagógico ---las enseñanzas para otras formas de
organización---. Semejante objeto puede ser un soporte concreto,
como un libro, una pieza artística o un programa de cómputo,
o un espacio, como _hackerspaces_ o cooperativas, o bien, el
tiempo necesario para eventos o talleres.

Una afinidad emparentada a la anterior es la percepción de que
la producción es propiedad. Para los teóricos de la +++PI+++
citados es una propiedad privada intangible que al paso de los
años el Estado la transforma en propiedad pública. Para los defensores
de los +++BC+++ es una propiedad en común desde su concepción.
Aunque de manera reciente se han empezado a percibir con ausencia
de lo «propio»; es decir, estos dejan de pertenecerle a todos,
para ser de nadie (Hall, 2016).

Estas similitudes requieren un análisis filosófico que va más
allá de esta investigación. Sin embargo, estas semejanzas dan
pie a pensar en pactos velados e inconscientes, pese a la autoconcepción
de un estado de «guerra». Ninguno de los «bandos» cuestiona con
profundidad la relevancia del autor, el contrato y el producto.
Una manera de comprender este fenómeno por medio de la bibliografía
consultada es la idea en común de un «marco teórico» que pretende
explicar la +++PRDC+++ de los productos culturales.

La +++PRDC+++ de los bienes culturales se ha configurado en tres
esferas. Una es la del creador que, mediante una relación de
parentesco, considera los productos de su actividad como creación
de su propiedad. El parentesco se da a partir de la erotización
de la apropiación (Schroeder, 1998) ---acto que «da a luz»---
o de su romantización (Baldwin, 2014) ---creación _ex nihilo_---:
concepciones metafísicas respecto a la producción. Esto se considera
la manera de hacer cultura, aunque en realidad sea una simplificación
o distorsión de la producción cultural (Benjamin, 2016; Foucault,
1999). Esta capacidad creativa única e irrepetible, y los tiempos
necesarios para su ejercicio, son las principales defensas para
salvaguardarla. Históricamente las primeras personas «creadoras»
fueron las dedicadas a la escritura, con el tiempo se incluyeron
a artistas e inventores, así como hubo actividades cuya creatividad
se cuestionó, por ejemplo, la labor de traductores e intérpretes
(Baldwin, 2014).

Por otro lado, la esfera del distribuidor en general tiene tres
objetivos garantizados por el contrato. Primero, produce un soporte
para el trabajo del creador según las capacidades técnicas y
las normas de presentación _ad hoc_ a las expectativas de cada
disciplina. Este puede ser un libro, un archivo digital, un concierto
o cualquier otro dispositivo que _da soporte_ a la divulgación
pública de la obra. Segundo, determina las posibilidades legales
de su reproducción y, por último, delimita los mecanismos y alcances
de la distribución. La cantidad de recursos humanos, económicos
y políticos para cumplir estos objetivos es el argumento más
relevante para la conservación de esta esfera. Vale la pena una
matización: hay una tendencia a la división del trabajo. Por
un lado están los reproductores, como los productores de música
o de películas, las compañías de videojuegos o las editoriales,
cuyos objetivos es la transformación de lo creado en un producto
reproducible y usable para el público. Además, hay una subespecialización
entre los que diseñan y los que manufacturan, por ejemplo, el
editor y el impresor. Por el otro lado están los agentes que
colocan el producto en el mercado, como los distribuidores de
libros, de música o de películas o las plataformas de venta como
Google Play, Amazon, App Store o Steam.

La última esfera es la del «público» que ejerce por lo menos
dos funciones. Primero, mediante el consumo o la donación alimenta
esta economía cultural, aunque su accesibilidad quede restringida
a los usos permitidos por las otras esferas. Segundo, esta esfera
visibiliza o entierra proyectos culturales a través de la crítica.

Cada esfera consiste en actores con diferentes funciones que
hacen del quehacer cultural una economía basada en la oferta
y la demanda. En este contexto existen varias propuestas, tanto
de teóricos de la +++PI+++ como de quienes apoyan los +++BC+++,
que se han mencionado en las primeras doce secciones. Por ejemplo,
leyes que acoten la esfera del distribuidor, como las reformas
pretendidas por el _copyjustright_. Su contraparte es el cabildeo
de dicha esfera para implementar cambios a favor de sus intereses,
como el robustecimiento de la +++PI+++.

Otra sugerencia es ampliar la esfera pública. En gran medida
los movimientos del _copyleft_ y _copyfarleft_ han apostado por
el incremento de las libertades _de uso_ para el público. El
_copyleft_ supone que la industria encontrará la manera de adecuarse
a esta amplificación. El _copyfarleft_ pretende que esto produzca
una economía no capitalista que se centre en los _productores_
y en sus necesidades de _subsistencia_ y organización. El polo
opuesto es la preocupación de que esto mine la democracia y la
libre expresión en un contexto donde la esfera pública es fácil
de persuadir, como el caso de Cambridge Analytica.

Por último, la esfera creativa ha impulsado políticas que favorezcan
sus intereses. Este fenómeno es paradójico. Los creadores, los
teóricos de la +++PI+++ y los defensores de los +++BC+++ han
resaltado la precariedad en el que viven la mayoría de los creadores,
aunque sean la fuerza de trabajo para la generación de plusvalía
---como el «cognitariado» de Berardi (2003)---. Por ello, deberían
tener un mayor control sobre _su_ obra o la plusvalía tendría
que repartirse de manera más equitativa. Sin embargo, este impulso
por lo general beneficia a las instituciones responsables de
esta insuficiencia. Se hace hincapié en el producto o en la riqueza
que genera, pero no en la infraestructura industrial necesaria
que yace en manos del distribuidor.

Entre ambos «bandos» de esta «guerra» no se encontraron motivos
que justifiquen la omisión de otra esfera: el Estado. A través
de las legislaciones de la +++PI+++ este garantiza la labor creativa
como una producción íntima de propiedad. Por el contrato las
instituciones estatales respaldan las relaciones comerciales
entre creadores y distribuidores para la «apertura» pública de
la creación. Por último, esta esfera avala las posibilidades
de uso justo de la +++PI+++, desde el establecimiento de normas
comerciales ---como el precio único en los libros o las leyes
antimonopolio---, pasando por excepciones jurídicas ---como el
uso sin autorización para fines de investigación o de educación,
o para ampliar la distribución a comunidades vulnerables---,
hasta el compromiso de velar por el uso legal del dominio público.

Esta esfera arbitra al resto y en su camino fortalece su presencia
en el quehacer cultural. No es un mero nexo entre las esferas
que son el centro reflexivo de los teóricos o los críticos expuestos
en esta investigación. Los subsidios o los impuestos son los
mecanismos más evidentes para incrementar la influencia estatal
en la +++PRDC+++. Sin embargo, la génesis del _copyright_ permite
ver otras formas en como las instituciones del Estado se fortalecen
mediante los derechos de +++PI+++ al mismo tiempo que constituyen
las posibilidades de acción de estas esferas. Por ello es lícito
describirla con brevedad, ya que permite cuestionar este marco
teórico conformado por esferas.

## 15. El surgimiento de las esferas del distribuidor y del público

El quehacer cultural delimitado por las esferas del creador,
el distribuidor, el público y el Estado es un supuesto «marco
teórico» compartido por los defensores y los críticos a la +++PI+++,
como se explicó en la sección pasada. Un punto de partida analítico
es posible a partir de un breve recorrido por la historia del
_copyright_. Acorde a Loewenstein (2002), el Estatuto de la Reina
Ana tiene sus antecedentes en el cabildeo de la Honorable Compañía
de Impresores y Periódicos ---una organización de gremios dedicados
a la producción de libros en Londres--- para tener derecho de
las obras a perpetuidad. Según esta, el pago a los escritores
implicaba la alienación de su propiedad. La exigencia de la Compañía
era porque impresores escoceses reproducían sus ediciones sin
consentimiento (Baldwin, 2014). Para los londinenses esta empresa
era ilegal, desleal y contraproducente a la industria debido
a la «mala» calidad y la falta de regulación (Loewenstein, 2002).
Es decir, el _copyright_ surge de una disputa entre impresores
mediada por el Estado.

El monopolio del mercado libresco en el mundo anglosajón era
permitido para la Compañía por la Corona inglesa desde el siglo
+++XVI+++ como control industrial y comercial de las publicaciones
(Loewenstein, 2002). Sin embargo, en el siglo +++XVII+++ esta
regulación cayó en desuso hasta que a principios del siglo +++XVIII+++
la Cámara de los Comunes, con un fuerte carácter antimonopólico
e independentista, rechazó este privilegio (Loewenstein, 2002).
Así nacieron las leyes de _copyright_ ---el estatuto tiene el
nombre de la monarca en turno---. Negar este monopolio disminuyó
el poder de los impresores londinenses, restringió facultades
de la Corona respecto al comercio y legitimó la función política
y jurídica de la Cámara de los Comunes.

Los bajos precios de los impresores escoceses les permitió competir
contra la Compañía y les facilitó llegar a un nuevo mercado:
Estados Unidos. La infraestructura de este nuevo Estado no daba
abasto a sus lectores, por lo que su alfabetización se dio a
partir de estos impresores hasta que existieron las condiciones
para el abastecimiento local (Baldwin, 2014). En este proceso,
Estados Unidos readaptó el Estatuto de la Reina Ana para sus
leyes de _copyright_. Así protegía su industria de los escoceses,
quienes habían comenzado a piratear ediciones estadunidenses
(Loewenstein, 2002; Baldwin, 2014).

En Francia la situación fue similar a la de Reino Unido. El privilegio
real fue concedido a los impresores parisinos en contra de la
pujante industria de Lyon que, entre sus actividades, reproducían
sus ediciones (Baldwin, 2014). Sin importar la oposición de ciertos
escritores, como Proudhon (1862), en la Francia del siglo +++XVIII+++
se implementó una legislación similar al _copyright_: los derechos
de autor. Entre los países europeos, el caso alemán fue el más
tardío debido a la fragmentación y descentralización de su organización
política (Baldwin, 2014). Después de Francia, Alemania promulgaría
derechos de autor. Hasta aquí las legislaciones de Reino Unido,
Estados Unidos, Francia y Alemania no presentaban grandes divergencias
(Baldwin, 2014). Durante el siglo +++XIX+++ se dará un distanciamiento
entre el _copyright_ y los derechos de autor, cuyas diferencias
se mencionaron en la sección 4.

Desde el siglo +++XVI+++ los reinos de España y Portugal fomentaron
el monopolio en la +++PRDC+++ de libros a partir de impresores
autorizados (Lafaye, 2002). A diferencia del mundo anglosajón,
galo o germánico, el control y la censura real continuó hasta
el siglo +++XIX+++. Pero como Lafaye (2002) informa, pese a su
constante presencia, principalmente a través de la Inquisición,
en estos reinos se imprimieron obras censuradas, apócrifas o
piratas. Las autoridades, en cohecho con libreros e impresores
no autorizados o por negligencia o disimulación, permitieron
este mercado negro. En el mundo hispanohablante hubo una carencia
de adaptación jurídica a las nuevas posibilidades técnicas de
reproducción de textos. En parte esto explica por qué los derechos
de autor en Hispania y América Latina son reformulaciones de
leyes elaboradas en Reino Unido, Estados Unidos, Francia y Alemania.

Entonces, antes del siglo +++XIX+++ se tienen legislaciones en
torno a los impresos muy similares entre los países europeos
que migraron de las formas jurídicas monárquicas. Las leyes de
_copyright_ inglesas son relevantes porque detonarán legislaciones
entre estas naciones. Pero ¿qué tiene que ver con las tres esferas
que han funcionado como marco teórico para las teorías de la
+++PI+++ y las críticas en pos de los +++BC+++ expuestas en esta
investigación?

El traslado de privilegios reales a legislaciones estatales generó
una reacción en cadena que configuró las esferas contemporáneas
del público, el distribuidor y el creador. La esfera pública
de bienes culturales carecía de forma jurídica definida. Su constitución
era a partir del comercio entre libreros y ropavejeros con determinados
sectores de la población (Lafaye, 2002). Este «público» no incluía
a campesinos, obreros, mujeres o niños: su incorporación es un
proceso posterior y paulatino (Bonfil _et al._, 2001). Además,
las leyes permitieron una regularización a la pujante industria
de los libros. Antes de ello, la reproducción de textos rara
vez se detenía ante concesiones jurídicas, pese a los peligros
que implicaba (Lafaye, 2002). La adecuación entre las técnicas
de reproducción y los marcos jurídicos comenzó en este traslado.
Por otro lado, el creador era parte de la esfera del impresor,
donde se le pagaba o daba un dote para realizar su trabajo. En
términos actuales, el autor era quien realizaba una obra por
encargo. Además, cabe resaltar que la publicación de autores
vivos no eclesiásticos ---por doctrina el sirviente de la Iglesia
no podía reclamar autoría--- era menor a la de autores muertos
(Lafaye, 2002), por lo que la apropiación del texto empezó a
ser una disputa cuando aumentó la cantidad de autor vivos.

Por este motivo, Loewenstein (2002) y Chartier (1999) critican
algunas inexactitudes en _¿Qué es un autor?_ de Foucault. Acorde
a Loewenstein (2002), cuando el escritor publicaba con un impresor
autorizado o el libro no se consideraba blasfemo, la apropiación
penal que menciona Foucault no existía. La atribución no implicaba
apropiación ni censura, así como antes del Estatuto no hay evidencia
clara donde la atribución de un texto conlleve su apropiación.
Loewenstein (2002) desea resaltar que el surgimiento de la autoría
como positividad e individualización del escritor tiene que contemplar
la historia del desarrollo tecnológico, económico y jurídico
en torno al libro, la imprenta y el mercado capitalista. Por
ello, los orígenes de la autoría no son reducibles a la apropiación
penal debido a la presencia de la apropiación comercial por parte
de libreros e impresores.

Para Chartier (1999), Foucault se equivoca en la datación del
autor como propietario a finales del siglo +++XVIII+++. La autoría
contemporánea surge a principios de ese siglo en Reino Unido
por el Estatuto de la Reina Ana. Esto no desecha la «función-autor»
elaborada por Foucault, sino que la depura y retrotrae la individualización
y la valoración positiva del escritor como propietario a la exigencia
de la Compañía y al Estatuto que hubo en consecuencia. Es decir,
el autor contemporáneo se originó a partir de una disputa entre
impresores y una institución estatal y no en una función transgresiva
del discurso del escritor.

La Compañía pretendía una legislación que les concediera derechos
a perpetuidad sobre obras literarias. Se defendió el derecho
del escritor sobre su trabajo ---siguiendo muy de cerca la teoría
de la propiedad de Locke, explicada en la sección 5--- para argumentar
la cesión a perpetuidad (Baldwin, 2014). Así como un granjero
es propietario de sus cultivos, el escritor lo es de sus obras,
porque en ambos casos se trata de productos del esfuerzo. Pero
así como el granjero pierde los derechos sobre su producción
debido a la venta, el autor los cede por la compensación económica
del librero o impresor. Es decir, la obra es por completo alienada
al ser vendida. Esto encendió la alerta roja en la Cámara de
los Comunes. Esta concesión le daba más poder a la Compañía porque
extrapolaba un privilegio real dentro de un nuevo contexto jurídico
en el cual la Cámara se jugaba su legitimidad como institución
independiente a la monarquía.

La Cámara interpretó la disputa entre impresores londinenses
y escoceses como una lucha por el control de la +++PRDC+++ de
libros en la que el Estado debía intervenir. Por este motivo,
la propuesta original de la Compañía sufrió una _pequeña_ modificación.
En efecto los impresores tenían derecho sobre las obras que pagaban,
pero este no sería perpetuo, sino que duraría catorce años _después
de la publicación_, además de requerir de un registro para avalarlo.

Esta _pequeña_ modificación tuvo una serie de consecuencias.
La más inmediata fue la oposición de la Compañía al Estatuto
de la Reina Ana. Durante varios años existió una discusión entre
políticos e impresores, cuya consecuencia fue una extensión adicional
de catorce años (Loewenstein, 2002; Baldwin, 2014). Desde el
siglo +++XVIII+++ se tiene un constante vaivén de modificaciones
cuantitativas y cualitativas al _copyright_ y los derechos de
autor que, en la actualidad, han llevado a una protección inherente
de por lo menos setenta años _después de la muerte del autor_.

Los cambios en la legislación de la +++PI+++ han sido impulsados
para el beneficio de la esfera del distribuidor a través de un
diálogo con instituciones estatales y en ocasiones con la complicidad
de gremios de escritores ---como el caso francés (Proudhon, 1862)---.
El traslado de un privilegio a una legislación en un primer momento
afectó los intereses comerciales de _ciertos_ impresores. De
ahí y hasta el siglo +++XIX+++ la historia del _copyright_ y
de los derechos de autor consiste en disputas para revertir y
reconfigurar las formas jurídicas que controlan la +++PRDC+++
a favor de los distribuidores. Después del siglo +++XIX+++ estas
legislaciones dejarán de afectarlo, de manera similar a como
los privilegios reales beneficiaron sus intereses. Así como los
avances tecnológicos desafiaron la organización del trabajo protegida
por privilegios reales, en el siglo +++XX+++ las nuevas tecnologías
digitales de reproducción otra vez ponen bajo disputa el control
reganado por esta esfera.

Otra consecuencia fue la constitución del dominio público. El
Estatuo aseguró la existencia de los _commons_ para la producción
literaria. El paso de un privilegio a una legislación implicó
un traslado de un monopolio garantizado por la Corona a uno _artificial_
avalado por el Estado. La artificialidad consiste en la limitación
dada al _copyright_. El Estatuto permitió que en un determinado
momento la obra pase a formar parte de los +++BC+++. Esta _pequeña_
modificación dotó de forma jurídica a los _commons_ intangibles
y la esfera «pública» en la que descansan.

No obstante, el beneficio económico se reduce al sector de la
población que cuenta con la infraestructura industrial para el
reuso del material dispuesto en el dominio público. El distribuidor
es quien tiene la capacidad de +++PRDC+++ de textos, imágenes,
audio o video. Los +++BC+++, en su sentido jurídico, surgen de
una limitación impuesta y controlada por el Estado hacia los
distribuidores. Sin embargo, estas nuevas formas jurídicas terminan
por ser rentables para el distribuidor, sea como titular de derechos
o como usuario de los _commons_, para la generación de más +++PI+++;
por ejemplo, el uso de historias en dominio público o la compra
de franquicias que Disney lleva a cabo. Un fenómeno similar se
da en las licencias de uso. La flexibilidad hecha al _copyright_
y a los derechos de autor a través del uso de licencias no ha
sido por una negociación directa entre distribuidores, creadores
y el público, sino por una relación mediada por instituciones
gubernamentales.

En la búsqueda de dar mayores derechos al público se tienden
a obviar dos asimetrías. Por un lado, un «público» sin capacidad
de producción queda reducido a quienes cuentan con la infraestructura
afín. En un contexto donde el «público» carece de medios para
la +++PRDC+++, la defensa a sus derechos de acceso por lo general
también resguardan los derechos del distribuidor. Por otro lado,
el «público» no es homogéneo. Las esferas no limitan la posibilidad
de acción de los sujetos que las conforman. En varios casos hay
actores que son creadores, distribuidores o parte del público.
Quien cuenta con el interés, iniciativa y capacidad de diálogo
con el Estado ---para engrosar, reformar o suprimir legislaciones---
muchas veces son el «público» que forma parte de otras esferas.
La concepción de lo «público», aunque potente y flexible, tiende
a ser un reflejo en el que se cobijan autores y distribuidores
para la protección de sus intereses de +++PRDC+++. Esta defensa
requiere de estrategias para la generación de infraestructura
y así garantizar que la esfera pública no quede reducida.

La esfera estatal surge como figura relevante para el surgimiento
de la +++PI+++ y sus transformaciones. Sin sus funciones mediadoras
no sería posible las modificaciones legislativas de los derechos
de +++PI+++. Tampoco existiría la conformación de la producción
cultural como un marco según tripartita. Las formas jurídicas
que surgieron del Estatuto, y para la protección de la +++PRDC+++
de libros, limitó, delimitó y, por último, fortaleció a la esfera
del distribuidor, así como generó una esfera «pública» a la que
apela el discurso de creadores y reproductores. Es decir, al
menos en sus formas jurídicas y en sus desafíos actuales no hay
+++PI+++ o +++BC+++ sin la mediación de una cuarta esfera: las
instituciones del Estado.

Una consecuencia es que una teoría de la +++PRDC+++ de los bienes
culturales debe tomar en cuenta la batalla entre la +++PI+++
y los +++BC+++. Pero su fuerza argumentativa corre peligro si
no se toman en cuenta las características técnicas que envuelven
al quehacer cultural y las asimetrías presentes en la organización
de su infraestructura. Esto hace patente que la elaboración de
textos, el trabajo artístico y la producción científica ---lo
que se ha catalogado como «quehacer cultural»--- quedan delimitados
por tres factores, los cuáles son interrogados en la transgresión
de sus respectivas reglas de formación: las posibilidades abiertas
por las técnicas y las tecnologías de la +++PRDC+++, las protecciones
o las excepciones garantizadas por las instituciones estatales
y la apertura de horizontes teóricos y prácticos que acarrea
la crítica a determinadas formas jurídicas.

## 16. El surgimiento, la transferencia y la individuación de la esfera del creador

La constitución del Estatuto de la Reina Ana por parte de la
Cámara de los Comunes, la cual es una relaboración a la propuesta
realizada por parte de impresores londinenses para la defensa
de sus intereses, marca la pauta para la delimitación de las
esferas dentro del quehacer cultural. En la sección anterior
se describió el surgimiento de dos de ellas, la del distribuidor
y la del público. No obstante, una de las consecuencias más interesantes
de esta _pequeña_ modificación hecha por la Cámara fue la fundación
de la autoría contemporánea: su individuación a partir de la
transferencia de la apropiación. En la sección pasada se vio
que el autor no surge de manera exclusiva por la apropiación
penal, sino también a partir de las relaciones comerciales entre
escritores, libreros e impresores mediadas por instituciones
gubernamentales. Foucault (1999) indica que la autoría nace de
la capacidad transgresiva y punitiva del discurso. Sin embargo,
estas relaciones explicitan que la apropiación no siempre corresponde
a la descripción ofrecida por Foucault.

Además de la apropiación penal o legal (Foucault, 1999), existe
la comercial. El carácter diferencial de esta apropiación consiste
en que la atribución ha sido posible bajo el cumplimiento de
los marcos jurídicos previos al _copyright_ sin que el productor
se convierta en propietario. El «régimen de propiedad para los
textos» que menciona Foucault (1999) no se da a finales del siglo
+++XVIII+++, como bien indica Chartier (1999). El trato del discurso
como «un producto, una cosa, un bien» ha estado presente en la
+++PRDC+++ de textos incluso antes de la invención de la imprenta;
por ejemplo, en el caso fenicio de Biblos (Mark, 2009). Autores
como Kant (2005) verán del discurso un acto similar a la descripción
que hace Foucault (1999), pero este tratamiento no es habitual
entre quienes lo materializan con las técnicas existentes.

El estudio del discurso, que Foucault (1999) indica al final
de _¿Qué es un autor?_, debe tomar en cuenta las técnicas de
producción y reproducción, así como los mecanismos de distribución
y conservación, que lo hacen posible. En la apropiación comercial
hay un ecosistema donde la atribución al escritor es posible
sin implicar una apropiación por su parte y, con ello, _tal vez_
maneras distintas de producir y recibir el discurso. Esta investigación
no pretende analizarlas, aunque falten investigaciones al respecto.
La tarea pendiente es: _¿qué importa cómo se reproduce el habla?_,
_¿qué importa el soporte con el que se recibe?_, o siendo más
puntual, _¿cuál es la relevancia de la materialidad del discurso
filosófico para su recepción?_

Foucault (1999) describe cuatro características, que cataloga
como insuficientes, del «discurso portador de la función-autor»:

1. El traslado o desplazamiento de la apropiación penal a la legal
   y el discurso que empieza a tener autor. Es decir, la determinación
   del discurso por las formas jurídicas vigentes.
2. Los mecanismos cambiantes o ausentes de la atribución según
   el contexto. Es decir, la variabilidad de la atribución.
3. La falta de espontaneidad de la atribución, por la que se
   da «según las épocas y los tipos de discurso». Es decir,
   la incapacidad del productor de atribuirse un discurso.
4. La remisión egoica del texto la cual lleva «signos que
   remiten al autor» en una pluralidad de egos. Es decir,
   la falta de un nexo entre el discurso y un individuo real.

Otra consecuencia de la apropiación comercial es la imposibilidad
de un nexo inherente que haga posible la atribución como apropiación.
Existen saltos cualitativos donde la atribución de un discurso
pasa a referirse a su apropiación y, de ahí, que esta sea relativa
al productor. Antes del Estatuto, la idea del autor como propietario
tenía poco peso en la discusión sobre la apropiación del texto.
Las características de la función-autor permiten ver que el nexo
entre el creador y la creación es un proceso emparentado a las
instituciones que rodean al productor. No obstante, por la posibilidad
de un régimen de propiedad con una apropiación distinta a la
penal o legal, estas cuatro características no explican el traslado
de la atribución a la apropiación e, incluso, el «nacimiento»
de la autoría.

En _¿Qué es un autor?_ Foucault (1999) no aclara con detenimiento
la relación entre atribución y apropiación, ¿son características
independientes o una deriva de otra? La apropiación penal se
da en la regulación de la +++PRDC+++ de libros, un procedimiento
inusual antes del siglo +++XVI+++ en Europa (Lafaye, 2002). Foucault
(1999) sitúa la apropiación legal «dentro del sistema de propiedad
que caracteriza a nuestra sociedad»; correctamente a principios
del siglo +++XVIII+++ en Europa. Aunque por su variabilidad y
la falta de espontaneidad, Foucault localiza la atribución en
un rango temporal y espacial más amplio, como en la Antigua Grecia,
la Edad Media y en otras civilizaciones. Pese a ser «génesis»
distintas, Foucault (1999) no describe sus relaciones sino que
parece suponerlas.

La historia de la +++PRDC+++ de textos da pie a pensar que, durante
la modernidad europea, la apropiación acontece como derivación
jurídica de la atribución. La última está presente, por ejemplo,
en la Antigua Grecia entre filósofos presocráticos, en China
con Sun Tzu o en Mesoamérica con Nezahualcóyotl. Una disputa
es si esta atribución implica un individuo y no, tal vez, un
«nombre de autor» que condensa las transformaciones del discurso
por parte de distintos actores. El consentimiento de la apropiación
comercial del texto por parte de entidades gubernamentales data
del siglo +++XVI+++, cuando las monarquías europeas autorizaron
la industria del libro a determinados gremios (Lafaye, 2002;
Loewenstein, 2002; Baldwin, 2014). Pero esta apropiación tiene
antecedentes previos, como el caso de Biblos, la ciudad fenicia
sede del comercio de papiros en la Antigüedad (Mark, 2009). La
atribución y la apropiación eran funciones remitidas a distintos
individuos, por un lado al escritor, por el otro al comerciante,
librero o impresor. Sin embargo, con el Estatuto estas dos características
comienzan a remitir a un sujeto emergente: el autor moderno.

La transferencia de la apropiación se posibilitó a través del
Estatuto durante la modernidad occidental. Este en sí no constituyó
al sujeto creador individualizado; sin embargo, permitió la división
del trabajo entre el escritor y el resto de los oficios dedicados
a la +++PRDC+++ de libros. El _copyright_ facultó la independencia
económica y el distanciamiento del escritor de otras actividades
productivas llevadas a cabo en la imprenta. Antes de ello, este
era un integrante de la esfera de los reproductores: un actor
más dentro de la producción de libros y no una esfera separada
y con procesos distintos.

La atribución al texto implicó ya su apropiación; el _copyright_
hizo propietario al productor. La apropiación legal no fue el
primer «régimen de propiedad para los textos» (Foucault, 1999),
pero sí el primero donde el remitente de la apropiación y la
atribución confluyeron en el mismo individuo. La administración
de la producción literaria pasa a ser ejercida por quien la ejecuta
y, en consecuencia, individualiza a su productor. No es una apropiación
inmanente, sino una transacción posible por la reconfiguración
de las formas jurídicas relativas a la +++PRDC+++ de textos.

Sin embargo, en el discurso se defendió la división del trabajo
y el surgimiento de una nueva esfera a través de las metafísicas
de la creación. La nueva percepción de la producción cultural
como parte del régimen de propiedad fue exaltada y respaldada
por escritores, artistas y científicos de la Ilustración y el
Romanticismo alemán (Baldwin, 2014). La autoría implicó la constitución
de un sujeto creador e individual, de un objeto creado e instituido
como propiedad gracias al respaldo del Estado y de su relación
a partir de un proceso de creación intrínseca. La producción
de textos de un integrante del gremio libresco mutó en la idea
de un individuo creador recluido en su habitación. La reconfiguración
jurídica del texto como un bien facultó la metamorfosis que independizó
la esfera del creador.

El término «creador» tiene un origen metafísico o teológico.
El acto creativo, mencionado en la sección 14, es un «dar a luz»
o la engendración _ex nihilo_. Antes de la modernidad europea
esta actividad se hubiera considerado una herejía. Con la transferencia
de la apropiación y la individualización del productor se posibilitó
percibir la redacción de textos, la ejecución artística y la
generación de conocimiento como creación individual. El Estatuto
facultó la mutación o el desplazamiento de la apropiación comercial
y penal a una legal. Es decir, permitió una nueva esfera en la
industria del libro basada en procesos que recurren a nociones
metafísicas en torno a la actividad productiva. Se trata de un
fenómeno «extraño» que requiere más indagación: cómo las instituciones
jurídicas modernas posibilitaron las metafísicas de la creación.

La _pequeña_ modificación al Estatuto desató otro horizonte de
comprensión para la autoría. Sin embargo, ¿es a partir de esta
como florece el autor? El paso ocurre de manera específica durante
la modernidad europea, no porque escritores de otras latitudes
no hubieran perecido o sido protegidos, sino debido a que el
cambio tiene efecto bajo el supuesto de que la obra es propiedad
del escritor. Estos saltos cualitativos son posibles por una
legislación y un discurso que torna al productor en propietario
y creador. Si acorde a Foucault (1999) esto hace que los discursos
empiecen «realmente a tener autores», entonces la autoría tiene
sus orígenes en la Europa de principios del siglo +++XVIII+++,
o bien, la función-autor que describe es según su modalidad occidental.

Esto pone bajo disputa el alcance de la primera característica
de la función-autor. La autoría sería sinónimo de la noción moderna
del autor que se desata por el Estatuo, se generaliza en Europa
a través de la implementación del _copyright_ o los derechos
de autor y del respaldo llevado a cabo por los discursos de las
metafísicas de la creación, así como se expande a un nivel global
por medio del Convenio de Berna. El resultado es que, cuando
se habla de autor, se afirman y reafirman los ideales de la modernidad
sobre la industria y el comercio del libro. Pero al mismo tiempo
indica la imposibilidad de la autoría «premoderna» o fuera de
la esfera de influencia occidental.

Independientemente al posible eurocentrismo de Foucault (1999),
existe otra consecuencia: la irrelevancia de la autoría para
regular la +++PRDC+++ de bienes culturales. El surgimiento del
autor no está bien establecido en Foucault (1999) porque describe
su desarrollo a través de las instituciones jurídicas de la Europa
del siglo +++XVIII+++. El resultado es que la primera característica
de la función-autor es relativa al contexto y la tradición cultural
de la que Foucault forma parte. Mientras tanto, la apropiación
comercial remitía a otro sujeto distinto al atribuido hasta que,
junto con la apropiación penal, fue trasladada o desplazada por
la apropiación legal del individuo productor. El primer caso
explica la eclosión del autor moderno. En el segundo se expone
la irrelevancia de la autoría para la propiedad del texto. En
ninguno de los casos hay referencia a la «génesis» del autor.

Barthes (1987) ha señalado la falta de fundamento de la autoría.
Para él, el autor no nutre al texto, sino que surge de este.
Foucault (1999) retoma esta crítica para puntualizar dos cuestiones.
El autor no es un nombre propio reducible a un instrumento analítico
para la lingüística, como indica Barthes (1987). En cambio, el
«nombre de autor» tiene una posición privilegiada en el discurso
porque permite una función clasificatoria para su tratamiento
entre el «estado civil de los hombres» y la «ficción de la obra»,
hasta caracterizar su «modo de existencia» (Foucault, 1999).
Además, Barthes reduce la autoría al individuo que escribe, colocándose
en un _a priori_ que «traspone en un anonimato trascendental
los caracteres empíricos del autor» (Foucault, 1999). Barthes
(1987) responderá que la comprensión de la obra queda delimitada
por el proceso de filiación hacia su autor. Es decir, el autor
está en una posición privilegiada, no como una forma trascendental
por el acto de escribir, sino como un mecanismo donde la escritura
socialmente legitima sus intenciones y, por ende, que la obra
se lea bajo la «inscripción del Padre».

El diálogo entre Barthes y Foucault ha sido un tanto desatendido.
_¿Qué es un autor?_ brota a partir de las críticas hechas a Foucault
sobre el uso de autores en _Las palabras y las cosas_ pero también
como respuesta a _La muerte del autor_ de Barthes. Tiempo después
en _De la obra al texto_ Barthes matizó su crítica por las puntualizaciones
de Foucault. ¿El resultado? El autor no tiene «génesis» en cuanto
tal. Pero ¿cómo es que esta categoría se vuelve fundamental para
la crítica literaria, la teoría del discurso, las teorías de
la +++PI+++ y las críticas en pos de los +++BC+++? Quizá pueda
explicarse por la velada mención que Barthes (1987) hace a la
teoría de los actos del habla de Austin (1955). Según Barthes,
el autor moderno no excede la escritura ni la precede: su constitución
se da desde su enunciación.

Como en Foucault (1999), Barthes (1987) se enfoca en el autor
moderno. Sin embargo, su referencia a Austin (1955) permite proponer
la consolidación central de la autoría para diversas vertientes
críticas o teóricas a partir de su tratamiento como «realizativo».
Según Austin (1955), existen expresiones que no describen ni
registran nada, sino que en su enunciación se realiza una acción
allende a lo lingüístico. Austin (1955) estipula seis condiciones
para dicho efecto:

1. La existencia de un procedimiento _convencional_ y _aceptado_.
2. La existencia de las personas y las circunstancias _apropiadas_.
3. La ejecución _correcta_ del procedimiento.
4. La ejecución _completa_ del procedimiento.
5. El comportamiento _comprometido_ de los participantes.
6. El comportamiento _efectivo_ de los participantes.

Con esto se establecen reglas de operación que implican roles
específicos para individuos e instituciones. En los tres tipos
de apropiación existen reglas entre los actores presentes en
la +++PRDC+++ de libros. Antes del Estatuto, la atribución en
la apropiación comercial no afectaba el cumplimiento efectivo
de explotación de capital. Si hay cabida para el autor ---uno
«premoderno» o «no-moderno»---, este se «realiza» por la variabilidad
de la atribución presente en los signos que en el texto remiten
a su productor (Foucault, 1999): no tiene peso en la industria
y comercio de los textos. Por otro lado, el autor de la apropiación
penal se «realizaba» entre los procesos inquisitoriales o de
censura. A través de acciones legales ---como la declaración
y el veredicto--- del escritor, librero, impresor y censor se
constituye al autor herético. Este sería el resultado efectivo
de un procedimiento burocrático detonado por la transgresión
del discurso. Por último, desde el siglo +++XVIII+++ y por mediación
estatal, la apropiación legal traslada o desplaza los otros tipos
de apropiación. La «realización» de la autoría es por relaciones
comerciales contractuales: el establecimiento efectivo de las
obligaciones y derechos del escritor y el editor gracias al contrato.

El autor carece de «génesis» histórica porque se realiza a través
de tres actos del habla: la mención de la atribución, la sentencia
por herejía o la formulación de un contrato. La figura autoral
siempre es una anacronía. La autoría es una invención moderna,
no por un estatuto o su relación a una cultura, sino porque su
«realización» implica un régimen de propiedad distinto a los
modos de organización feudal, un cambio de sentido de la producción
cultural a partir de las metafísicas de la creación y la asunción
de que el uso del lenguaje permite la constitución de realidades
ontológicas. Por ello la insistencia sobre la relación de las
técnicas y tecnologías con el discurso, su materialidad y modalidades
de recepción. Estas generan una tensión que permite el establecimiento
de nuevas reglas de operación en los ámbitos jurídico ---las
leyes que ceden o restringen posibilidades de hacer y de ser---
y teórico ---el discurso que reconfigura o encuentra otras vertientes
para lo que es o que posibilita formas de hacer y de ser---.

El autor no estaría presente más allá de la capacidad analítica
y crítica de ver en estos tres actos una categoría que afecta
a la crítica literaria, la teoría del discurso, las teorías de
la +++PI+++ y las críticas en pos de los +++BC+++. El autor yace
supuesto en la +++PI+++ o los +++BC+++ porque ambas posturas
han asentido en la existencia de tres esferas presentes en el
quehacer cultural. No hay «génesis», sino una intrincada «realización»
de la esfera autoral a través de menciones atributivas, relaciones
industriales y comerciales contractuales, juicios de herejía,
legislaciones como el _copyright_ o los derechos de autor y discursos
elaborados por las metafísicas de la creación.

La reproducción de este anacronismo elude la posición del autor
dentro de la +++PRDC+++ de bienes culturales. Sin importar la
independencia o privacidad de su actividad productiva, la divulgación
pública de la obra ha sido posible por los distribuidores. El
control de estos procesos ha variado con el tiempo. No obstante,
la esfera del autor ha dependido del distribuidor. En este sentido
caben interpretar las nuevas técnicas de autopublicación como
un ejercicio independentista de los productores. Pero mientras
que la industria continúe controlada por el reproductor, cualquier
intento de empoderar al autor se traducirá en la concesión de
un mayor control a la infraestructura bajo su dominio. Una lucha
contra la precariedad económica de los autores que ignora su
dependencia tecnológica corre el peligro de convertirse en una
lucha a favor de los intereses de los distribuidores.

## 17. La implosión de las esferas

Se necesita un análisis pormenorizado sobre las apropiaciones
porque pueden no evidenciar las reglas de operación que justifiquen
la autoría a modo de realizativo, como se describió en la sección
anterior. Lo que se ha querido señalar es que el autor, sin su
enunciación, desaparece. La autoría brota en tres situaciones:
la apropiación indebida, el comercio injusto y la atribución
del discurso. De la demanda por derechos de autor que en varios
casos ni el escritor está de acuerdo; pasando al distribuidor
que exalta al creador, aunque el porcentaje de regalías al productor
no supere el cinco por ciento; hasta la crítica que hace del
autor y _sus_ conceptos el centro de sus reflexiones aunque,
a veces, no se dé cuenta que leen a otro, la autoría solo está
cuando se le llama.

¿Dónde está su autonomía, qué importan sus procesos, quién defiende
sus derechos? Sin la repetición de su enunciación, el autor no
asegura su permanencia. Los dos ejes que constituyen a la autoría
son la atribución y la apropiación remitida al mismo individuo.
No obstante, en la sección pasada se explicó que

* la atribución es posible sin implicar apropiación; es decir,
  hay productor, incluso asalariado, pero no propietario;
* la atribución como apropiación está garantizada por instituciones
  gubernamentales; en otros términos, la ley permitió su comunión
  en el productor;
* la remisión de la atribución y la apropiación al mismo individuo
  justificó procesos intrínsecos; a saber, las metafísicas de
  la creación.

En Foucault (1999) ya existe la preocupación de conceder a una
técnica productiva ---en este caso la escritura--- un «estatuto
originario» donde opera en «términos trascendentales». Pero con
anterioridad, Benjamin (2018) permite hablar de las «metafísicas
de la creación» gracias al concepto de «aura». Si bien este emerge
de una teoría del arte que considera las consecuencias de la
reproducibilidad mecánica, Benjamin (2018) indica que la pérdida
del aura es «un proceso sintomático cuya relevancia va más allá
del ámbito del arte».

No hay una definición unitaria del «aura», sino que Benjamin
(2018) poco a poco suelta características definitorias y clasificatorias.
Este concepto evidencia una singularidad «natural» de la obra
de arte: su lejana unicidad. La lejanía es esencial, donde la
obra de arte, sin importar su proximidad material, se manifiesta
con distancia. Este alejamiento es por el valor «cultual» donde
el rito le concede autenticidad a la obra. Este es el primer
valor de uso dado al arte: un culto a la belleza cuyo culmen
es una teología del arte que rechaza toda función social. La
reproducibilidad mecánica acercará la obra con nuevos modos de
percepción y de valor: la exposición. La cercanía es un proceso
de descentralización de la +++PRDC+++ artística, un cambio de
hábitat donde el arte queda modificado por los espacios en donde
se localizan las masas. El «aquí y ahora» es la unicidad de la
obra artística. La producción de arte se suponía irreproducible
o con una reproducción a partir de un original. Sin embargo,
con la reproducción mecánica la obra de arte puede realizarse
sin un original o, como Benjamin (2018) vio en la fotografía
y el cine, incluso incitar nuevas técnicas artísticas donde la
originalidad reside en la «personalidad» de los actores presentes
en la producción y no en el objeto producido.

En su decaída, el aura es más perceptible a través de sus destellos.
A Benjamin (2018) no le interesa más la génesis de esta categoría
que las posibilidades abiertas por su pérdida debido a la reproducción
mecánica del arte. No obstante, advierte que no es coincidencia
que esta noción ---junto con otros conceptos «tradicionales»
como el genio, la creatividad, la eternidad y el misterio---
sea operante para el fascismo. La decadencia tiene cabida por
la cercanía posible de la reproducibilidad, además de la negación
de la unidad en la serialización. Con ello el arte pierde su
autonomía, porque su operatibilidad mecánica la emancipa del
culto, pero la coloca en el circuito mercantil. Años antes Benjamin
(2016) indicó las implicaciones hacia el productor. El autor,
como elaborador de productos y no como organizador de los medios
de producción, tiene el efecto contrarrevolucionario de poner
su trabajo a disposición del capital. Por ello la urgencia de
que se pregunte «por su posición en el proceso de producción».

El productor como elaborador de mercancías en lugar de organizador
de la producción es un indicativo de la falta de autonomía de
la esfera autoral. Sus procesos creativos adquieren relevancia
por el acceso a la propiedad privada garantizada y fomentada
por instituciones estatales y el comercio. El creador funda su
autonomía por procesos «inmanentes» que acontecen en la privacidad
de su habitación o de su ser. Sin embargo, esto no es funcional
afuera de la esfera del distribuidor. En su decaída, el aura
abre posibilidades, incluyendo las novedosas prácticas de extracción
de capital.

Un ejemplo de la dependencia de la esfera autoral está en las
«economías del regalo» del desarrollo de _software_ libre o de
código abierto, así como en el movimiento de la cultura libre
o en la iniciativa del acceso abierto. Pese a la insistencia
de Stallman (2004) de que el _software_ libre apuesta por el
programador y el usuario ---el creador y el «público»---, las
cuatro libertades establecidas ---presentes en la sección 8---
suponen un salto cualitativo de las libertades en el uso de un
programa a las libertades de quien lo desarrolla o lo consume.
El discurso de Stallman (2004) y de la +++FSF+++ se enfoca en
el _uso_ libre del _software_ para el fomento de una sociedad
«libre» y democrática. No obstante, cuando se cuestiona la organización
de la producción, el discurso del _software_ libre apela a una
posición templada, hasta el punto de negar la categoría de «libre»
al _software_ cuyas licencias impiden la acumulación de capital,
como la Licencia de Producción de Pares (Kleiner, 2019), la Licencia
Editorial Abierta y Libre (Zhenya, 2019) o cualquier licenciamiento
_copyfarleft_.

En las comunidades del _software_ libre y del código abierto
es célebre la disputa en relación con la cláusula necesaria para
el _copyleft_. La licencia del programa computacional tiene que
poseer una condición que garantice la «libertad» al negar el
uso de otra licencia. Esta cláusula hereditaria o virulenta _limita_
las obras derivadas a la misma licencia. Esta paradoja ha sido
criticada por la iniciativa del código abierto, porque la libertad
de empresa está negada ---aunque más bien pretende pasar de una
economía de producción a una basada en la prestación de servicios---.
Aquí se incentiva una asociación entre productores y distribuidores,
como el apoyo corporativo de The Linux Foundation (2019), hasta
el punto de promocionar modelos de negocios sostenibles en contextos
políticos y económicos muy específicos; a saber, un mercado de
capital de riesgo e instituciones gubernamentales que auxiliarían
financieramente y con recursos públicos a las empresas en caso
de quiebra ---como pasó con WeWork, como Uber o Lyft aún trabajan
en números rojos o como Amazon durante años no reportó ganancias---.

El movimiento de la cultura libre ha dado cabida a un discurso
a partir de las metafísicas de la creación. Lessig (2005) sentó
las bases en pos de los creadores a través de reformas al _copyright_.
Este _copyjustright_ enfatiza una renovación de la relación entre
creadores y distribuidores en lugar de cuestionar los fundamentos
que la hacen posible. Aquí la «libertad» es relativa a la elaboración
de bienes y no a la reorganización de la producción. De la mano
de garantías contractuales y de un discurso creacionista, los
productores se ven ante una libertad de empresa que favorece
la transferencia de la riqueza hacia quienes ostentan la infraestructura
de la +++PRDC+++ de bienes culturales. El acceso abierto es un
caso de un creador en específico: el productor de conocimiento.
Sus márgenes de acción por lo general se restringen a las posibilidades
dentro del marco institucional de las universidades y sus relaciones
con los sectores público y privado.

La «libertad» y la «apertura» se han convertido en conceptos
operantes para una mayor extracción de capital a partir del trabajo
de las «economías del regalo». Al mismo tiempo diversas instituciones
ajenas a la esfera autoral fomentan su cultivo según ciertas
reglas de operación que permiten la transferencia de la riqueza
del productor al reproductor. El constante ejercicio de llamar
al autor a conveniencia demuestra que en lugar de ser una realidad
evidente por sí misma, es de una existencia tan fugaz que impide
dar pie a un ecosistema autónomo.

La obsesión por el autor no es clara en un contexto donde el
control de la +++PRDC+++ de bienes culturales está cada vez más
concentrado e inaccesible al productor. Esta opacidad expone
que la creación no es más relevante que las técnicas de escritura,
pintura, escultura, danza, teatro, música, cine, programación
e investigación que facilitan la reproducción de bienes. La pérdida
del aura no es consecuencia del advenimiento de una nueva época
para la producción cultural, sino un desplazamiento técnico impulsado
por los distribuidores gracias a las nuevas posibilidades en
la reproducción de obras. Las metafísicas de la creación son
efectivas en alejar al productor de las condiciones para su organización,
al mismo tiempo que alimentan a la esfera del distribuidor debido
a que cada creador requiere del colaborador que publique los
resultados de su ejercicio practicado en privado.

Este no es el único discurso creacionista posible. La tecnificación
de los procesos creativos permiten el acceso a las herramientas
a un amplio público. Sin embargo, estas capacidades «creativas»
por lo general están limitadas por las tecnologías empleadas;
por ejemplo, lo que permite hacer un programa o lo que autoriza
un distribuidor en su plataforma. La atribución y la apropiación
de la obra queda garantizada por medio de los términos de servicio,
junto a una distribución inequitativa de la riqueza. Por ejemplo,
la generación de contenidos gratuitos de _makers_, _influencers_
y _youtubers_ a cambio de visibilidad o regalías ofrecidas por
el distribuidor. Esto da valor a las plataformas al mismo tiempo
que reproduce la idea de las posibilidades de subsistencia abiertas
por las tecnologías de la creación.

Un discurso creacionista que no cumpla con el doble objetivo
de aislar al productor y reproducir su industria, es una metafísica
o una tecnología de la creación fallida. Esto toma un matiz legislativo
cuando otros modos de organización carecen de formas jurídicas
que las protejan. La autoría ha tenido derechos cuando la cesión
fue posible. Sin esa garantía contractual de transferencia de
la propiedad, la relación entre creador y distribuidor no es
posible. En su reclusión, el productor es autor por el ejercicio
mínimo que garantiza su existencia: la enunciación constante
y efímera de un nombre.

Cada esfera supone cierta autonomía, procesos propios y derechos
específicos. Para la esfera autoral su proceso específico es
la creación; para el reproductor, la +++PRDC+++; para el público,
el uso, el consumo y la crítica. Contrario al creador, cuya autonomía
y derechos están subsumidos a la esfera del reproductor, el público
es un amplio espectro que impide la aclaración de su autonomía
y la identificación de sus derechos.

El uso promovido por las esferas estatal y comercial ha dado
preferencia al consumo privado. Los únicos derechos perceptibles
en la esfera pública ---las doctrinas del dominio público y del
uso justo--- tienen cabida después del término garantizado o
a través de empleos específicos que no afectan la explotación
regular y privada de la obra. No hay en estos derechos un privilegio
en el uso público sobre el privado, al menos que el Estado considere
que el producto es de utilidad pública; a saber, cuando se estima
necesario «para el adelanto de la ciencia, la cultura y la educación
nacionales» («Ley Federal del Derecho de Autor», 1996). Estos
casos son tan infrecuentes que no se localizaron ejemplos contemporáneos
en México.

El consumo tampoco da autonomía al público ya que depende de
las configuraciones políticas y económicas hechas al mercado.
Los derechos del público se diseñan para la defensa del consumidor
en contra de mercancías apócrifas o de prestación defectuosa
de servicios pero no para facilitar su organización. El público
se fragmenta en cada consumidor y su libertad de elección: una
falta de capacidad operativa para generar nueva relaciones con
el mercado; es decir, una pérdida de su capacidad política para
la acción.

Por último, la crítica, aunque amplificada por las nuevas tecnologías
de la información y la comunicación, tiene su alcance público
a través de la mediación de instituciones gubernamentales o privadas.
La crítica en un medio de comunicación tradicional, como el periódico,
la radio o la televisión, es más accesible al crítico profesional
y también más controlado por las esferas del distribuidor y del
Estado. La crítica por otros medios, como las redes sociales
o el _blog_, muchas veces se transforma en un ejercicio sin voz.
La crítica pierde dimensión pública, centrándose en nichos específicos;
o bien, su alcance se limita a los suscriptores de una plataforma
privada. En uno u otro caso la autonomía es desplazada por los
medios que el distribuidor pone a disposición del público y de
los cuales no permite su remodelación.

Aunque queda pendiente un análisis más puntual al respecto, la
esfera pública en muchos casos engloba dinámicas, relaciones
y actores cuyo origen o fin no es «público». Más que una esfera,
el sector «público» es un término que engloba una diversidad
de prácticas políticas, económicas y jurídicas constantes en
su generalidad pero aisladas en su pretensión pública.

## 18. Una teoría común de la producción cultural

En un «marco teórico» que concibe la +++PRDC+++ de bienes culturales
compuesto por tres esferas ---el creador, el distribuidor y el
público---, el uso de la +++PI+++ y de los +++BC+++ exhiben una
interacción constante entre dos actores: instituciones gubernamentales
y entidades privadas ---el productor en su aislamiento, el distribuidor
y su control sobre la producción y un publico que se caracteriza
por el uso, consumo y crítica en privado, todo ello facultado
y delimitado por el Estado---. La concepción de los +++BC+++
como propiedad pública se contrapone a la propiedad privada fomentada
por la +++PI+++. Sin embargo, más allá de la disputa sobre el
tipo de propiedad, ambos espectros se entretejen para dar forma
a una teoría en común, como se ha tratado de evidenciar en las
secciones 13--17 que componen este apartado.

El interés en torno a la +++PI+++ es la manera más patente de
concebir la producción cultural como la fabricación de bienes
para un mercado. En la disputa por la infraestructura en más
de una ocasión la iniciativa del código abierto y el movimiento
de la cultura libre han buscado aliarse a los agentes que sustentan
dicho control. Desde compañías dedicadas principalmente al _hardware_
---como Apple, +++IBM+++, Intel, +++HP+++, Samsung, Sony, Nintendo
o +++LG+++---, pasando por empresas desarrolladoras de _software_
---por ejemplo, Microsoft, Adobe, Electronic Arts, Autodesk,
Oracle, Tencent, Tata o +++SAP SE+++---, hasta plataformas o
servicios en línea ---Amazon, Alphabet (antes Google), Facebook,
Netflix, PayPal, Spotify, Alibaba o Baidu---, varias de estas
empresas han sacado provecho de los licenciamientos de esta iniciativa
y este movimiento. Además, las +++CC+++ han sido abrazadas por
diversas compañías debido a la conveniencia que representan para
la transferencia de capital.

El movimiento del _software_ libre y la iniciativa del código
abierto de manera independiente han apoyado proyectos o empresas
que buscan integrarse en la dinámica actual de comercialización
de bienes; por ejemplo, ThinkPenguin, Technoethical, System76,
Canonical o Red Hat. La iniciativa del acceso abierto en su estado
actual ha quedado en gran parte absorbido por plataformas que
reducen la «apertura» en gratuidad ---la redistribución o la
modificación por lo general son restringidas--- y, en su paso,
encuentran modelos de acumulación de capital como es perceptible
en Academia.edu o en los distintos «tipos» de acceso abierto
de repositorios como Elsevier o +++JSTOR+++.

Las plataformas _anticopyright_, como Pirate Bay, LibGen, Aaaaarg,
Sci-Hub, Memory of the World entre otras, o las organizaciones
ligadas al _copyfarleft_ o a la privacidad en internet, como
Tor, +++P2P+++ Foundation, Sursiendo, el Rancho Electrónico o
En Defensa del Software Libre, muestran una mayor resistencia
ante los modelos de comercialización de bienes culturales. No
obstante, tienden a detentar una acción restringida, muchas veces
consciente y estratégica, o esporádica y enfocada a campañas
y eventos en específico.

Categorías que se pensaban inoperantes para la explotación de
capital, como la «libertad» y la «apertura», han evidenciado
su efectividad para la distribución desigual de la riqueza y
el incremento de las metafísicas o las tecnologías de la creación.
El panorama, aunque quizá desolador, ofrece la oportunidad de
encontrar nuevos «marcos teóricos» para comprender el funcionamiento
contemporáneo de la +++PRDC+++ de bienes culturales, así como
de abrir el horizonte para la constitución o consolidación de
otras prácticas relativas al quehacer cultural. La lucha del
anti-996 o la crisis de la +++FSF+++ o de +++CC+++ después de
la controversia de Jeffrey Epstein hacen evidente esta necesidad.
La tarea no es sencilla, pero entre la complejidad y la diversidad
de problemas abiertos es posible llevarla a su cumplimiento.

# Conclusión. Temas a explorar

Esta investigación se ha enfocado en tratar de señalar diversos
temas para que la reflexión filosófica cuestione las infraestructuras
técnicas, las formas jurídicas y las categorías en las que la
producción, reproducción, distribución y conservación de bienes
culturales, incluyendo las suyas, obtienen su fundamentación.
Según su orden de aparición, los análisis pueden ser sobre:

* El estatuto ontológico de las «ideas» y sus «expresiones concretas».
* La «ética kantiana» propuesta por el _software_ libre.
* La «libertad» presente en las comunidades desarrolladoras de
  _software_.
* El problema de esta «libertad» como dificultad filosófica general.
* La influencia del _software_ libre y el código abierto al
  acceso abierto.
* El impacto del acceso abierto en la producción filosófica
  de América Latina.
* La coherencia teórica de las categorías marxistas empleadas
  por el _copyfarleft_.
* La recepción del _copyfarleft_ en comunidades productoras
  de conocimiento en América Latina.
* La dependencia de la +++PI+++ y los +++BC+++ a la reproducción
  constante de mercancías.
* La diferencia entre las suposiciones de los teóricos de la
  +++PI+++ y los supuestos intelectuales de las filosofías utilizadas.
* Las similitudes entre los defensores de la +++PI+++ y los
  +++BC+++.
* Las implicaciones de la reproducción del habla y los soportes
  que la distribuye.
* La influencia de la materialidad del discurso filosófico para
  su recepción.
* El impulso de las metafísicas y las tecnologías de la creación
  a partir de las facultades garantizadas por instituciones
  jurídicas modernas.
* La posibilidad y las características de la autonomía de la
  esfera pública y sus procesos específicos.

La producción cultural pensada a través de esferas o por medio
de una disputa entre dos «bandos» es insuficiente para explicar
el impacto y los fundamentos de la +++PI+++. Dicho «marco teórico»
también impide la percepción de la diversidad de los movimientos
en pos de los +++BC+++ como diferentes estrategias en un campo
de disputa. La «batalla» del _copyleft_ es principalmente en
el espectro industrial y comercial, la del _copyjustright_ en
el campo jurídico, la del _anticopyright_ sobre las libertades
individuales y la del _copyfarleft_ en torno a la autonomía de
las comunidades productivas. De la economía capitalista al derecho
y del individuo a la sociedad, esta diversidad explicita que
una teoría contemporánea de la producción, reproducción, distribución
y conservación de bienes culturales no puede reducirse a una
cuestión jurídica, legislativa, política, económica o social.

La disputa también está en un terreno teórico y su acervo conceptual
que permiten ir allende a la simplificación de un marco teórico
basado en esferas o al reduccionismo constante a partir de la
disputa entre la +++PI+++ y los +++BC+++. Las nuevas tecnologías
de la información no solo han impulsado la reestructuración de
diversas legislaciones relativas al quehacer cultural, también
han provocado una crisis conceptual en torno a los mecanismos
que permiten la constitución de sujetos dentro de este quehacer,
como son el autor, el inventor, el creador, el reproductor, el
distribuidor y el público.

Ante semejante crisis, pareciera que esta investigación sugiere
desechar el marco teórico que dispone la +++PRDC+++ en esferas,
las cuales establecen, entre varios fenómenos, la «guerra» entre
quienes defienden o critican la +++PI+++. Sin embargo, semejante
sugerencia está lejos de ser posible. La configuración actual
del quehacer cultural es a partir de una retroalimentación constante
entre las ideas y teorías, y las prácticas políticas, jurídicas,
sociales y económicas que las retoman, transforman, eliminan
o dan pie a más producción conceptual. Las esferas son categorías
operantes en cuyo análisis no solo sería posible comprender el
modo de organización de la producción cultural en nuestros días,
sino que también exigiría justificar nociones dadas por sentadas
en dicho tipo de producción. En sus inconsistencias, limitaciones
y saltos cualitativos estas esferas tienen la utilidad de ser
un punto de partida que permite la exposición de un panorama
que es necesario reflexionar filosóficamente.

El quehacer filosófico tiene la capacidad de cuestionar los motivos
por los cuales el desfase entre la teoría y los avances técnicos,
en lugar de poner en crisis al desarrollo tecnológico actual,
termina por reproducirlo o transformarlo. No obstante, una reflexión
filosófica que no cuestiona su dependencia tecnológica y legislativa
para su producción y reproducción, así como obvia su inconsciente
pertenencia a la esfera del creador o productor, es una actividad
que difícilmente reconocerá su sujeción a concepciones románticas,
metafísicas, teológicas o atemporales de una práctica que traslada
la plusvalía de su trabajo a cambio de ciertos intereses distantes
a la reproducción y sustento de su disciplina. El «marco teórico»
expuesto en esta investigación no es una explicación o una solución
a los problemas expuestos, sino una invitación a pensar la +++PRDC+++
en relación directa con las legislaciones y técnicas que hacen
posible el quehacer cultural, como es el caso de la producción
de conocimiento filosófico.

# Glosario

Copyright _y derecho de autor_. Tipos de +++PI+++ que protegen
a las «creaciones literarias y artísticas» (+++OMPI+++, 2019).
Por lo general se cree que custodian las «ideas». No obstante,
estas legislaciones velan por las «expresiones concretas» de
dichas «ideas» (Moore, 2008) ---aunque esto no solventa las dificultades
en su comprensión, sino que abre el problema de su estatus ontológico---.
Otra confusión es la asimilación entre los derechos de autor
y el _copyright_, cuando sus orígenes y ámbitos de protección
son distintos. El _copyright_ cubre el derecho de copia en la
tradición jurídica anglosajona (Baldwin, 2014). Mientras tanto,
los derechos de autor abarcan los derechos patrimoniales, que
en su aplicación son similares al derecho de copia, pero también
los derechos morales elaborados por la «tradición continental»
(Baldwin, 2014). Según el Convenio de Berna, el plazo mínimo
de protección de obras literarias y artísticas abarca la vida
del autor más cincuenta años *después de su muerte*, aunque la
mayoría de los Estados participantes tienen un plazo de por lo
menos setenta años. {.sin-sangria .espacio-arriba1}

_Denominaciones de origen_. Tipo de +++PI+++ que consiste en
un conjunto de signos para identificar productos según las propiedades
específicas del medioambiente de una región o de sus técnicas
de producción (+++OMPI+++, 2019). Estas se emplean para validar
la procedencia del producto. El plazo de protección es indefinido
porque pretende evitar la competencia desleal, proteger al consumidor
e incentivar el desarrollo económico (+++OMPI+++, 2019). {.sin-sangria
.espacio-arriba1}

_Derechos conexos_. Tipo de +++PI+++ que permite a intérpretes,
ejecutantes o productores la utilización de las obras con derechos
de autor o _copyright_ (+++OMPI+++, 2019). El plazo de protección
es de cincuenta años una vez realizada la interpretación de la
obra (+++OMPI+++, 2019). {.sin-sangria .espacio-arriba1}

_Derechos patrimoniales y morales_. Tipos de derechos insertos
en la «tradición continental» del derecho de autor (Baldwin,
2014). Los derechos patrimoniales hacen referencia a la explotación
de la obra y su plazo mínimo de protección abarca hasta los cincuenta
años después de la muerte del autor, aunque lo común sea un plazo
de setenta a cien años. Los derechos morales consideran que la
obra está «unida» al autor («Ley Federal del Derecho de Autor»,
1996) y, según cada legislación, pueden ser renunciables o inalienables.
En México, los derechos patrimoniales están vigentes durante
la vida del autor y cien años *después de su muerte*, mientras
que los derechos morales son inalienables e irrenunciables («Ley
Federal del Derecho de Autor», 1996). {.sin-sangria .espacio-arriba1}

_Diseño industrial_. Tipo de +++PI+++ que protege «los aspectos
ornamentales o estéticos de un objeto» (+++OMPI+++, 2019). Este
tiene que ser novedoso y original, mas no funcional. Su cometido
es atraer al consumidor. En ciertos casos el diseño puede protegerse
como patente o derechos de autor, por lo que está sujeta a interpretación
jurídica. El plazo de protección tiende a ser de cinco años y
renovable hasta quince. {.sin-sangria .espacio-arriba1}

_Marcas_. Tipo de +++PI+++ que consiste en un signo distintivo
de productos o servicios privados (+++OMPI+++, 2019). En el mercado
funcionan para la identificación de insumos y la evasión de mercancía
apócrifa. El plazo de protección es variable y con renovación
indefinida (+++OMPI+++, 2019). {.sin-sangria .espacio-arriba1}

_Patentes_. Tipo de +++PI+++ que abarca el derecho exclusivo
sobre una solución técnica a un problema, también conocida como
«invención». Los inventos deben obedecer ciertas condiciones:
(1) ser novedosos, (2) no ser una solución obvia y (3) ser posibles
y funcionales (+++OMPI+++, 2019). Esto evita que, por ejemplo,
la falta de novedad de la rueda, la obviedad del riego por gravedad
o la imposibilidad de generadores de energía infinita sean patentables.
Estas condiciones no impiden la patentación de invenciones relevantes
para nuestra especie. Por este motivo en varios países no es
posible patentar invenciones de importancia pública como las
teorías o el descubrimiento de sustancias naturales. No existe
consenso respecto a estas limitaciones, así que la relevancia
pública es flexible. El plazo de protección tiende a ser de veinte
años y sin posibilidad de renovación (+++OMPI+++, 2019). {.sin-sangria
.espacio-arriba1}

_Secretos comerciales_. Tipo de +++PI+++ que consiste en elementos
o procesos que no son públicos. Su divulgación se previene por
medio de un contrato que impide su difusión. Estos mecanismos
para su protección y su carácter privado lo tornan en el único
tipo de +++PI+++ cuya protección no implica una nueva legislación.
La revelación del secreto es tratado como incumplimiento de contrato
o como espionaje, por lo que su plazo aplica hasta que este sea
delatado. {.sin-sangria .espacio-arriba1}

# Bibliografía

Anónimo (1996). _Ley Federal del Derecho de Autor_. En &lt;[http://alturl.com/6zghg](http://alturl.com/6zghg)&gt;.
{.frances}

Anónimo (2002). _Budapest Open Access Initiative_. En &lt;[http://alturl.com/xk95z](http://alturl.com/xk95z)&gt;.
{.frances}

Anónimo (2019). _La ingeniosa protesta en China contra el sistema
de trabajo 996_. En &lt;[http://alturl.com/9agox](http://alturl.com/9agox)&gt;.
{.frances}

Arendt, Hannah (2003). «La esfera privada: la propiedad» en _La
condición humana_, pp. 67--72. Paidós. En &lt;[http://alturl.com/mhqra](http://alturl.com/mhqra)&gt;.
{.frances}

Austin, John (1955). _Cómo hacer cosas con palabras_. Escuela
de Filosofía Universidad +++ARCIS+++. En &lt;[http://alturl.com/ucuhi](http://alturl.com/ucuhi)&gt;.
{.frances}

Baldwin, Peter (2014). _The Copyright Wars_. Princenton University
Press. En &lt;[http://shorturl.com/3pm59](http://shorturl.com/3pm59)&gt;.
{.frances}

Barron, Anne (2012). «Kant, Copyright and Communicative Freedom».
_Law and Philosophy_, 31, (1), 1--48. En &lt;[http://alturl.com/ish22](http://alturl.com/ish22)&gt;.
{.frances}

Barthes, Roland (1987). _El susurro del lenguaje_. Paidós. En
&lt;[http://alturl.com/bnqdm](http://alturl.com/bnqdm)&gt;. {.frances}

Benjamin, Walter (2016). _El autor como productor_. Casimiro
libros. En &lt;[http://alturl.com/xsd6q](http://alturl.com/xsd6q)&gt;.
{.frances}

Benjamin, Walter (2018). _La obra de arte en la época de su reproducción
mecánica_. Casimiro libros. En &lt;[http://alturl.com/bgg63](http://alturl.com/bgg63)&gt;.
{.frances}

Berardi, Franco (2003). _La fábrica de la infelicidad_. Traficantes
de Sueños. En &lt;[http://alturl.com/s5vwa](http://alturl.com/s5vwa)&gt;.
{.frances}

Bonfil, Robert; Cavallo, Guglielmo; Chartier, Roger (2001). _Historia
de la lectura en el mundo occidental_. Taurus. En &lt;[http://alturl.com/u39op](http://alturl.com/u39op)&gt;.
{.frances}

Breakey, Hugh (2010). «Natural Intellectual Property Rights and
the Public Domain». _The Modern Law Review_, 73, (2), 208--239.
En &lt;[http://alturl.com/m8t3m](http://alturl.com/m8t3m)&gt;.
{.frances}

+++CERLALC-UNESCO+++ (2015). _América Latina: la balanza comercial
en propiedad intelectual_. +++CERLALC+++. En &lt;[http://alturl.com/tiyxx](http://alturl.com/tiyxx)&gt;.
{.frances}

Chartier, Roger (1999). «Trabajar con Foucault: esbozo de una
genealogía de la “función-autor”». _Signos históricos_, 1, 11--27.
En &lt;[http://alturl.com/7pf3t](http://alturl.com/7pf3t)&gt;.
{.frances}

Condorcet, marqués de (1776). _Fragments sur la liberté de la
presse_. Firmin Didot. En &lt;[http://alturl.com/2s5g5](http://alturl.com/2s5g5)&gt;.
{.frances}

Cotter, Thomas (1997). «Pragmatism, Economics, and the Droit
Moral». _North Carolina Law Review_, 76, (1), 1--96. En &lt;[http://alturl.com/ijrpt](http://alturl.com/ijrpt)&gt;.
{.frances}

Crossland, David (2008). _The Free Font Movement_. University
of Reading. En &lt;[http://alturl.com/7nbjq](http://alturl.com/7nbjq)&gt;.
{.frances}

Debian (2004). _Contrato social de Debian_. En &lt;[http://alturl.com/57iot](http://alturl.com/57iot)&gt;.
{.frances}

Epstein, Richard (2009). «The Disintegration of Intellectual
Property». _Stanford Law Review_, 62, 455--522. En &lt;[http://alturl.com/nk9b3](http://alturl.com/nk9b3)&gt;.
{.frances}

European Union Intellectual Property Office (2019). _Orphan Works
texscdb_. En &lt;[http://alturl.com/gmhuj](http://alturl.com/gmhuj)&gt;.
{.frances}

Fichte, Johann Gottlieb (1793). _Beweis der Unrechtmäßigkeit
des Büchernachdrucks_. Haude and Spener. En &lt;[http://alturl.com/ezocj](http://alturl.com/ezocj)&gt;.
{.frances}

Foucault, Michel (1999). _¿Qué es un autor?_. Universidad de
los Andes. En &lt;[http://alturl.com/p648o](http://alturl.com/p648o)&gt;.
{.frances}

Free Software Foundation (2019). _A roundup of recent updates
to our licensing materials -- November 2018 to June 2019_. En
&lt;[http://alturl.com/rxqyq](http://alturl.com/rxqyq)&gt;. {.frances}

Hall, Gary (2016). _Pirate Philosophy: For a Digital Posthumanities_.
MIT Press. En &lt;[http://alturl.com/tb4j7](http://alturl.com/tb4j7)&gt;.
{.frances}

Hegel, Georg Wilhelm Friedrich (2005). «La propiedad» en _Principios
de la filosofía del derecho_, pp. 125--160. Editora y Distribuidora
Hispano Americana, S.A. (texscedhasa). En &lt;[http://alturl.com/o924q](http://alturl.com/o924q)&gt;.
{.frances}

Hettinger, Edwin C. (1989). «Justifying Intellectual Property».
_Philosophy &amp; Public Affairs_, 18, (1), 31--52. En &lt;[http://alturl.com/4pmz3](http://alturl.com/4pmz3)&gt;.
{.frances}

Hughes, Justin (1988). «The Philosophy of Intellectual Property».
_Georgetown Law Journal_. En &lt;[http://alturl.com/ygugs](http://alturl.com/ygugs)&gt;.
{.frances}

Kant, Immanuel (1785). _Von der Unrechtmäßigkeit des Büchernachdrucks_.
J. E. Biester, F. Gedike. En &lt;[http://alturl.com/o2ihn](http://alturl.com/o2ihn)&gt;.
{.frances}

Kant, Immanuel (2005). «¿Qué es un libro?» en _La metafísica
de las costumbres_, pp. 114--116. Editorial Tecnos. En &lt;[http://alturl.com/ry567](http://alturl.com/ry567)&gt;.
{.frances}

Kehr, Ludwig Christian (1799). _Vertheidigung des Bücher-Nachdruks_.
En &lt;[http://alturl.com/hcsm5](http://alturl.com/hcsm5)&gt;.
{.frances}

Kleiner, Dmytri (2012). _Licencia de Producción de Pares_. En
&lt;[http://alturl.com/t4exk](http://alturl.com/t4exk)&gt;. {.frances}

Kleiner, Dmytri (2019). _Manifiesto telecomunista_. En Defensa
del Software Libre. En &lt;[https://shorturl.at/fhjEH](https://shorturl.at/fhjEH)&gt;.
{.frances}

Lafaye, Jacques (2002). _Albores de la imprenta_. +++FCE+++.
En &lt;[http://alturl.com/cgoin](http://alturl.com/cgoin)&gt;.
{.frances}

Lessig, Lawrence (2005). _Por una cultura libre_. Traficantes
de Sueños. En &lt;[http://alturl.com/26uai](http://alturl.com/26uai)&gt;.
{.frances}

Lessig, Lawrence (2009). _El código 2.0_. Traficantes de Sueños.
En &lt;[http://alturl.com/ogikv](http://alturl.com/ogikv)&gt;.
{.frances}

Locke, John (2006). «De la propiedad» en _Segundo Tratado sobre
el Gobierno Civil_, pp. 32--55. Tecnos. En &lt;[http://alturl.com/xebqa](http://alturl.com/xebqa)&gt;.
{.frances}

Loewenstein, Joseph (2002). _The Author's Due_. _Printing and
the Prehistory of Copyright_. University of Chicago Press. En
&lt;[http://alturl.com/9gh7h](http://alturl.com/9gh7h)&gt;. {.frances}

Mark, Joshua (2009). _Byblos_. En &lt;[http://alturl.com/fgypb](http://alturl.com/fgypb)&gt;.
{.frances}

Maynard, Jay (2010). _Pros and cons of the +++GNU+++ General
Public License_. En &lt;[http://alturl.com/kuiej](http://alturl.com/kuiej)&gt;.
{.frances}

Moore, Adam (2008). «Personality-Based, Rule-Utilitarian, and
Lockean Justifications of Intellectual Property» en _The Handbook
of Information and Computer Ethics_, pp. 105--130. John Wiley
&amp; Sons, Inc.. En &lt;[http://alturl.com/verct](http://alturl.com/verct)&gt;.
{.frances}

Moore, Adam (2012). «A Lockean Theory of Intellectual Property
Revisited». _San Diego Law Review_, 50. En &lt;[http://alturl.com/ndxyf](http://alturl.com/ndxyf)&gt;.
{.frances}

Moore, Adam; Himma, Ken (2014). «Intellectual Property». _The
Stanford Encyclopedia of Philosophy_. En &lt;[http://alturl.com/o3da6](http://alturl.com/o3da6)&gt;.
{.frances}

Moore, Samuel (2019). «Revisiting “the 1990s debutante”: Scholar-led
Publishing and the pre-History of the Open Access Movement».
_Humanities Commons_. En &lt;[http://alturl.com/z7u8e](http://alturl.com/z7u8e)&gt;.
{.frances}

+++OMPI+++ (1971). _Convenio de Berna para la Protección de las
Obras Literarias y Artísticas_. En &lt;[http://alturl.com/cp7ny](http://alturl.com/cp7ny)&gt;.
{.frances}

+++OMPI+++ (2019). _¿Qué es la Propiedad Intelectual?_. En &lt;[http://alturl.com/xxq77](http://alturl.com/xxq77)&gt;.
{.frances}

Palmer, Tom G. (1990). «Are Patents and Copyrights Morally Justified?
The Philosophy of Property Rights and Ideal Objects». _Harvard
Journal of Law &amp; Public Policy_, 13, (3), 817--865. En &lt;[http://alturl.com/6ntxj](http://alturl.com/6ntxj)&gt;.
{.frances}

Proudhon, Pierre J. (1862). _Les Majorats littéraires_. Office
de publicité. En &lt;[http://alturl.com/fiubs](http://alturl.com/fiubs)&gt;.
{.frances}

Proudhon, Pierre J. (2010). _¿Qué es la propiedad?_. Diario Público.
En &lt;[http://alturl.com/pwhdp](http://alturl.com/pwhdp)&gt;.
{.frances}

Raymond, Eric Steven (2016). «La catedral y el bazar» en _¿Propiedad
intelectual? Una recopilación de ensayos críticos_. Perro Triste.
En &lt;[http://alturl.com/gdfbe](http://alturl.com/gdfbe)&gt;.
{.frances}

Schroeder, Jeanne (1998). _The Vestal and the Fasces: Hegel,
Lacan, Property, and the Feminine_. University of California
Press. En &lt;[http://alturl.com/eahew](http://alturl.com/eahew)&gt;.
{.frances}

Schroeder, Jeanne (2004). «Unnatural Rights: Hegel And Intellectual
Propery». _Legal Studies Research Paper_. En &lt;[http://alturl.com/i2d9j](http://alturl.com/i2d9j)&gt;.
{.frances}

Sci+++ELO+++ México (2019). _Acerca de Sci+++ELO+++ México_.
En &lt;[http://alturl.com/h39tb](http://alturl.com/h39tb)&gt;.
{.frances}

Shiffrin, Seana Valentine (2007). «Intellectual Property» en
_A Companion to Contemporary Political Philosophy_, pp. 653--668.
Blackwell. En &lt;[http://alturl.com/4qqa5](http://alturl.com/4qqa5)&gt;.
{.frances}

Stallman, Richard (2004). _Software libre para una sociedad libre_.
Traficantes de Sueños. En &lt;[http://alturl.com/sia8a](http://alturl.com/sia8a)&gt;.
{.frances}

Stallman, Richard (2016). «El manifiesto de +++GNU+++» en _¿Propiedad
intelectual? Una recopilación de ensayos críticos_. Perro Triste.
En &lt;[http://alturl.com/ranhu](http://alturl.com/ranhu)&gt;.
{.frances}

Stengel, Daniel (2004). «Intellectual Property in Philosophy».
_Archives for Philosophy of Law and Social Philosophy_, 90, (1),
20--50. En &lt;[http://alturl.com/x8k6i](http://alturl.com/x8k6i)&gt;.
{.frances}

The Linux Foundation (2019). _Members_. En &lt;[http://alturl.com/6bxjv](http://alturl.com/6bxjv)&gt;.
{.frances}

Tuerto, Perro (2019). _De la edición con_ software _libre a la
edición libre_. En &lt;[http://alturl.com/pqr4m](http://alturl.com/pqr4m)&gt;.
{.frances}

Viollier, Pablo (2017). _Obras huérfanas: un limbo que no beneficia
a nadie_. En &lt;[http://alturl.com/nwepq](http://alturl.com/nwepq)&gt;.
{.frances}

Wikipedia (2019). _Intellectual property_. En &lt;[http://alturl.com/f6gnb](http://alturl.com/f6gnb)&gt;.
{.frances}

Wikipedia (2019). _Sci+++ELO+++_. En &lt;[http://alturl.com/fc9n2](http://alturl.com/fc9n2)&gt;.
{.frances}

Zhenya, Nika (2017). _Derecho de autor cero: entre el derecho
de autor y el dominio público_. En &lt;[http://alturl.com/damrq](http://alturl.com/damrq)&gt;.
{.frances}

Zhenya, Nika (2019). _Licencia Editorial Abierta y Libre_. En
&lt;[http://alturl.com/xa7h3](http://alturl.com/xa7h3)&gt;. {.frances}
