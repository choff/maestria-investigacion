% Universal settings
\documentclass[11pt,extrafontsizes]{book}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[spanish,es-noindentfirst]{babel}
\usepackage[osf]{Alegreya}

% PACKAGE DEFINITION
% Typographical packages
\usepackage{microtype} % for micro-typographical adjustments
\usepackage{setspace} % for line spacing
\usepackage{lettrine} % for drop caps and awesome chapter beginnings
\usepackage[md]{titlesec} % for manipulation of chapter titles
\usepackage{enumitem} % for lists
\usepackage{geometry} % page size
\usepackage{fancyhdr} % headers and footers
\usepackage{tocloft} % toc
\usepackage[font=small,labelformat=empty]{caption} % captions
\usepackage{chngcntr} % change numnering behavior
\usepackage{csquotes} % change quoting behavior
\usepackage{emptypage}
\usepackage{calc}
\usepackage{hologo}
\usepackage[pdfencoding=auto,hidelinks]{hyperref}
\usepackage{ragged2e}
\usepackage{graphicx}

% Bibliography
\usepackage[style=apa,backend=biber]{biblatex} % for bibliography
@bibliography

% Meta
\newcommand{\mytitle}{\emph{@title}}
\newcommand{\myauthor}{@title}
\newcommand{\press}{@press}
\newcommand{\printyear}{@year}
\newcommand{\printdate}{@date}

\if\myauthor\empty
  \newcommand{\myauthor}{@title}
\fi

% Page size
\geometry{
	paperwidth=8.5in,
	paperheight=11in,
	inner=2in,
	outer=2in
}

% TOC
\addto\captionsspanish{
  \renewcommand{\contentsname}{\textsc{Índice general}}
}
\renewcommand{\cfttoctitlefont}{\normalfont\Large}
\renewcommand{\cftpartfont}{\normalfont}
\renewcommand{\cftpartpagefont}{\normalfont}
\renewcommand{\cftchapfont}{\vspace{-1em}\footnotesize\normalfont}
\renewcommand{\cftchappagefont}{\normalfont}

% Defining the title and the author
\title{\mytitle}
\date{@year}

% Custom second title page
\makeatletter
\newcommand*\halftitlepage{\begingroup % Misericords, T&H p 153
	\setlength\drop{0.1\textheight}
	\begin{center}
		\vspace*{\drop}
		\rule{\textwidth}{0in}\par
		{\large\textsc\thetitle\par}
		\rule{\textwidth}{0in}\par
		{\textsc\theauthor\par}
		\vfill
	\end{center}
	\endgroup}
\makeatother

% Part title display
\makeatletter
\renewcommand\part{%
	\if@openright
	\cleardoublepage
	\else
	\clearpage
	\fi
	\thispagestyle{empty}
	\if@twocolumn
	\onecolumn
	\@tempswatrue
	\else
	\@tempswafalse
	\fi
	\null\vfil
	\secdef\@part\@spart}
\makeatother

\titleformat
{\part}
[display]
{\normalfont\scshape\Large}
{\Huge\thepart\centering}
{0pt}
{\centering}[]

% Reset chapter number in each part
\counterwithin*{chapter}{part}

% Chapter title display
\titleformat
{\chapter}
[display]
{\normalfont\scshape\Large}
{\Huge\thechapter\centering}
{0pt}
{\centering}[]

% Quotation
\renewenvironment{quote}
{\small\list{}{
  \listparindent=1.5em
  \leftmargin=.5cm
  \rightmargin=0cm
  \parsep=0cm}%
 \item\relax}
{\endlist}

% Typographical settings for the body text
\setlength{\parskip}{0em}
\linespread{1.09}

% Modify spaces between headers
\titlespacing*{\chapter}
{0pt}{2em}{1em}
\titlespacing*{\section}
{0pt}{1em}{1em}

% HEADER AND FOOTER MANIPULATION
\renewcommand{\headrulewidth}{0.0pt}
\renewcommand{\footrulewidth}{0.0pt}

\setlength{\RaggedRightParindent}{\parindent}

%  THE DOCUMENT
\begin{document}

% Preliminares
@ragged
\frontmatter
\pagestyle{empty}
\maketitle

% Legal
\begin{center}
	\footnotesize
	\vspace*{\fill}

	\mytitle \\ @author \\ \press

	\vskip 1em
  Última edición: \printdate.

	\vskip 1em
	Permitida su reproducción y difusión por cualquier \\
  medio mecánico o electrónico sin la autorización \\
  escrita del titular de los derechos.

  \vskip 1em
  \href{mailto:hi@perrotuerto.blog}{hi@perrotuerto.blog} /
  \href{https://maestria.perrotuerto.blog}{maestria.perrotuerto.blog}

	\vskip 1em
	Hecho en México / \emph{Made in Mexico}

	\normalsize
\end{center}

% Principal
\mainmatter
\newgeometry{
  top=1.5in,
  bottom=2.5in,
	inner=1.5in,
	outer=3in
}

\include{@file}

% Finales
\backmatter
@printbibliography

% Índice
% Descomentar las siguientes líneas si se quiere índice
\cleardoublepage
\pagestyle{fancy}
\fancyhead{}
\fancyhead[CO]{\footnotesize\textsc{Índice general}\normalsize}
\fancyhead[CE]{\footnotesize\textsc{\myauthor}\normalsize}
\begin{center}
	\tableofcontents
\end{center}
\tocloftpagestyle{empty}

% Descomentar para enviar colofón a una página par
%\cleardoublepage\pagestyle{empty}\mbox{} % +2 páginas blancas
%\clearpage\pagestyle{empty}\mbox{} % +1 página blanca

% Colofón
\newgeometry{
	inner=2.5in,
	outer=2.5in
}
\pagestyle{empty}
\vspace*{\fill}
\begin{center}
	\mytitle \hspace{.1em} fue escrita y compuesta
  con \textit{software} libre o de código abierto: \textsc{gnu}/Linux,
  vim, Gedit, biber, JabRef, {\fontfamily{lmr}\selectfont lua\LaTeX},
  Pandoc y Pecas. Para su composición principalmente se usó la familia
  tipográfica Alegreya.

  \vskip 1em
  La producción de este documento implicó el desarrollo
  de distintos programas para su automatización y
  cuidado editorial:
  \href{https://gitlab.com/snippets/1917485}{\texttt{texte}},
  \href{https://gitlab.com/snippets/1917487}{\texttt{texti}},
  \href{https://gitlab.com/snippets/1917488}{\texttt{textu}},
  \href{https://gitlab.com/snippets/1917490}{\texttt{export-pdf}} y
  \href{https://gitlab.com/snippets/1917492}{\texttt{baby-biber}}.

  \vskip 1em
	Documento compuesto el \printdate.

  \vskip 1em
  \href{mailto:hi@perrotuerto.blog}{hi@perrotuerto.blog} /
  \href{https://maestria.perrotuerto.blog}{maestria.perrotuerto.blog}
\end{center}

\end{document}
% END THE DOCUMENT
