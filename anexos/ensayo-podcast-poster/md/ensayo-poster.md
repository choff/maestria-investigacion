# Metafísicas y tecnologías de la creación

## I

Según _The Economist_, el recurso más valioso en el mundo ya
no es el petróleo, sino los datos. La industria más lucrativa
y de más amplio crecimiento es la relacionada con el control
y la minería de la información. Alphabet ---antes Google---,
Amazon, Apple, Facebook y Microsoft son las compañías que más
beneficios están obteniendo por este tipo de economía. Pero detrás
de ellos también están otras empresas como Uber, Snapchat, Tesla,
Vimeo, Adobe y demás corporaciones localizadas en Silicon Valley,
en California, en Nueva York, en Estados Unidos, Europa o China.

En la práctica esta industria provee de plataformas, servicios
y _software_ a sus usuarios de manera gratuita o por «módicos»
precios. A cambio, estas compañías reciben la información de
sus usuarios, la cual analizan para mejorar sus productos o venderla
a terceros. Se trata de un recurso cuya capitalización es relativa
a su cantidad y calidad. Como individuo tu información valdrá
unos cuantos centavos, pero como comunidad su valor se incrementa
casi de manera exponencial, más si detrás de ella existe un _data
analyst_ que la ha destilado y categorizado.

Con esta práctica se tiene ya una manera eficiente de crecimiento
económico: mejores productos es igual a más usuarios; más clientes
implica más datos; más información ayuda a la optimización y
generación de más productos; una mayor producción equivale a
una mayor captación de plusvalía; más capital implica mayor capacidad
de penetración pública; más público conlleva al crecimiento en
la cantidad de usuarios. Así de manera cíclica e inagotable dentro
de un mundo finito.

Un punto de fuga del reportaje hecho por _The Economist_ es la
manera en como legalmente es posible este tipo de economía. Entre
los vericuetos de los términos de servicios muchas veces se garantiza
que la información generada es del usuario, aunque en la práctica
se demuestre lo contrario. Como ejemplos están las técnicas de
_soft delete_, la censura arbitraria de contenidos o el uso de
la información para otros fines distintos y desconocidos por
los usuarios que la generan.

En el lado del cliente existe esa tenue garantía del respeto
a lo que unos llaman datos o información y, otros, _propiedad_
o _bienes_ digitales. En el lado del servidor las tecnologías
y técnicas que hacen posible la masificación de productos y servicios
digitales se cubren con un velo de misterio. La cantidad de personas
y comunidades con la capacidad para auditar los modos de producción
en como estas compañías operan es relativamente escaso. No es
por falta de preparación o una ausencia abismal de recursos técnicos,
sino por los armatostes legales que imposibilitan el análisis
de estas infraestructuras, en lugar de alentar su transparencia
y debate público.

No se trata ya de infraestructuras públicas que el Estado distorsiona
para evitar su correcta evaluación, sino de modos de organización
y de producción que no son visibles gracias a los mecanismos
de propiedad intelectual (+++PI+++) ejercidos por diversas compañías
y que son avaladas, garantizadas y resguardadas por legislaciones
nacionales e internacionales. Desde los derechos de autor, patentes
y marcas, pasando por el diseño industrial, las denominaciones
de origen y los derechos conexos, y hasta los secretos comerciales,
esta «economía de los datos» asegura su permanencia y expansión
a través de las legislaciones de +++PI+++. Por un lado permiten
el uso gratuito de sus productos y servicios. Por el otro, refuerzan
la inaccesibilidad a los modos de producción, reproducción, distribución
y conservación (+++PRDC+++) que los hace posible.

Respecto a la economía de la +++PI+++, en 2015 el Centro Regional
para el Fomento del Libro en América Latina y el Caribe señaló
una balanza «ampliamente deficitaria para la región». El déficit
para 2013 fue de 9,444 millones de dólares, casi el doble del
2005, donde los egresos ascendieron a 10,548 mdd mientras que
los ingresos solo fueron de 1,104 mdd ---¿qué nos depara para
2020 según esta tendencia?---. Esto representa una diferencia
de casi 1/9. Por cada dólar que se ganó por concepto de derechos
de autor, patentes, marcas, diseño industrial, denominaciones
de origen, derechos conexos o secretos comerciales, en América
Latina y el Caribe se pagaron 8.5 dólares por los mismos conceptos.

No obstante, globalmente en 2013 los ingresos por +++PI+++ llegaron
a 279,511 mdd, donde 129,178 mdd terminó en Estados Unidos, lo
que representa el 46% ---¿qué les depara para 2020?---. Es decir,
Estados Unidos recaudó 117 veces más que toda América Latina
y el Caribe. Los egresos de ese año en ese país fueron de 39,016
mdd. Por cada dólar que en Estados Unidos se ganó por conceptos
de +++PI+++ durante 2013, este país gastó 0.3 dólares por el
mismo concepto.

Esta enorme disparidad entre una y otra región puede tener muchos
nombres, varios de ellos problemáticos o poco atractivos pero
que hacen explícito el desbalance que representa la economía
de la +++PI+++. Estos términos pueden ser: intercambio desigual,
extractivismo, colonialismo, imperialismo, capitalismo, entre
otros. Úsese o acúñese el término que más convenga para describir
el fenómeno donde la economía de la +++PI+++ genera más del triple
de ingresos para Estados Unidos al mismo tiempo que representa
una pérdida de casi nueve veces para América Latina y el Caribe.

La economía de los datos y de la +++PI+++ no son del todo asimilables.
No obstante, el nexo entre ambas economías no es circunstancial.
La centralización de los datos por parte de las compañías y las
posibilidades de uso que tienen sus usuarios para los productos
o servicios que les ofrecen están determinadas por los mecanismos
que resguardan la +++PI+++. Los mismos dispositivos que aparentemente
protegen el contenido generado por los usuarios como _propiedad_
suya, también permite que la infraestructura quede afuera de
su alcance.

El resultado que se tiene con esto es un acceso sobre el producto,
pero no sobre la infraestructura de la producción ni de su organización.
Como consecuencia, el usuario carece de mecanismos democráticos
que permitan su participación en la manera en como se despliegan
las tecnologías y técnicas desarrolladas por la industria. La
intervención estatal también queda limitada, ya que las actuales
legislaciones de la +++PI+++ la suponen como propiedad privada.
El Estado supuestamente tiene la responsabilidad de velar por
su protección y regulación, pero carece de facultades para intervenir
de manera directa sobre su gestación.

La dependencia tecnológica y técnica hacia los dueños de los
medios de producción, como sucede con los usuarios de _software_
o de plataformas propietarias, no es un fenómeno reciente. Ya
en 1934 en _El autor como productor_ se alertaba sobre la necesidad
de que el trabajo intelectual y artístico dejara de limitarse
a la elaboración de productos y se enfocara en la función organizadora
de los medios de producción.

En nuestro tiempo, las nuevas tecnologías de la información y
la comunicación han amplificado las posibilidades de producción
cultural y generación de plusvalía. Sin embargo, en esta infraestructura
regida por bits se observa una paulatina reducción de los receptores
de la riqueza producida que a su vez cuentan con la capacidad
política de fijar su rumbo. Entre _youtubers_, artistas _influencers_
en Instagram, cineastas en Vimeo, músicos en Spotify, intelectuales
en Twitter y científicos con perfiles de Linkedin, se hace evidente
que el quehacer cultural, pese a su crisis estructural, continúa
produciendo _ad hoc_ a la demanda de la cultura de masas.

Pero también muestra una cruda realidad. Mientras que estos productores
creen reservar su producción como propiedad, muchas veces se
encuentran sujetos a condiciones laborales precarias o de (auto)explotación,
además del miedo constante al reemplazo o la desactualización.
La garantía ante esta situación es el ofrecimiento gratuito de
tecnologías proveídas por los desarrolladores de _software_,
lo que disminuye sus costos marginales de producción.

El dominio y explotación de una técnica queda bajo responsabilidad
del productor, mientras que el análisis de su uso permite al
distribuidor el desarrollo de nuevas tecnologías, muchas veces
sin tomar en cuenta el desplazamiento y el tiempo invertido por
sus usuarios. Y no solo eso, mientras que esta nueva casta de
productores tratan de capitalizar lo único que estas plataformas
les ofrece ---«exposición» y _likes_---, los distribuidores hace
tiempo extraen su plusvalia a través de la economía de los datos.

¿Cómo es posible que a la par de la alta capacidad productiva,
estos obreros de la cultura se encuentren en una franca alienación?
¿Qué yace ahí que impide a estos productores hacerse cargo del
desarrollo de las tecnologías que permiten la amplificación de
sus técnicas y su sustento de vida, en lugar de estar a la expectativa
de lo que la industria les ofrece? Una hipótesis a descartar
es la ausencia de reacción por ignorancia.

El origen de Amazon fue una librería, el de Alphabet un buscador,
Facebook tuvo su punto de partida con una red social o Adobe
con el +++PDF+++ y después con Creative Cloud. En la actualidad
la economía que rige a las empresas detrás del _modo moderno_
de hacer cultura no son el ofrecimiento de productos o servicios,
sino la recaudación, destilación y capitalización de la información
generada por sus usuarios. Este modelo económico no ha pasado
inadvertido, incluso varios agentes en el sector cultural han
ejercido funciones miméticas ---en la mayoría de los casos ignorando
el contexto geopolítico por el cual dicho modelo es posible---.

Esta disparidad económica entre quien produce y quien distribuye
tiene una larga historia. Esta se correlaciona al proceso histórico
que constituyó y puso a la +++PI+++ en el centro de la +++PRDC+++
de bienes culturales, con especial atención a los derechos de
autor.

## II

Nos encontramos en la Inglaterra del siglo +++XVII+++, sin importar
qué tan ajeno es para nosotros este contexto. La Honorable Compañía
de Impresores y Periódicos, la organización de los gremios editoriales
londinenses, está en conflicto abierto con sus pares escoceses.
¿El problema? Los impresores de Escocia están reproduciendo sus
ediciones sin su consentimiento ---lo que luego se conocería
como «piratería»---. ¿La solución? La Compañía demanda ante la
Cámara de los Comunes la generación de una legislación que garantice
la propiedad de las obras a perpetuidad.

Antes de ello los textos se habían protegido mediante privilegios
reales, un mecanismo legal un tanto anacrónico ante un clima
cultural que abraza la lluvia secularizadora y _anti-establishment_
cada vez más intensa de la Ilustración. La Cámara se percata
del capital político que tiene la osada propuesta de la Compañía.
Esta legislación, entre otras, será el ejercicio de un poder
que demarcará las facultades políticas y su posición dentro del
Estado, ya que desplazará el poder real sobre la +++PRDC+++ de
libros, una industria de alta importancia en la esfera pública
y cultural de Inglaterra.

Pero la Cámara también quiere delimitar el poder político de
la Compañía. Al hacerlo, no solo hará patente su autonomía ante
la autoridad real, sino que también será un agente con una fuerte
influencia en la industria editorial. Recuerda, estamos en el
siglo +++XVII+++, gran parte de la difusión del conocimiento
y la cultura se concretiza a través del comercio de textos.

Entonces, la propuesta de la Compañía sufre una _pequeña_ modificación.
La Cámara permite que jurídicamente el libro se considere una
propiedad, pero con protección caduca. La producción cultural
como propiedad obtiene un matiz claro en esta decisión. La propiedad
como control efímero tiene su antecedente en esta nueva manera
de hacer política.

Así se promulgó el Estatuto de la Reina Ana ---el nombre es para
honorarla, ya que no tuvo una participación activa en el proceso---:
la primera legislación de _copyright_; una acción jurídica moderna
dentro del ecosistema cultural; un ejercicio del poder que hace
de lado los supuestos derechos naturales de impresores, libreros
y mercaderes.

¿Cuáles fueron las consecuencias de esta _minúscula_ modificación?

Primero, una acalorada confrontación entre la Cámara y la Compañía:
la legislación era para protegerlos, no para su restricción.
En su primera legislación, el _copyright_ tenía una duración
de catorce años _después de publicada la obra_ y si y solo si
era registrada. En la actualidad la duración abarca un mínimo
de cincuenta años, por lo regular setenta y cinco o, como sucede
en México, hasta cien años _después de la muerte del autor_ y
sin necesidad de registro. Ante ello, la Cámara permitió un segundo
periodo que extendió su protección por catorce años adicionales,
siempre y cuando se volviera a registrar.

Segundo, sentó las bases jurídicas modernas dentro de la +++PRDC+++
cultural para el resto de las naciones europeas. Así vemos el
surgimiento de legislaciones similares en Francia ---que Proudhon
denunció, pero nadie escuchó---, Alemania, España y Portugal.
También Estados Unidos legislaría de modo análogo, aunque durante
décadas precisó de la piratería escocesa para la alfabetización
de su población, debido a la carencia de infraestructura para
satisfacer las demandas de impresión. El proceso no fue inmediato,
duró al menos dos siglos. Tampoco fue uniforme, la homogeneidad
en materia de derechos de autor es una consecuencia decimonónica
del Convenio de Berna ---como también es responsable del engrosamiento
cualitativo y cuantitativo en esta materia---.

Tercero, _al menos_ dio forma jurídica a los actores dentro de
la +++PRDC+++ de bienes culturales. Se trata de un marco donde
la producción cultural se organiza en cuatro grandes esferas.
Una es la del distribuidor y el reproductor cuya tarea es la
difusión y la reproducibilidad de los productos culturales; ¿en
dónde reside su autonomía?, en el control de los medios de producción
y en la centralización de la extracción de plusvalía. Por otro
lado está el «público», el cual se caracteriza por el uso, el
consumo y la crítica ---por lo general en privado--- de los bienes
culturales y cuya autonomía es relativa a su capacidad de acceso
a la cultura.

Por supuesto el Estado es otra esfera, una que yace en el fondo
y cuya función específica es velar y supuestamente regular las
relaciones mercantiles establecidas por el resto de las esferas.
Por último, está la esfera del productor o, como gusta autodenominarse,
del _creador_. Nótese cómo este sale a relucir justo hasta ahora.
Antes del giro moderno en el quehacer cultural, el productor
se percibía en una función subordinante o plenamente dependiente
al área de influencia del reproductor. El productor no se recluía
en una habitación, sino que llevaba a cabo su ejercicio en un
taller o en compañía de otros.

Cuarto, la atribución y la apropiación del texto empieza a recaer
sobre el mismo sujeto. Antes de la «modernización» del quehacer
cultural el productor del texto ejercía una función atributiva
que permitía evaluar y conservar el conocimiento presente en
un texto. Sin embargo, este no era propietario y, en su lugar,
la apropiación era una función llevada a cabo por el mercader,
el librero o el impresor que desde Fenecia se tiene antecedente
de tratar al texto como un bien comercial.

En _¿Qué es un autor?_ se acierta en descubrir dos grandes funciones
de la «función-autor»; a saber, la atribución y la apropiación.
Pero se equivoca en su localización histórica y geográfica ---que
varias investigaciones han hecho notar---, así como en la cantidad
y tipo de apropiaciones. Además de la apropiación penal o legal
de un texto a un autor, también existe una apropiación comercial
que a partir del giro moderno en la cultura permite la gestación
de la autoría. Autor era quien la inquisición juzgaba por un
texto profano, quien la autoridad real indicaba que tenía un
privilegio pero también quien la autoridad estatal designa como
el _propietario_ de su _producción_ en miras a su comercialización.

Es decir, la autoría en la manera en como hoy la entendemos surge
a través de tres actos del habla ---guiño a _Cómo hacer cosas
con palabras_---: la mención de la atribución, la sentencia por
herejía o la formulación de un contrato. El autor es una anacronía:
jamás ha tenido tiempo más allá de los actos del habla que lo
instituye. La autoría es una invención moderna, no por el giro
jurídico dentro del quehacer cultural ni por su relación a un
marco occidental, sino porque su «realización» implica un régimen
de propiedad ajeno a la organización feudal y debido al supuesto
del cual depende: la capacidad de nuestro lenguaje para la constitución
de realidades y no solo para su descripción. En _La muerte del
autor_ se acierta en sospechar sobre la función que la autoría
ejerce sobre un texto, pero no podemos concluir que el autor
ha muerto. Nos es imposible asesinar a lo que carece de vida:
el autor es un fantasma; un espíritu que se alimenta a través
de la tradición oral y textual como _una_ técnica para la +++PRDC+++
de nuestra cultura.

## III

Estas dos últimas consecuencias se nos presentan hoy en día con
mayor urgencia. El marco que percibe al quehacer cultural a modo
de esferas es problemático. Su carácter sintético simplifica
y reduce la realidad del quehacer cultural. El desfase entre
lo que las leyes permiten y lo que las nuevas tecnologías hacen
posible le acompaña otra asincronía entre la teoría y una realidad
que se le ha escapado de sus manos ---o que jamás estuvo bajo
su mando, pero que así la supuso---. Sin embargo, pese a esta
crisis conceptual, desechar una teoría en pos de otras nuevas
---como pretenden varios teóricos de la +++PI+++ para la justificación
de este tipo de propiedad y para la descripción de la situación
actual de la +++PRDC+++ cultural--- no implica necesariamente
la posibilidad de una mejor comprensión.

Aún no hemos contestado cómo, teniendo los recursos tecnológicos
a un mayor alcance en comparación con otros tiempos, los obreros
de la cultura continúan con una subjetividad de dependencia «creativa».
A partir de las fisuras, las lagunas, las conscientes omisiones
y, en general, lo que estas esferas no nos quieren decir, cabe
la posibilidad de emplear sus categorías para profundizar y al
menos comprender el complejo entrelazamiento entre figuras que
se suponen esferas pero que actúan como mecanismos aislados y
al unísono omnipresentes en el quehacer cultural.

Uno de estos mecanismos se relacionan a la última consecuencia.
Gracias a los movimientos en pos de los bienes comunes y al quehacer
de la crítica de la cultura es cada vez más claro que la apropiación
no es una función esencial para la autoría o, de serlo, deja
de haber autor y obra cuando el trabajo no emplea al mercado
como eje fundacional para el texto o la pieza artística. La capacidad
de apropiación del producto cultural por parte de su productor
cuenta con una historia, rastreable mas quizá no asimilable a
la historia del _copyright_.

Pero ¿qué hay de la atribución? Ni siquiera en _¿Qué es un autor?_
hay claridad respecto a su historicidad. Si la función apropiativa
no es esencial, entonces la función mínima de la autoría reside
en la relación de un nombre de autor con un producto en cuyo
ejercicio surge la obra. No obstante, ha de existir por lo menos
un empleo concreto de esta función atributiva que justifique
dos cuestiones: ¿por qué se la supone relevante en el quehacer
cultural contemporáneo?, ¿por qué se restringe al autor y su
obra?

La tradición humanística siempre ha estado bajo la inspección
y la sospecha de otras tradiciones. Frente a la tradición animalista
justificó el centro de reflexiones en nuestra especie. Frente
a las tradiciones religiosas ha debatido que ninguna quimera
podrá salvarnos. Frente a la producción científica de conocimiento,
se ha valido de la humanidad de nuestra especie para justificar
las clases de subjetividad e intersubjetividad de su discurso,
que lo hacen distinto a cualquier otro tipo de discurso.

La atribución ha sido _una técnica_ que ha permitido la preservación
y la difusión del conocimiento y la sensibilidad producto de
esta tradición. El acompañamiento de una obra con un autor ha
facilitado la transmisión, reproducción y generación de ideas,
conceptos y preceptos. Sin embargo, la idea, de ser acompañada
por una persona pasó a ser apropiada: la idea y _su_ persona.
Lo que empezó como una subordinación ahora ha devenido en una
personificación: la persona y _su_ idea.

El texto y la pieza artística, antes productos de técnicas, han
transmutado en obras cuya creatividad autoral le otorga no solo
de una génesis material, sino también de significado y de sentido.
Existe un salto cualitativo y sin mención expresa de su justificación
entre una actividad productora y una acción creadora, consecuencia
de una conjunción entre las funciones atributivas y apropiativas
hacia un mismo sujeto.

La creación es una categoría metafísica que mienta un «dar a
luz» o una generación _ex nihilo_. Ninguna de estas dos acepciones
hace evidente el contexto social, político y cultural en el que
un texto o una pieza artística ha sido producido. En su lugar
elimina todas estas capas que otorgan significado y sentido a
la producción cultural por una íntima y erótica relación de parentesco
entre el autor y la obra.

El deseo vehemente del autor de ser reconocido a partir de un
ejercicio que supone realizarlo en privado y el cuál tiene su
temple, en poco o en nada se asemeja a la necesidad de conservación
de una tradición. Las tecnologías al servicio del conocimiento
o afectos humanísticos no eran capaces de transmitirlos en el
soporte mismo sin correr el peligro de una pérdida de sus coordenadas
espaciales y temporales que permiten una mayor inteligibilidad.
El ejercicio técnico de asociar una idea o un afecto con un nombre
---no siempre una persona de carne y hueso---, y por ende de
una historia, fue una manera de evitar la degradación de sus
discursos a través del tiempo.

No obstante, las nuevas tecnologías de la información y la comunicación
están acercando un horizonte donde el producto en sí mismo puede
contener esa referencia espaciotemporal y contexual. Es decir,
aunque aún impracticable, ya hay pautas para pensar en un tipo
de atribución cuyo fundamento resida en el mismo objeto atribuido.
Con ello se palpa la posibilidad de prescindir del autor.

Bajo semejante hipótesis existen una serie de consecuencias interesantes.
Si la atribución es la última función aún sin historización plena
que justifica la existencia de la autoría, pero a su vez puede
ser un mecanismo que no requiera de ningún nombre para establecer
la posición de un producto dentro de nuestra cultura, entonces
esta función no requiere esencialmente de ninguna autoría. Ante
ello, la atribución no sería sinónimo de _atribución a través
de un nombre_, así como el autor pasa a ser una clase potencialmente
vacía por su poca operatividad como técnica ante el nuevo contexto
tecnológico de la +++PRDC+++ cultural.

Es decir, la atribución no sería una cualidad o una función de
la autoría, sino que el autor se evidenciaría como un mecanismo
de asignación de cualidades a un discurso, un dispositivo cuya
hegemonía técnica pretende justificarse por su omnipresencia.
Cabe la pena resaltar que esto no implica la eliminación de todo
nombre de autor de un texto, sino que este acto nominativo deja
de ser _la_ técnica para la preservación de las cualidades de
un texto y, con mayor gravedad, pasa de ser una clase con funciones
a una variable. En lugar de ejercer una influencia sobre la inteligibilidad
de un texto, el nombre de autor vendría a ser una cualidad adicional
para relacionar al texto con un contexto a través de un sujeto.

Esto implica que las funciones atributiva y apropiativa de la
autoría no son primordiales. Ante esta pérdida nos encontramos
con una función adicional que bien podría ser la esencial para
esta categoría. El deseo que implica la constitución y conservación
de una idea a través de un nombre, incluso al punto de apropiarla
y personificarla, deja entrever que la autoría cuenta con una
función ideológica.

La concepción a perpetuar consiste en un imaginario que invisibiliza
la realidad material, social y política por una idea que tiene
valía en la intimidad de su ejercicio. Pero este elemento es
ya indemostrable porque su prueba requiere salir de la esfera
erótica del autor y su obra: un rompimiento del «aura» cuya teología
supone el rechazo de toda función social, descrito en _La obra
de arte en la época de su reproducción mecánica_.

La autonomía de la esfera creativa por la relación entre el autor
y la obra tiene el carácter paradójico de pretender su justificación
y demostración ---acciones ajenas a esta esfera--- a partir de
una relación solo comprensible a través de una íntima experiencia.
El fenómeno es semejante a un acto de fe: extrínsecamente indemostrable
pero intrínsecamente evidente de suyo, cuya experiencia religiosa
en común convierte este acto en un culto que, en la ortodoxia
de su reproducción, se transforma en dogma.

## IV

Debido a las características presentes en la categoría «autor»,
cuando se habla de metafísicas ---por no decir «teologías», para
evitar más equívocos--- de la creación nos referimos a la justificación
intrínseca para la autonomía de una esfera que se supone cualitativamente
distinta al resto del ecosistema de la +++PRDC+++ cultural. A
saber, la esfera del productor que una y otra vez se constituye
como creador a través de los actos del habla y de fe. Sin embargo,
desde un ámbito extrínseco, la individuación de un agente dentro
de la cadena de producción cultural masiva semeja más a una actitud,
creencia o pretensión que a un hecho o una realidad inteligible
fuera de la relación erótica entre el autor y la obra.

El problema de esta denominación es su connotación negativa:
las metafísicas de la creación son una de las condiciones para
la alienación del productor cultural. En defensa puede decirse
que la negatividad de este sintagma recae principalmente en la
creación. La sustitución de la materialidad, la cual un discurso
adquiere las posibilidades para su intelección, por un carácter
irrisorio sobre la producción cultural tiene como consecuencia
la negación de los mecanismos de extracción de plusvalía que
el productor tiene enfrente de sí al ejecutar las técnicas específicas
de su quehacer. Al contrario, percibe esta explotación a través
de una abstracción de las funciones concretas de distintas empresas,
como si otras alternativas tecnológicas no fueran ya posibles,
pese a tenerlas a la mano.

Existen otros mundos aún no concretos y quizá jamás realizables
que funcionan como guía para muchos productores que no comparten
el discurso de las metafísicas de la creación. No por ello dejan
de ser metafísicas, pero sí abandonan los supuestos de que la
producción es creación, que la creación es apropiación y que
su ejercicio es un acto de fe.

Las metafísicas de la creación operan en el discurso y en la
concepción que el productor tiene de sí en relación con los medios
de +++PRDC+++ cultural. Cabe ahora la pena preguntarse por los
mecanismos que permiten traducir esta subjetividad en precariedad
y (auto)explotación, al mismo tiempo que centraliza y redirige
la riqueza en cada vez menos reproductores.

La acción creadora como mecanismo ideológico omnipresente aísla
al productor del resto de la cadena productiva a partir de un
discurso que le confiere de un ideal irrealizable e indemostrable.
En el aislamiento de su quehacer, el productor pierde la noción
de dos características fundamentales para la +++PRDC+++ cultural.
El creador ve delante de sí una realidad dada en la cual su capacidad
productiva se reduce a la capacidad de producir productos, en
lugar de una capacidad de organizar la producción. Esto de manera
fundamental implica la pérdida de casi toda capacidad política
y la estetización del quehacer cultural. En la supuesta apoliticidad
de su ejercicio, este productor además deja escapar cuán valiosa
es su actividad productiva. En su lugar, el creador se queda
en una posición de frustración constante por no poder demostrar
el valor de su producción.

Esta ausencia de valor no es debido a que el productor, la producción
o el acto de producir no tengan valía ni significado ni sentido.
La imposibilidad de capitalizar la «exposición» y los _likes_
que las plataformas de producción le confieren tiene su explicación
en un hecho: la plusvalía es extraída y las posibilidades para
su cooptación es invalidada antes de que el productor tenga oportunidad
de acceder a su riqueza generada.

Imagina por un momento que, en lugar de ser un obrero de la cultura,
desempeñas tus actividades en una minería. Tu trabajo consiste
en la extracción de un mineral. A simple vista parece tierra;
sin embargo, a través de procesos industriales de ese polvo se
extrae plata. Tú sabes que esa tierra que sacas con tu esfuerzo
tiene valor, pero no cuentas con los medios de producción para
«destilarlo». De hecho, desconoces a tal grado este proceso que
hablas de alquimia en lugar de procedimientos químicos e industriales.
Un día, la compañía minera decida mudarse, por lo que te da un
ultimátum: migrar con ellos o quedarte y tratar de obtener beneficios
de la mina por tu propia cuenta. Por diversos motivos decides
quedarte. Día tras día, mes tras mes, año tras año intentas extraer
plata. En un primer momento te era imposible porque no contabas
con la infraestructura. Para solucionarlo pides un préstamo para
conseguir el equipo necesario. No obstante, después de décadas
de tratar de capitalizar esa mina, y entre el mar de deudas,
frustración, precariedad y autoexplotación, te percatas del motivo
por el cual la compañía decidió migrar: la mina ya no tiene los
márgenes mínimos de utilidad para que valga la pena ser explotada.

En una producción cultural que depende de las nuevas tecnologías
de la comunicación y la información, la infraestructura y los
conocimientos para la capitalización de la actividad productiva
se encuentra en manos de compañías tecnológicas. Grandes o pequeñas,
en Silicon Valley o en Shenzhen, con un producto o servicio original
o su mímesis, con un modelo de negocios sostenible o dependiente
de varios tipos de subsidios, la mayoría de estas empresas se
caracterizan por tener como su función principal la extracción
de plusvalía, sea en la +++PRDC+++ cultural o en cualquier otro
ecosistema. Su lealtad no es ante los procesos productivos en
particular de alguna disciplina, sino en la «monetización» y
capitalización de estos procesos.

No es objeto de reflexión ética que la valoración del productor,
el producto o los procesos de producción sea en relación con
un margen de ganancias mínimo que justifique la implementación
o el mantenimiento de cierta tecnología. No importa tu posición
dentro de esa cadena, no es relevante lo que tú produzcas ni
tampoco tus procesos de producción tienen valía por sí mismos
---el arte por el arte, el conocimiento por el conocimiento---.
Si no se adaptan al plan de negocios en donde tu trabajo ---apolíticamente
denominado «uso»--- permita una economía de datos o de +++PI+++,
las opciones que quedan al alcance son sencillas: migras o las
técnicas de tu producción cultural quedan sin la infraestructura
para su reproducción.

En el abandono se evidencia la imposibilidad de mantener y abastecer
una técnica determinada como obsoleta por agentes externos a
la producción, pero que por cuestiones de +++PI+++ son los propietarios
de las tecnologías necesarias para su ejecución. En la migración
no yace una realidad más optimista. Debido a que el factor importante
es la extracción de plusvalía del trabajo realizado por los usuarios,
poco o nada es motivo de preocupación que los cambios constantes
de infraestructura impliquen una insistente adaptación del usuario
y sus técnicas a las tecnologías en voga disponibles. Al final,
la plusvalía obtenida no es por una actividad productiva ni por
la labor de un individuo. En la amplificación del trabajo gratuito
dado a una tecnología propietaria reside una economía de los
datos y de +++PI+++ en la que al obrero se le entrega la ganga
para su goce y capitalización: los derechos de autor sobre su
producto, la «exposición» y los _likes_.

Para ponerlo desde otra perspectiva, la plusvalía no reside ni
en el productor ni en su producto, sino en el proceso de producción
que transforma en capital el trabajo realizado dentro de una
plataforma o un _software_. Es decir, no importa el tipo de actividad
que se realice con las tecnologías ofrecidas, el valor reside
en el trabajo implicado, bueno o malo, profesional o amateur,
con fines personales o corporativos. Por ello la categoría «uso»
no hace justicia al problema político de fondo: el obrero trabaja
creyendo que su actividad productiva es una acción creativa cuyo
principal bien económico es su creación, cuando en su lugar el
valor reside en el trabajo mismo que se transforma en información
y de ahí en capital.

Con la vestidura de economías de servicios, la riqueza potencial
de las economías de los datos y de la +++PI+++ reside en modelos
económicos basados en el fenómeno donde la acumulación de capital
depende del trabajo gratuito ejercido dentro de procesos productivos
propietarios. La categoría «uso» no permite evidenciar que no
es el usuario a quienes se les ofrece plataformas o _software_
gratuito para su trabajo, sino que es el trabajo gratuito del
usuario el que da valor a estos servicios o productos, incluso
hasta el punto donde _de facto_ estas tecnologías propietarias
---gratuitas o con bajos precios--- se convierten en el estándar
de una industria.

El uso gratuito de tecnologías propietarias o, lo que es lo mismo,
el trabajo donado para la producción de mercancías sin la capacidad
política de organizar su infraestructura, moldean las posibilidades
actuales y futuras de la +++PRDC+++ cultural. Aunque parezca
paradójico, la amplitud de las capacidades «creativas» ---aquellas
cuyo ejercicio íntimo suponen una casi nula influencia extrínseca---
están delimitadas por las tecnologías «gratuitas» disponibles
a la mano. El creador lleva a cabo su ideal pero bajo la regulación
de lo que un programa o una plataforma permiten. En esta estrechura
de las técnicas posibles se pierde la posibilidad objetiva de
emancipación. Además, la constitución de la subjetividad del
productor de conocimiento o de cultura se constriñe a estas reglas
de formación y a los procedimientos establecidos por los agentes
con capacidad política: las instituciones privadas que desarrollan
las tecnologías estandarizadas por la industria y las instituciones
estatales que resguardan el control y mantienen el orden en dicha
organización.

Entonces, las tecnologías de la creación son aquellas que en
la concretud de sus técnicas y procedimientos permiten el aislamiento
del productor del resto de los procesos productivos, así como
delimitan arbitrariamente el horizonte de sus técnicas posibles
y, con ello, determinan la constitución de su subjetividad. Las
metafísicas de la creación conceden un horizonte de significado
y de sentido intrínseco a semejante enclaustramiento. Las tecnologías
de la creación son la implementación concreta para la extracción
de plusvalía del trabajo llevado a cabo por el productor. Las
metafísicas de la creación permiten que esta extracción sea ignorada
o desviada hacia una entidad abstracta, lo cual impide la asimilación
de la acción creadora como una actividad productora en la que
el obrero no tiene injerencia sobre su dirección. La acción creativa
tiene su fuerza discursiva en la transformación de la capacidad
política en un acto de fe o en una estetización del ejercicio.
Por ello, un discurso creacionista que no cumpla con el doble
objetivo de aislar al productor y reproducir los modos actuales
de organización de la producción, es una metafísica o una tecnología
de la creación fallida.

En la falta de cuestionamiento de las ideas, los conceptos, las
categorías y los marcos teóricos centrales de nuestros quehacer
yace la posibilidad de su estratificación hasta el punto de funcionar
como una pieza en una maquinaria que persigue otros objetivos,
algunos de ellos a veces contrarios a las intenciones de la actividad
productora. Una producción cultural con duras posturas políticas
deviene en metafísica de la creación en la medida en que se obvia
la influencia que las tecnologías empleadas y la manera en como
están organizadas determinan el sentido y el alcance de los objetivos
políticos y de las técnicas aplicadas.

Por ello, te pregunto, ¿es ese el tipo de alienación al que te
has suscrito?
