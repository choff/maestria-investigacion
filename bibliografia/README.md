# Bibliografía

La bibliografía está hecha en BibTeX, para su visualización puede 
utilizarse una interfaz gráfica como [JabRef](http://www.jabref.org/) o 
[verse directamente](https://0xacab.org/NikaZhenya/maestria-investigacion/blob/master/bibliografia/bibliografia.bib).
