biber --tool \
      --output_align \
      --output_indent=2 \
      --output_fieldcase=lower \
      --output_field_order=a  \
      --strip_comments \
      bibliografia.bib

mv bibliografia_bibertool.bib bibliografia.bib

rm bibliografia.bib.*

sed 's/<title>[^<]*<\/title>/<title>El creador y lo creado | Bibliografía<\/title>/g' bibliografia.html > bibliografia.html.new

mv bibliografia.html.new bibliografia.html
